# Control Logic

First setup:
  - $ cp .env.development .env
  - Prepare environment variables
  - Prepare composer dependencies
  - $ php artisan migrate
  - $ php artisan db:seed
  
Happy coding!
