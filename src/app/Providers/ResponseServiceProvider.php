<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot(): void
    {
        Response::macro('api', fn(string $message = 'Success', $data = [], array $errors = [], int $status = 200) => response()
            ->json(compact('message', 'data', 'errors', 'status'), $status)
        );
    }
}
