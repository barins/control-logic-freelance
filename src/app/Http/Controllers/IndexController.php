<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class IndexController
 *
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    /**
     * Get main page
     *
     * @return View
     */
   public function __invoke(): View
   {
      return view('welcome');
   }
}
