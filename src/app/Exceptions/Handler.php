<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param  Throwable  $exception
     * @return Response|JsonResponse
     *
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($request->wantsJson()) {
            return $this->prepareJsonResponse($request, $exception);
        }

        return parent::render($request, $exception);
    }

    /**
     * Prepare a JSON response for the given exception.
     *
     * @param  Request  $request
     * @param  Throwable $exception
     *
     * @return JsonResponse
     */
    protected function prepareJsonResponse($request, Throwable $exception): JsonResponse
    {
        return response()->api(
            $exception->getMessage(),
            [],
            [$this->convertExceptionToArray($exception)],
            $this->isHttpException($exception) ? $exception->getStatusCode() : 500,
        );
    }

    /**
     * Convert the given exception to an array.
     *
     * @param  Throwable  $exception
     * @return array
     */
    protected function convertExceptionToArray(Throwable $exception): array
    {
        return config('app.debug') ? [
            'exception' => get_class($exception),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ] : [
            'info' => $this->isHttpException($exception) ? $exception->getMessage() : 'Server Error',
        ];
    }
}
