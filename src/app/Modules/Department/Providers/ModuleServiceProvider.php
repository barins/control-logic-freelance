<?php

namespace App\Modules\Department\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('department', 'Resources/Lang', 'app'), 'department');
        $this->loadViewsFrom(module_path('department', 'Resources/Views', 'app'), 'department');
        $this->loadMigrationsFrom(module_path('department', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->mergeConfigFrom(module_path('department', 'Validation') . '/validation-rules.php', 'validation-rules');
            $this->loadConfigsFrom(module_path('department', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('department', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
