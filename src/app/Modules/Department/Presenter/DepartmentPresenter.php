<?php

namespace App\Modules\Department\Presenter;

use Yaro\Presenter\AbstractPresenter;

class DepartmentPresenter extends AbstractPresenter
{
    protected $arrayable = [
        'id',
        'name',
        'manager',
        'office',
        'company_id',
        'parent_department',
        'total_number_of_employees',
        'total_number_of_projects',
        'current_monthly_income',
        'current_monthly_costs',
        'current_month_profit',
        'previous_month_profit',
        'previous_month_growth',
        'annual_income',
        'annual_costs',
        'total_income',
        'total_costs',
        'created_at',
        'updated_at',
    ];

    public function getOfficePresent(): array
    {
        return $this->__get('office')->toArray();
    }

    public function getParentDepartmentPresent(): ?array
    {
        return $this->getUnknownProperty('parent_department');
    }

    protected function getUnknownProperty(string $modelName): ?array
    {
        $model = $this->__get($modelName);

        if($model) {
            return $model->toArray();
        }

        return null;
    }
}