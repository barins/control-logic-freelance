<?php

/**
 * Routes for REST-API of department
 */

use App\Modules\Department\Http\Controllers\DepartmentCRUDController;
use App\Modules\Department\Http\Controllers\DepartmentTableController;

Route::group(['middleware' => 'auth:master_user'], static function () {
    Route::get('{uuid}', [DepartmentCRUDController::class, 'index'])->name('departments.index');
    Route::post('create', [DepartmentCRUDController::class, 'create'])->name('departments.create');
    Route::put('{uuid}/update', [DepartmentCRUDController::class, 'update'])->name('departments.update');
    Route::delete('{uuid}', [DepartmentCRUDController::class, 'delete'])->name('departments.delete');

    Route::get('select/options', [DepartmentCRUDController::class, 'getOptionsForSelect'])->name('departments.select.option');

    /**
     * To build the main table for departments
     */
    Route::group(['prefix' => 'table'], static function () {
        Route::get('columns', [DepartmentTableController::class, 'getColumns'])->name('departments.table.columns');
        Route::get('structure', [DepartmentTableController::class, 'getTableStructure'])->name('departments.table.structure');
        Route::get('filter/select', [DepartmentTableController::class, 'getOptionsForSelectFilter'])->name('departments.table.filter.select');
        Route::get('/', [DepartmentTableController::class, 'getDepartments'])->name('departments.table.get');
    });
});