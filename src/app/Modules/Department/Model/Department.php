<?php

namespace App\Modules\Department\Model;

use ScoutElastic\Searchable;
use App\Modules\Office\Model\Office;
use App\Modules\Core\Model\BaseModel;
use App\Modules\Core\Traits\UsesUuid;
use App\Modules\Employee\Model\Employee;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Modules\Department\Presenter\DepartmentPresenter;
use App\Modules\Department\ElasticIndexConfigurator\DepartmentIndexConfigurator;

/**
 * Class Department
 *
 * @property string name
 * @property integer manager_id
 * @property integer office_id
 * @property integer parent_department_id
 * @property integer number_of_employees
 * @property integer total_expenses
 * @property integer total_income
 * @property integer number_of_projects
 *
 * @package App\Modules\Department\Model
 */
class Department extends BaseModel
{
    use UsesUuid;
    use Searchable;

    protected string $presenter = DepartmentPresenter::class;

    protected $indexConfigurator = DepartmentIndexConfigurator::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'manager_id',
        'office_id',
        'company_id',
        'parent_department_id',
        'total_number_of_employees',
        'total_number_of_projects',
        'current_monthly_income',
        'current_monthly_costs',
        'current_month_profit',
        'previous_month_profit',
        'annual_income',
        'annual_costs',
        'total_income',
        'total_costs',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $mapping = [
        'properties' => [

        ]
    ];

    /**
     * Get office for the department.
     */
    public function office(): hasOne
    {
        return $this->hasOne(Office::class, 'id', 'office_id');
    }

    /**
     * Get office for the department.
     */
    public function parent_department(): hasOne
    {
        return $this->hasOne($this, 'id', 'parent_department_id');
    }

    /**
     * Get manager for the department.
     */
    public function manager(): hasOne
    {
        return $this->hasOne(Employee::class, 'id', 'manager_id');
    }
}
