<?php

namespace App\Modules\Department\ElasticIndexConfigurator;

use ScoutElastic\Migratable;
use ScoutElastic\IndexConfigurator;

class DepartmentIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'control_logic_department';

    /**
     * @var array
     */
    protected $settings = [
        'mapping' => [
            'total_fields' => [
                'limit' => 2000
            ]
        ]
    ];
}