<?php

/**
 * Rules for department.
 */

return [
    'department_type' => [
        'name' => ['string'],
        'office_id' => ['string'],
        'manager_id' => ['string'],
        'parent_department_id' => ['string'],
    ],

    'department_create' => [
        'name' => ['string', 'required'],
        'office_id' => ['uuid', 'required'],
        'manager_id' => ['uuid'],
        'company_id' => ['uuid', 'required'],
        'parent_department_id' => ['uuid'],
    ],
];