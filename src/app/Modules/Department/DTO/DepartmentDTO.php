<?php

namespace App\Modules\Department\DTO;

use App\Modules\Core\DTO\BaseDTO;

/**
 * Class DepartmentDTO
 *
 * @package App\Modules\Department\DTO
 */
final class DepartmentDTO extends BaseDTO
{
    protected string $name;
    protected string $office_id;
    protected ?string $manager_id;
    protected string $company_id;
    protected ?string $parent_department_id;

    private function __construct(
        string $name,
        string $office_id,
        ?string $manager_id,
        string $company_id,
        ?string $parent_department_id
    )
    {
        $this->name = $name;
        $this->office_id = $office_id;
        $this->manager_id = $manager_id;
        $this->company_id = $company_id;
        $this->parent_department_id = $parent_department_id;
    }

    /**
     * @param  array  $data
     *
     * @return DepartmentDTO
     */
    public static function fromArray(array $data): DepartmentDTO
    {
        return new self(
            $data['name'],
            $data['office_id'],
            $data['manager_id'] ?? null,
            $data['company_id'],
            $data['parent_department_id'] ?? null,
        );
    }
}