<?php

namespace App\Modules\Department\Services\CRUD;

use App\Modules\Department\DTO\DepartmentDTO;
use App\Modules\Department\Services\CRUD\Handlers\{
    Get,
    Create,
    Update,
    Delete,
};

final class CRUDService
{
    public function get(string $department_id): array
    {
        return (New Get($department_id))->handle();
    }

    public function create(DepartmentDTO $department_DTO): array
    {
        return (New Create($department_DTO))->handle();
    }

    public function update(string $uuid_department, DepartmentDTO $department_DTO): array
    {
        return (New Update($uuid_department, $department_DTO))->handle();
    }

    public function delete(string $uuid_department): bool
    {
        return (New Delete($uuid_department))->handle();
    }
}