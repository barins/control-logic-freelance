<?php

namespace App\Modules\Department\Services\CRUD\Handlers;

use App\Modules\Core\Services\Handler;
use App\Modules\Department\Model\Department;

final class Delete implements Handler
{
    private string $uuid_department;

    protected array $office;

    public function __construct(string $uuid_department)
    {
        $this->uuid_department = $uuid_department;
    }

    public function handle(): bool
    {
       return (bool) Department::where('id', $this->uuid_department)->delete();
    }
}