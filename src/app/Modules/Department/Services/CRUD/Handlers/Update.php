<?php

namespace App\Modules\Department\Services\CRUD\Handlers;

use App\Modules\Core\Services\Handler;
use App\Modules\Department\DTO\DepartmentDTO;
use App\Modules\Department\Model\Department;

final class Update implements Handler
{
    private string $uuid_department;
    private DepartmentDTO $department_DTO;

    protected array $department;

    public function __construct(string $uuid_department, DepartmentDTO $department_DTO)
    {
        $this->uuid_department = $uuid_department;
        $this->department_DTO = $department_DTO;
    }

    public function handle(): array
    {
        $this->saveInDB();

        return $this->department;
    }

    protected function saveInDB(): void
    {
        $department = Department::where('id', $this->uuid_department)->first();
        $department->fill(
            $this->department_DTO->toArray()
        )->save();

        $this->department = $department->toArray();
    }
}