<?php

namespace App\Modules\Department\Services\CRUD\Handlers;

use App\Modules\Core\Services\Handler;
use App\Modules\Department\Model\Department;

final class Get implements Handler
{
    protected string $department_id;

    public function __construct(string $department_id)
    {
        $this->department_id = $department_id;
    }

    public function handle(): array
    {
        $department = Department::where('id', $this->department_id)->first();

        $success = (bool) $department;

        return [
            'success' => $success,
            'department' => $success ? $department->toArray() : null,
        ];
    }
}