<?php

namespace App\Modules\Department\Services\CRUD\Handlers;

use App\Modules\Core\Services\Handler;
use App\Modules\Department\DTO\DepartmentDTO;
use App\Modules\Department\Model\Department;

final class Create implements Handler
{
    protected DepartmentDTO $department_DTO;

    public function __construct(DepartmentDTO $department_DTO)
    {
        $this->department_DTO = $department_DTO;
    }

    public function handle(): array
    {
       return Department::create($this->department_DTO->toArray())->refresh()->toArray();
    }
}