<?php

namespace App\Modules\Department\Services\Table\Filters\Select;

use App\Modules\Office\Model\Office;
use App\Modules\Employee\Model\Employee;
use App\Modules\Department\Model\Department;
use App\Modules\Core\Services\Table\Filters\Select\SelectFilter;

final class FieldByRelation implements SelectFilter
{
    protected string $filter_name;
    protected string $company_id;

    public static array $availableRelationship = [
        'parent_department' => [
            'model' => Department::class,
            'field' => 'name',
        ],
        'manager' => [
            'model' => Employee::class,
            'field' => 'first_name',
        ],
        'office' => [
            'model' => Office::class,
            'field' => 'name',
        ],
    ];

    protected array $valueForSelect;
    protected array $namesForSelect;

    public function __construct(string $filter_name, string $company_id)
    {
        $this->filter_name = $filter_name;
        $this->company_id = $company_id;

        $this->getValueForSelect();
        $this->getNamesForSelect();
    }

    /**
     * This function prepare names of variants for filter select.
     */
    public function getNamesForSelect(): void
    {
        $builder = (New self::$availableRelationship[$this->filter_name]['model']);
        $builder = $builder->whereIn('id', $this->valueForSelect);
        $builder = $builder->pluck(self::$availableRelationship[$this->filter_name]['field']);

        $this->namesForSelect = $builder->all();
    }

    /***
     * This function prepare values of variants for filter select.
     */
    public function getValueForSelect(): void
    {
        $column = $this->filter_name.'_id';

        $this->valueForSelect = Department::query()
            ->where('company_id', '=', $this->company_id)
            ->where($column, '!=', null)
            ->groupBy($column)
            ->pluck($column)
            ->all();
    }

    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function getPrepareData(): array
    {
       $result = [];

       foreach ($this->valueForSelect as $key => $value) {
           $result[] = [
               'value' => $value,
               'name' => $this->namesForSelect[$key],
           ];
       }

       return $result;
    }
}