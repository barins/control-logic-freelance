<?php

namespace App\Modules\Department\Services\Table\Filters;

use App\Modules\Core\QueryBuilder\HandlerFilter;
use App\Modules\Core\Services\Table\Filters\AllFilters as Filters;

final class AllFilters extends Filters
{
   public function get(): array
   {
       return [
           HandlerFilter::register('Name', $this->select(), 'name'),
           HandlerFilter::register('Manager', $this->select(), 'manager_id', 'manager'),
           HandlerFilter::register('Office', $this->select(), 'office_id', 'office'),
           HandlerFilter::register('Parent department', $this->select(), 'parent_department_id', 'parent_department'),
           HandlerFilter::register('Number of employees', $this->range(), 'total_number_of_employees'),
           HandlerFilter::register('Total number of projects', $this->range(), 'total_number_of_projects'),
           HandlerFilter::register('Current monthly income', $this->range(), 'current_monthly_income'),
           HandlerFilter::register('Current monthly costs', $this->range(), 'current_monthly_costs'),
           HandlerFilter::register('Current month profit', $this->range(), 'current_month_profit'),
           HandlerFilter::register('Current month profit', $this->range(), 'previous_month_profit'),
           HandlerFilter::register('Previous month profit', $this->range(), 'previous_month_profit'),
           HandlerFilter::register('Annual income', $this->range(), 'annual_income'),
           HandlerFilter::register('Annual costs', $this->range(), 'annual_costs'),
           HandlerFilter::register('Total income', $this->range(), 'total_income'),
           HandlerFilter::register('Total costs', $this->range(), 'total_costs'),
           HandlerFilter::register('Created at', $this->datePicker(), 'created_at'),
           HandlerFilter::register('Updated at', $this->datePicker(), 'updated_at'),
       ];
   }
}