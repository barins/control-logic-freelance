<?php

namespace App\Modules\Department\Services\Table\Sort;

use Spatie\QueryBuilder\Sorts\Sort;
use Illuminate\Database\Eloquent\Builder;

final class ManagerSort implements Sort
{
    public function __invoke(Builder $query, $descending, string $property) : Builder
    {
        return $query
            ->join('employees', 'employees.id', '=', 'departments.manager_id')
            ->select('departments.*')
            ->orderBy('employees.first_name', $descending ? 'desc' : 'asc');
    }
}