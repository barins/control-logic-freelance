<?php

namespace App\Modules\Department\Services\Table\Handlers;

use App\Modules\Department\Services\Table\Filters\AllFilters;
use App\Modules\Core\Services\Table\Handler\TableHandler;

final class GetColumns extends TableHandler
{
    public function __construct()
    {
        parent::__construct(New AllFilters);
    }

    public function handle(): array
    {
        return $this->prepareFiltersForClient();
    }
}