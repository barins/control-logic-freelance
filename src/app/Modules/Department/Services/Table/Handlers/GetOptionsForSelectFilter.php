<?php

namespace App\Modules\Department\Services\Table\Handlers;

use App\Modules\Core\Services\Handler;
use App\Modules\Department\Services\Table\Filters\Select\FieldByRelation;
use App\Modules\Department\Services\Table\Filters\Select\FieldInTable;

final class GetOptionsForSelectFilter implements Handler
{
    protected string $filter_name;
    protected string $company_id;

    public function __construct(string $filter_name, string $company_id)
    {
        $this->filter_name = $filter_name;
        $this->company_id = $company_id;
    }

    public function handle(): array
    {
        if($this->fieldThroughRelation()) {
            $select_filter = New FieldByRelation($this->filter_name, $this->company_id);
        } else {
            $select_filter = New FieldInTable($this->filter_name, $this->company_id);
        }

        return $select_filter->getPrepareData();
    }

    protected function fieldThroughRelation(): bool
    {
        return isset(FieldByRelation::$availableRelationship[$this->filter_name]['model']);
    }

}