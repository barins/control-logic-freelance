<?php

namespace App\Modules\Department\Services\Table\Handlers;

use App\Modules\Core\Services\Table\Handler\TableHandler;
use App\Modules\Department\Services\Table\Filters\AllFilters;

final class GetTableStructure extends TableHandler
{
    public function __construct()
    {
        parent::__construct(new AllFilters);
    }

    public function handle(): array
    {
        return [
            'Info' => [
                [
                    'title'      => 'Name',
                    'identifier' => 'name',
                ],
                [
                    'title'      => 'Manager',
                    'identifier' => 'manager',
                ],
                [
                    'title'      => 'Office',
                    'identifier' => 'office',
                ],
                [
                    'title'      => 'Parent department',
                    'identifier' => 'parent_department',
                ],
                [
                    'title'      => 'Number of employees',
                    'identifier' => 'total_number_of_employees',
                ],
                [
                    'title'      => 'Total number of projects',
                    'identifier' => 'total_number_of_projects',
                ]

            ],
            'Cost' => [
                [
                    'title'      => 'Current month costs',
                    'identifier' => 'current_monthly_costs',
                ],
                [
                    'title'      => 'Annual costs',
                    'identifier' => 'annual_costs',
                ],
                [
                    'title'      => 'Total costs',
                    'identifier' => 'total_costs',
                ],
            ],
            'Income' => [
                [
                    'title'      => 'Current month income',
                    'identifier' => 'current_monthly_income',
                ],
                [
                    'title'      => 'Annual income',
                    'identifier' => 'annual_income',
                ],
                [
                    'title'      => 'Total income',
                    'identifier' => 'total_income',
                ],
            ],
            'Profit' => [
                [
                    'title'      => 'Current month profit',
                    'identifier' => 'current_month_profit',
                ],
                [
                    'title'      => 'Previous month profit',
                    'identifier' => 'previous_month_profit',
                ],
                [
                    'title'      => 'Previous month growth %',
                    'identifier' => 'previous_month_growth'
                ]
            ],
        ];
    }
}