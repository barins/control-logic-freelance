<?php

namespace App\Modules\Department\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Modules\Department\Model\Department;

class OwnerDepartment implements Rule
{
    private string $company_id;

    public function __construct(string $company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  string|array  $department_id
     * @return bool
     */
    public function passes($attribute, $department_id): bool
    {
        try {
            $builder = Department::where('company_id', $this->company_id);

            if(is_array($department_id)) {
                $builder = $builder->whereIn('id', $department_id);

                $access = $builder->count() === count($department_id);
            } else {
                $builder = $builder->where('id', $department_id);

                $access = $builder->exists();
            }
        } catch (\Exception $exception) {
            $access = false;
        }

        return $access;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.owner');
    }
}
