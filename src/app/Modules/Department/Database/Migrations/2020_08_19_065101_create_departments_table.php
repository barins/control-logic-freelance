<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('departments', static function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->uuid('office_id');
            $table->uuid('parent_department_id')->nullable();
            $table->integer('total_number_of_employees')->default(0);
            $table->integer('total_number_of_projects')->default(0);
            $table->double('current_monthly_income')->default(0);
            $table->double('current_monthly_costs')->default(0);
            $table->double('current_month_profit')->default(0);
            $table->double('previous_month_profit')->default(0);
            $table->double('annual_income')->default(0);
            $table->double('annual_costs')->default(0);
            $table->double('total_income')->default(0);
            $table->double('total_costs')->default(0);
            $table->uuid('company_id');
            $table->timestamps();

            $table->foreign('office_id')->references('id')->on('offices')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });

        Schema::table('departments', static function (Blueprint $table) {
            $table->foreign('parent_department_id')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('departments');
    }
}
