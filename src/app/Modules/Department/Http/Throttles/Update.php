<?php

namespace App\Modules\Department\Http\Throttles;

use App\Modules\Core\Throttles\Throttle;

class Update extends Throttle
{
    public string $key = 'name';
    public int $maxAttempts = 5;
    public int $decaySeconds = 60;
}