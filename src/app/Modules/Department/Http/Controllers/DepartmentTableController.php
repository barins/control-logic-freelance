<?php

namespace App\Modules\Department\Http\Controllers;

use App\Modules\Company\Http\Requests\OwnerCompanyRequest;
use Illuminate\Http\JsonResponse;
use App\Modules\Core\Http\Controllers\Controller;
use App\Modules\Department\Services\Table\TableService;
use App\Modules\Department\Http\Requests\Table\GetOptionsForSelectFilterRequest;

/**
 * Class DepartmentTableController
 *
 * @package App\Modules\Department\Http\Controllers
 */
class DepartmentTableController extends Controller
{
    protected TableService $service;

    public function __construct()
    {
        $this->service = new TableService();
    }

    /**
     * Get departments.
     *
     * Including filters.
     *
     * @param OwnerCompanyRequest $request
     *
     * @return JsonResponse
     */
    public function getDepartments(OwnerCompanyRequest $request): JsonResponse
    {
        $departments = $this->service->getItems($request->get('company_id'));

        return $this->response('Departments', compact('departments'));
    }

    /**
     * Get the structure of the table
     *
     * Including filters.
     *
     * @return JsonResponse
     */
    public function getTableStructure(): JsonResponse
    {
        $structure = $this->service->getTableStructure();

        return $this->response('Departments table structure', compact('structure'));
    }

    /**
     * Get all columns for building departments list table.
     *
     * @return JsonResponse
     */
    public function getColumns(): JsonResponse
    {
        $columns = $this->service->getColumns();

        return $this->response('Columns for building department list table', compact('columns'));
    }

    /**
     * @param  GetOptionsForSelectFilterRequest  $request
     *
     * @return JsonResponse
     */
    public function getOptionsForSelectFilter(GetOptionsForSelectFilterRequest $request): JsonResponse
    {
        $options = $this->service->getOptionsForSelectFilter($request->get('filter_name'), $request->get('company_id'));

        return $this->response('Options for select filter', compact('options'));

    }
}
