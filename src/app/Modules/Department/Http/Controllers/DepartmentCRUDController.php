<?php

namespace App\Modules\Department\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Modules\Department\DTO\DepartmentDTO;
use App\Modules\Core\Http\Controllers\Controller;
use App\Modules\Department\Services\CRUD\CRUDService;
use App\Modules\Department\Services\Select\SelectService;
use App\Modules\Department\Http\Requests\CRUD\{CreateDepartmentRequest,
    GetDepartmentRequest,
    UpdateDepartmentRequest,
    DeleteDepartmentRequest,
    GetOptionsForSelectRequest};

/**
 * Class DepartmentCRUDController
 *
 * @package App\Modules\Department\Http\Controllers
 */
class DepartmentCRUDController extends Controller
{
    protected CRUDService $service;

    public function __construct()
    {
        $this->service = New CRUDService();
    }

    /**
     * Get Department.
     *
     * @param  GetDepartmentRequest  $request
     *
     * @return JsonResponse
     */
    public function index(GetDepartmentRequest $request): JsonResponse
    {
        $data = $this->service->get($request->input('uuid'));

        return $this->response("Data of department id:{$request->input('uuid')}", ['department' => $data['department']]);
    }

    /**
     * @param  CreateDepartmentRequest  $request
     * 
     * @return JsonResponse
     */
    public function create(CreateDepartmentRequest $request): JsonResponse
    {
        $department = DepartmentDTO::fromArray($request->validated());

        $department = $this->service->create($department);

        return $this->response('Department successfully added', compact('department'));
    }


    /**
     * @param  UpdateDepartmentRequest  $request
     *
     * @return JsonResponse
     */
    public function update(UpdateDepartmentRequest $request): JsonResponse
    {
        $department = DepartmentDTO::fromArray($request->validated());

        $department = $this->service->update($request->input('uuid'), $department);

        return $this->response('Department successfully updated', compact('department'));
    }

    /**
     * @param  DeleteDepartmentRequest  $request
     *
     * @return JsonResponse
     */
    public function delete(DeleteDepartmentRequest $request): JsonResponse
    {
        $this->service->delete($request->input('uuid'));

        return $this->response('Department deleted');
    }

    /**
     * Get all options for select of department.
     *
     * @param  GetOptionsForSelectRequest  $request
     *
     * @return JsonResponse
     */
    public function getOptionsForSelect(GetOptionsForSelectRequest $request): JsonResponse
    {
        $options = (New SelectService)->getOptions(
            $request->input('name_select'),
            $request->input('parameter', null),
        );

        return $this->response('Options for select: ' . $request->input('name_select'), compact('options'));
    }
}
