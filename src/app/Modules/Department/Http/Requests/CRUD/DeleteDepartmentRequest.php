<?php

namespace App\Modules\Department\Http\Requests\CRUD;

use App\Modules\MasterUser\Rules\OwnerCompany;
use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\Department\Rules\OwnerDepartment;

/**
 * Class DeleteDepartmentRequest
 **
 * @package App\Modules\Department\Http\Requests\CRUD
 */
class DeleteDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->merge(['uuid' => $this->segment(4)]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        return [
            'company_id' => ['required', New OwnerCompany],
            'uuid' => ['required', 'uuid', New OwnerDepartment($this->company_id)],
        ];
    }
}
