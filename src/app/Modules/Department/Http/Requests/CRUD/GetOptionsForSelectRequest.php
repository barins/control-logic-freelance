<?php

namespace App\Modules\Department\Http\Requests\CRUD;

use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\MasterUser\Rules\OwnerCompany;
use Illuminate\Validation\Rule;

/**
 * Class GetOptionsForSelectRequest
 *
 * @property-read string name_select
 * @property-read string parameter
 * @property-read string company_id
 *
 * @package App\Modules\Office\Http\Requests
 */
class GetOptionsForSelectRequest extends FormRequest
{
    /**
     * Available selects for CRUD 'department'.
     */
    protected array $availableSelects = [
        'manager',
        'office',
        'department',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
       return [
           'name_select' => ['required', 'string', Rule::in($this->availableSelects)],
           'parameter' => ['uuid', 'required', 'exists:companies,id'],
           'company_id' => ['uuid', New OwnerCompany],
       ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name_select.in' => [
                'description' => __('validation.in'),
                'available options' => $this->availableSelects,
            ],
        ];
    }
}
