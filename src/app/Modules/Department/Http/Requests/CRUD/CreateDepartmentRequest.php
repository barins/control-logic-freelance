<?php

namespace App\Modules\Department\Http\Requests\CRUD;

use App\Modules\Office\Rules\OwnerOffice;
use App\Modules\Employee\Rules\OwnerEmployee;
use App\Modules\MasterUser\Rules\OwnerCompany;
use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\Department\Rules\OwnerDepartment;
use App\Modules\Department\Http\Throttles\Create;

/**
 * Class CreateDepartmentRequest
 *
 * @method setDepartmentCreateRules()
 *
 * @package App\Modules\Department\Http\Requests\CRUD
 */
class CreateDepartmentRequest extends FormRequest
{
    public Create $throttle;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->registerThrottle();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        $this->setDepartmentCreateRules();

        return [
            'company_id' => [New OwnerCompany],
            'office_id' => [New OwnerOffice($this->company_id)],
            'manager_id' => [New OwnerEmployee($this->company_id)],
            'parent_department_id' => [New OwnerDepartment($this->company_id)]
        ];
    }

    protected function registerThrottle(): void
    {
        $throttle = new Create($this);

        $throttle->check();

        $throttle->incrementAttempts();

        $this->throttle = $throttle;
    }
}
