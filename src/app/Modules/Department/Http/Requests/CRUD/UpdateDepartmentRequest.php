<?php

namespace App\Modules\Department\Http\Requests\CRUD;

use App\Modules\Office\Rules\OwnerOffice;
use App\Modules\Employee\Rules\OwnerEmployee;
use App\Modules\MasterUser\Rules\OwnerCompany;
use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\Department\Http\Throttles\Update;
use App\Modules\Department\Rules\OwnerDepartment;

/**
 * Class UpdateDepartmentRequest
 *
 * @method setDepartmentCreateRules()
 *
 * @package App\Modules\Department\Http\Requests\CRUD
 */
class UpdateDepartmentRequest extends FormRequest
{
    public Update $throttle;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->merge(['uuid' => $this->segment(4)]);

        $this->registerThrottle();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        $this->setDepartmentCreateRules();

        return [
            'company_id' => [New OwnerCompany],
            'uuid' => [New OwnerDepartment($this->company_id)],
            'office_id' => [New OwnerOffice($this->company_id)],
            'manager_id' => [New OwnerEmployee($this->company_id)],
            'parent_department_id' => [New OwnerDepartment($this->company_id)]
        ];
    }

    protected function registerThrottle(): void
    {
        $throttle = new Update($this);

        $throttle->check();

        $throttle->incrementAttempts();

        $this->throttle = $throttle;
    }
}
