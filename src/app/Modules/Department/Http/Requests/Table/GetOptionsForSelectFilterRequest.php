<?php

namespace App\Modules\Department\Http\Requests\Table;

use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\MasterUser\Rules\OwnerCompany;
use Illuminate\Validation\Rule;

class GetOptionsForSelectFilterRequest extends FormRequest
{
    /**
     * Select filters available for table 'department'.
     */
    protected array $availableFilters = [
        'name',
        'manager',
        'office',
        'parent_department'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        return [
            'company_id' => ['required', 'uuid', New OwnerCompany],
            'filter_name' => ['required', Rule::in($this->availableFilters)]
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'filter_name.in' => [
                'description' => __('validation.in'),
                'available options' => $this->availableFilters,
            ]
        ];
    }
}
