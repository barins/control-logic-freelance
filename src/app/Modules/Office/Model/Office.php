<?php

namespace App\Modules\Office\Model;

use ScoutElastic\Searchable;
use App\Modules\Core\Model\City;
use App\Modules\Core\Model\Country;
use App\Modules\Core\Traits\UsesUuid;
use App\Modules\Core\Model\BaseModel;
use App\Modules\Company\Model\Company;
use App\Modules\Employee\Model\Employee;
use App\Modules\Office\Presenter\OfficePresenter;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Modules\Office\ElasticIndexConfigurator\OfficeIndexConfigurator;

/**
 * Class Office
 *
 * @property string $id
 *
 * @package App\Modules\Office\Model
 */
class Office extends BaseModel
{
    use UsesUuid;
    use Searchable;

    protected string $presenter = OfficePresenter::class;

    protected $indexConfigurator = OfficeIndexConfigurator::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'country_id',
        'city_id',
        'company_id',
        'address',
        'zipcode',
        'floor_area',
        'floor',
        'number_of_floors',
        'total_number_of_employees',
        'total_salary_costs',
        'schedule_work',
        'owned_office',
        'total_rental_costs',
        'total_rental_costs_currency',
        'total_household_costs',
        'total_communal_costs',
        'total_unexpected_costs',
        'monthly_rent_payment_date',
        'total_income',
        'manager_id',
        'total_number_of_projects',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'schedule_work' => 'json',
    ];

    protected $mapping = [
        'properties' => [

        ]
    ];

    /**
     * Get country for the office.
     */
    public function country(): hasOne
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    /**
     * Get city for the office.
     */
    public function city(): hasOne
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    /**
     * Get company for the office.
     */
    public function companies(): HasOne
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    /**
     * Get manager for the office.
     */
    public function manager(): HasOne
    {
        return $this->hasOne(Employee::class, 'id', 'manager_id');
    }
}
