<?php

namespace App\Modules\Office\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('office', 'Resources/Lang', 'app'), 'office');
        $this->loadViewsFrom(module_path('office', 'Resources/Views', 'app'), 'office');
        $this->loadMigrationsFrom(module_path('office', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('office', 'Config', 'app'));
            $this->mergeConfigFrom(module_path('office', 'Validation') . '/validation-rules.php', 'validation-rules');
        }
        $this->loadFactoriesFrom(module_path('office', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
