<?php

namespace App\Modules\Office\Rules;

use App\Modules\Office\Model\Office;
use Illuminate\Contracts\Validation\Rule;

class OwnerOffice implements Rule
{
    private string $company_id;

    public function __construct(string $company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  string|array  $office_id
     * @return bool
     */
    public function passes($attribute, $office_id): bool
    {
        try {
            $builder = Office::where('company_id', $this->company_id);

            if(is_array($office_id)) {
                $builder = $builder->whereIn('id', $office_id);

                $access = $builder->count() === count($office_id);
            } else {
                $builder = $builder->where('id', $office_id);

                $access = $builder->exists();
            }
        } catch (\Exception $exception) {
            $access = false;
        }

        return $access;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.owner');
    }
}
