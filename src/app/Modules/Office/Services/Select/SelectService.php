<?php

namespace App\Modules\Office\Services\Select;

use App\Modules\Core\Services\Select\SelectService as BaseSelectService;

final class SelectService extends BaseSelectService
{
  public string $moduleName = 'Office';
}