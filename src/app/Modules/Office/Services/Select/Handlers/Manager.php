<?php

namespace App\Modules\Office\Services\Select\Handlers;

use Illuminate\Support\Facades\DB;
use App\Modules\Employee\Model\Employee;
use App\Modules\Core\Services\Select\Handlers\SelectHandler;

final class Manager implements SelectHandler
{
    protected string $company_id;

    public function __construct(string $company_id)
    {
        $this->company_id = $company_id;
    }

    public function handle(): array
    {
        $builder = Employee::disablePresenter();
        $builder = $builder->where('company_id', $this->company_id);
        $builder = $builder->select(DB::raw("concat(first_name, ' ', last_name) as name, id as value"));
        $builder = $builder->get();

        return $builder->toArray();
    }
}