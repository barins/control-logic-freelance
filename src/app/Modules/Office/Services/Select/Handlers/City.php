<?php

namespace App\Modules\Office\Services\Select\Handlers;

use App\Modules\Core\Model\City as ModelCity;
use App\Modules\Core\Services\Select\Handlers\SelectHandler;

final class City implements SelectHandler
{
    protected int $country_id;

    public function __construct(int $countryId)
    {
        $this->country_id = $countryId;
    }

    public function handle(): array
    {
        $builder = ModelCity::disablePresenter();
        $builder = $builder->where('country_id', $this->country_id);
        $builder = $builder->select(['name', 'id as value']);
        $builder = $builder->get();

        return $builder->toArray();
    }
}