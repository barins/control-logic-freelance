<?php

namespace App\Modules\Office\Services\Select\Handlers;

use App\Modules\Core\Model\Country as CountryModel;
use App\Modules\Core\Services\Select\Handlers\SelectHandler;

final class Country implements SelectHandler
{
    public function handle(): array
    {
        $builder = CountryModel::disablePresenter();
        $builder = $builder->select(['name', 'id as value']);
        $builder = $builder->get();

        return $builder->toArray();
    }
}