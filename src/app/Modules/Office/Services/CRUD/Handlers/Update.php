<?php

namespace App\Modules\Office\Services\CRUD\Handlers;

use App\Modules\Core\Services\Handler;
use App\Modules\Office\DTO\OfficeDTO;
use App\Modules\Office\Model\Office;

final class Update implements Handler
{
    private string $uuid_office;
    private OfficeDTO $office_DTO;

    protected array $office;

    public function __construct(string $uuid_office, OfficeDTO $office_DTO)
    {
        $this->uuid_office = $uuid_office;
    }

    public function handle(): array
    {
        $this->saveInDB();

        ///...
        return $this->office;
    }

    protected function saveInDB(): void
    {
        $office = Office::where('id', $this->uuid_office)->first();
        $office->fill(
            $this->office_DTO->toArray()
        )->save();

        $this->office = $office->toArray();
    }
}