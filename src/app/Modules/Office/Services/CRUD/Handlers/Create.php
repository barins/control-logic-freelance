<?php

namespace App\Modules\Office\Services\CRUD\Handlers;

use App\Modules\Core\Services\Handler;
use App\Modules\Office\DTO\OfficeDTO;
use App\Modules\Office\Model\Office;

final class Create implements Handler
{
    private OfficeDTO $office_DTO;

    protected array $office;

    public function __construct(OfficeDTO $office_DTO)
    {
        $this->office_DTO = $office_DTO;
    }

    public function handle(): array
    {
        $this->saveInDB();

        ///...
        return $this->office;
    }

    protected function saveInDB(): void
    {
        $office = Office::create($this->office_DTO->toArray());
        $this->office = $office->toArray();
    }
}