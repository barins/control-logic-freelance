<?php

namespace App\Modules\Office\Services\CRUD\Handlers;

use App\Modules\Office\Model\Office;
use App\Modules\Core\Services\Handler;

final class Delete implements Handler
{
    private string $uuid_office;

    protected array $office;

    public function __construct(string $uuid_office)
    {
        $this->uuid_office = $uuid_office;
    }

    public function handle(): bool
    {
       return (bool) Office::where('id', $this->uuid_office)->delete();
    }
}