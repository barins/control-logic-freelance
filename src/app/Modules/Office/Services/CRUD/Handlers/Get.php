<?php

namespace App\Modules\Office\Services\CRUD\Handlers;

use App\Modules\Office\Model\Office;
use App\Modules\Core\Services\Handler;

final class Get implements Handler
{
    private string $id_office;

    protected array $office;

    public function __construct(string $id_office)
    {
        $this->id_office = $id_office;
    }

    public function handle(): array
    {
       return Office::where('id', $this->id_office)->first()->toArray();
    }
}