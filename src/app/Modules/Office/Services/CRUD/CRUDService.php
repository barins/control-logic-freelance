<?php

namespace App\Modules\Office\Services\CRUD;

use App\Modules\Office\DTO\OfficeDTO;
use App\Modules\Office\Services\CRUD\Handlers\{Create, Delete, Get, Update};
final class CRUDService
{
    public function getOffice(string $id_office): array
    {
       return (New Get($id_office))->handle();
    }

    public function create(OfficeDTO $office_DTO): array
    {
       return (New Create($office_DTO))->handle();
    }

    public function update(string $uuid_office, OfficeDTO $office_DTO): array
    {
       return (New Update($uuid_office, $office_DTO))->handle();
    }

    public function delete(string $uuid_office): bool
    {
        return (New Delete($uuid_office))->handle();
    }
}