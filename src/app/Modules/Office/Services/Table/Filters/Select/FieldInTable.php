<?php

namespace App\Modules\Office\Services\Table\Filters\Select;

use App\Modules\Office\Model\Office;
use App\Modules\Core\Services\Table\Filters\Select\SelectFilter;

final class FieldInTable implements SelectFilter
{
    protected string $filter_name;
    protected string $company_id;

    public function __construct(
        string $filter_name,
        string $company_id
    )
    {
        $this->filter_name = $filter_name;
        $this->company_id = $company_id;
    }

    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function getPrepareData(): array
    {
        $builder = Office::disablePresenter();
        $builder = $builder->where('offices.company_id', '=', $this->company_id);
        $builder = $builder->where($this->filter_name, '!=', null);
        $builder = $builder->select("$this->filter_name as value", "$this->filter_name as name");
        $builder = $builder->groupBy('value');

        return $builder->get()->toArray();
    }
}