<?php

namespace App\Modules\Office\Services\Table\Filters\Select;

use App\Modules\Core\Model\City;
use App\Modules\Core\Model\Country;
use App\Modules\Employee\Model\Employee;
use App\Modules\Office\Model\Office;
use App\Modules\Core\Services\Table\Filters\Select\SelectFilter;

final class FieldByRelation implements SelectFilter
{
    protected string $filter_name;
    protected string $company_id;

    public static array $availableRelationship = [
        'city' => [
            'model' => City::class,
            'columnName' => 'name',
        ],
        'country' => [
            'model' => Country::class,
            'columnName' => 'name',
        ],
        'manager' => [
            'model' => Employee::class,
            'columnName' => 'first_name',
        ]
    ];

    protected array $valueForSelect;
    protected array $namesForSelect;

    public function __construct(
        string $filter_name,
        string $company_id
    )
    {
        $this->filter_name = $filter_name;
        $this->company_id = $company_id;

        $this->getValueForSelect();
        $this->getNamesForSelect();
    }

    /**
     * This function prepare names of variants for filter select.
     */
    public function getNamesForSelect(): void
    {
        $builder = (New self::$availableRelationship[$this->filter_name]['model']);
        $builder = $builder->whereIn('id', $this->valueForSelect);
        $builder = $builder->pluck(self::$availableRelationship[$this->filter_name]['columnName']);

        $this->namesForSelect = $builder->all();
    }

    /***
     * This function prepare values of variants for filter select.
     */
    public function getValueForSelect(): void
    {
        $column = $this->filter_name.'_id';

        $this->valueForSelect = Office::query()
            ->where('company_id', '=', $this->company_id)
            ->where($column, '!=', null)
            ->groupBy($column)
            ->pluck($column)
            ->all();
    }

    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function getPrepareData(): array
    {
       $result = [];

       foreach ($this->valueForSelect as $key => $value) {
           $result[] = [
               'value' => $value,
               'name' => $this->namesForSelect[$key],
           ];
       }

       return $result;
    }
}