<?php

namespace App\Modules\Office\Services\Table\Filters;

use App\Modules\Core\QueryBuilder\HandlerFilter;
use App\Modules\Core\Services\Table\Filters\AllFilters as Filters;

final class AllFilters extends Filters
{
   public function get(): array
   {
       return [
           HandlerFilter::register('Name', $this->select(), 'name'),
           HandlerFilter::register('Address', $this->select(), 'address'),
           HandlerFilter::register('Manager', $this->select(), 'manager_id', 'manager'),
           HandlerFilter::register('Country', $this->select(), 'country_id', 'country'),
           HandlerFilter::register('City', $this->select(), 'city_id', 'city'),
           HandlerFilter::register('Owned office', $this->checkBox(), 'owned_office'),
           HandlerFilter::register('Floor', $this->range(), 'floor'),
           HandlerFilter::register('Zipcode', $this->select(), 'zipcode'),
           HandlerFilter::register('Schedule work', $this->datePicker(), 'schedule_work'),
           HandlerFilter::register('Floor area', $this->range(), 'floor_area'),
           HandlerFilter::register('Number of floors', $this->range(), 'number_of_floors'),
           HandlerFilter::register('Total number of employees', $this->range(), 'total_number_of_employees'),
           HandlerFilter::register('Total number of departments', $this->range(), 'total_number_of_departments'),
           HandlerFilter::register('Total salary costs', $this->range(), 'total_salary_costs'),
           HandlerFilter::register('Total household costs', $this->range(), 'total_household_costs'),
           HandlerFilter::register('Monthly rent payment date', $this->range(), 'monthly_rent_payment_date'),
           HandlerFilter::register('Current salary costs', $this->range(), 'current_salary_costs'),  // ?
           HandlerFilter::register('Current monthly rental costs', $this->range(), 'current_monthly_rental_costs'),  // ?
           HandlerFilter::register('Current monthly household costs', $this->range(), 'current_monthly_household_costs'),  // ?
           HandlerFilter::register('Current monthly communal costs', $this->range(), 'current_monthly_communal_costs'),  // ?
           HandlerFilter::register('Current monthly unexpected costs', $this->range(), 'current_monthly_unexpected_costs'),  // ?
           HandlerFilter::register('Monthly office costs', $this->range(), 'monthly_office_costs'),  // ?
           HandlerFilter::register('Current monthly income', $this->range(), 'current_monthly_income'),  // ?
           HandlerFilter::register('Total number of projects', $this->range(), 'total_number_of_projects'),
           HandlerFilter::register('Annual salary costs', $this->range(), 'annual_salary_costs'), // ?
           HandlerFilter::register('Annual rental costs', $this->range(), 'annual_rental_costs'), // ?
           HandlerFilter::register('Annual household costs', $this->range(), 'annual_household_costs'), // ?
           HandlerFilter::register('Annual communal costs', $this->range(), 'annual_communal_costs'), // ?
           HandlerFilter::register('Annual unexpected costs', $this->range(), 'annual_unexpected_costs'), // ?
           HandlerFilter::register('Annual income', $this->range(), 'annual_income'), // ?
           HandlerFilter::register('Annual office costs', $this->range(), 'annual_office_costs'), // ?
           HandlerFilter::register('Current month profit', $this->range(), 'current_month_profit'), // ?
           HandlerFilter::register('Previous month profit', $this->range(), 'previous_month_profit'), // ?
           HandlerFilter::register('Previous month growth %', $this->range(), 'previous_month_growth'), // ?
           HandlerFilter::register('Annual profit', $this->range(), 'annual_profit'), // ?
           HandlerFilter::register('Previous annual profit', $this->range(), 'previous_annual_profit'), // ?
           HandlerFilter::register('Total income', $this->range(), 'total_income'),
           HandlerFilter::register('Total unexpected costs', $this->range(), 'total_unexpected_costs'),
           HandlerFilter::register('Total communal costs', $this->range(), 'total_communal_costs'),
           HandlerFilter::register('Total household costs', $this->range(), 'total_household_costs'),
           HandlerFilter::register('Total rental costs', $this->range(), 'total_rental_costs'),
           HandlerFilter::register('Total salary costs', $this->range(), 'total_salary_costs'),
           HandlerFilter::register('Created at', $this->datePicker(), 'created_at'),
           HandlerFilter::register('Updated at', $this->datePicker(), 'updated_at'),
       ];
   }
}