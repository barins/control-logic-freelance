<?php

namespace App\Modules\Office\Services\Table\Handlers;

use App\Modules\Core\Services\Table\Handler\TableHandler;
use App\Modules\Office\Services\Table\Filters\AllFilters;

final class GetTableStructure extends TableHandler
{
    public function __construct()
    {
        parent::__construct(new AllFilters);
    }

    public function handle(): array
    {
        return [
            'Info' => [
                [
                    'title'      => 'Name',
                    'identifier' => 'name',
                ],
                [
                    'title'      => 'Country',
                    'identifier' => 'country',
                ],
                [
                    'title'      => 'City',
                    'identifier' => 'city',
                ],
                [
                    'title'      => 'Zipcode',
                    'identifier' => 'zipcode',
                ],
                [
                    'title'      => 'Owned office',
                    'identifier' => 'owned_office',
                ],
                [
                    'title'      => 'Manager',
                    'identifier' => 'manager',
                ],
                [
                    'title'      => 'Address',
                    'identifier' => 'address',
                ],
                [
                    'title'      => 'Floor',
                    'identifier' => 'floor',
                ],
                [
                    'title'      => 'Floor area',
                    'identifier' => 'floor_area',
                ],
                [
                    'title'      => 'Number of floors',
                    'identifier' => 'number_of_floors',
                ],
                [
                    'title'      => 'Departments',
                    'identifier' => 'total_number_of_departments',
                ],
                [
                    'title'      => 'Employees',
                    'identifier' => 'total_number_of_employees',
                ],
                [
                    'title'      => 'Projects',
                    'identifier' => 'total_number_of_projects',
                ],
            ],
            'Monthly cost' => [
                [
                    'title'      => 'Unexpected costs',
                    'identifier' => 'current_monthly_unexpected_costs',
                ],
                [
                    'title'      => 'Salary costs',
                    'identifier' => 'current_salary_costs',
                ],
                [
                    'title'      => 'Rental costs',
                    'identifier' => 'current_monthly_rental_costs',
                ],
                [
                    'title'      => 'Household costs',
                    'identifier' => 'current_monthly_household_costs',
                ],
                [
                    'title'      => 'Communal costs',
                    'identifier' => 'current_monthly_communal_costs',
                ],
                [
                    'title'      => 'Office costs',
                    'identifier' => 'monthly_office_costs',
                ],
                [
                    'title'      => 'Rent payment date',
                    'identifier' => 'monthly_rent_payment_date',
                ],
            ],
            'Annual cost' => [
                [
                    'title'      => 'Salary costs',
                    'identifier' => 'annual_salary_costs',
                ],
                [
                    'title'      => 'Household costs',
                    'identifier' => 'annual_household_costs',
                ],
                [
                    'title'      => 'Rental costs',
                    'identifier' => 'annual_rental_costs',
                ],
                [
                    'title'      => 'Communal costs',
                    'identifier' => 'annual_communal_costs',
                ],
                [
                    'title'      => 'Office costs',
                    'identifier' => 'annual_office_costs',
                ],
                [
                    'title'      => 'Unexpected costs',
                    'identifier' => 'annual_unexpected_costs',
                ],
            ],
            'Cost' => [
                [
                    'title'      => 'Unexpected costs',
                    'identifier' => 'total_unexpected_costs',
                ],
                [
                    'title'      => 'Communal costs',
                    'identifier' => 'total_communal_costs',
                ],
                [
                    'title'      => 'Household costs',
                    'identifier' => 'total_household_costs',
                ],
                [
                    'title'      => 'Rental costs',
                    'identifier' => 'total_rental_costs',
                ],
                [
                    'title'      => 'Salary costs',
                    'identifier' => 'total_salary_costs',
                ],
            ],
            'Profit' => [
                [
                    'title'      => 'Current month profit',
                    'identifier' => 'current_month_profit',
                ],
                [
                    'title'      => 'Previous month profit',
                    'identifier' => 'previous_month_profit',
                ],
                [
                    'title'      => 'Annual profit',
                    'identifier' => 'annual_profit',
                ],
                [
                    'title'      => 'Previous annual profit',
                    'identifier' => 'previous_annual_profit',
                ],
                [
                    'title'      => 'Previous month growth %',
                    'identifier' => 'previous_month_growth',
                ],
            ],
            'Schedule work' => [
                [
                    'title'      => 'Schedule work',
                    'identifier' => 'schedule_work',
                ],
            ],
            'Income' => [
                [
                    'title'      => 'Current monthly income',
                    'identifier' => 'current_monthly_income',
                ],
                [
                    'title'      => 'Annual income',
                    'identifier' => 'annual_income',
                ],
                [
                    'title'      => 'Total income',
                    'identifier' => 'total_income',
                ],
            ],
        ];
    }
}