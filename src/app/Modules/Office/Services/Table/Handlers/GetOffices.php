<?php

namespace App\Modules\Office\Services\Table\Handlers;

use App\Modules\Office\Model\Office;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use App\Modules\Office\Services\Table\Filters\AllFilters;
use App\Modules\Core\Services\Table\Handler\TableHandler;
use App\Modules\Office\Services\Table\Sort\ {
    CitySort,
    CountrySort,
    ManagerSort,
};

final class GetOffices extends TableHandler
{
    private string $company_id;
    protected QueryBuilder $builder;

    public function __construct(string $company_id)
    {
        $this->company_id = $company_id;

        parent::__construct(New AllFilters);
    }

    /**
     * Todo: not done
     *
     * @return array
     */
    public function handle(): array
    {
        if(request('query')) {
            return $this->searchByElastic(request('query'));
        }

        $baseQuery = Office::where('company_id', '=', $this->company_id);

        $this->builder = QueryBuilder::for($baseQuery);

        $this->builder = $this->builder->allowedFilters($this->filters);
        $this->builder = $this->builder->allowedSorts($this->getAllowedColumnsForSorts());

        return $this->builder->apiPaginate()->toArray();
    }

    /**
     * For test
     *
     * @param  string  $query
     * @return array
     */
    private function searchByElastic(string $query): array
    {
        return Office::search($query)->apiPaginate()->toArray();
    }

    private function getAllowedColumnsForSorts(): array
    {
        $baseColumn = (New Office)->getFillable();

        $columnByRelation = [
            AllowedSort::custom('country', New CountrySort),
            AllowedSort::custom('city', New CitySort),
            AllowedSort::custom('manager', New ManagerSort),
        ];

        return array_merge($baseColumn, $columnByRelation);
    }
}