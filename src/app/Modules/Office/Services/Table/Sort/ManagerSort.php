<?php

namespace App\Modules\Office\Services\Table\Sort;

use Spatie\QueryBuilder\Sorts\Sort;
use Illuminate\Database\Eloquent\Builder;

final class ManagerSort implements Sort
{
    public function __invoke(Builder $query, $descending, string $property) : Builder
    {
        return $query
            ->join('employees', 'employees.id', '=', 'offices.manager_id')
            ->select('offices.*')
            ->orderBy('employees.first_name', $descending ? 'desc' : 'asc');
    }
}