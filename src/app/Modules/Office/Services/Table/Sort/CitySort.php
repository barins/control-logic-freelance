<?php

namespace App\Modules\Office\Services\Table\Sort;

use Spatie\QueryBuilder\Sorts\Sort;
use Illuminate\Database\Eloquent\Builder;

final class CitySort implements Sort
{
    public function __invoke(Builder $query, $descending, string $property) : Builder
    {
        return $query
            ->join('cities', 'cities.id', '=', 'offices.city_id')
            ->select('offices.*')
            ->orderBy('cities.name', $descending ? 'desc' : 'asc');
    }
}