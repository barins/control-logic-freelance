<?php

namespace App\Modules\Office\Services\Table\Sort;

use Spatie\QueryBuilder\Sorts\Sort;
use Illuminate\Database\Eloquent\Builder;

final class CountrySort implements Sort
{
    public function __invoke(Builder $query, $descending, string $property) : Builder
    {
        return $query
            ->join('countries', 'countries.id', '=', 'offices.country_id')
            ->select('offices.*')
            ->orderBy('countries.name', $descending ? 'desc' : 'asc');
    }
}