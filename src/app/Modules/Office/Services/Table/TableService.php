<?php

namespace App\Modules\Office\Services\Table;

use App\Modules\Core\Services\Table\TableService as Table;
use App\Modules\Office\Services\Table\Handlers\ {
    GetOffices,
    GetColumns,
    GetTableStructure,
    GetOptionsForSelectFilter
};

/**
 * Class TableService
 *
 * @package App\Modules\Office\Services\Table
 */
final class TableService implements Table
{
    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function getItems(string $company_id): array
    {
        return (New GetOffices($company_id))->handle();
    }

    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function getTableStructure(): array
    {
        return (New GetTableStructure)->handle();
    }

    /**
     * {@inheritDoc}
     *
     * @return array
     */
    public function getColumns(): array
    {
        return (New GetColumns)->handle();
    }

    /**
     * {@inheritDoc}
     *
     * @param  string  $filter_name
     *
     * @return array
     */
    public function getOptionsForSelectFilter(string $filter_name, string $company_id): array
    {
        return (New GetOptionsForSelectFilter($filter_name, $company_id))->handle();
    }
}