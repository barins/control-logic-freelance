<?php

namespace App\Modules\Office\DTO;

use App\Modules\Core\Http\Requests\FormRequest;

final class OfficeDTO
{
    private string $name;
    private int $country_id;
    private ?string $manager_id;
    private int $city_id;
    private string $company_id;
    private ?string $address;
    private ?string $zipcode;
    private ?float $floor_area;
    private ?int $floor;
    private ?int $number_of_floors;
    private ?array $schedule_work;
    private ?bool $owned_office;
    private ?string $monthly_rent_payment_date;

    private function __construct(
        string $name,
        int $country_id,
        ?string $manager_id,
        int $city_id,
        string $company_id,
        ?string $address,
        ?string $zipcode,
        ?float $floor_area,
        ?int $floor,
        ?int $number_of_floors,
        ?array $schedule_work,
        ?bool $owned_office,
        ?string $monthly_rent_payment_date
    )
    {
        $this->name = $name;
        $this->country_id = $country_id;
        $this->city_id = $city_id;
        $this->manager_id = $manager_id;
        $this->company_id = $company_id;
        $this->address = $address;
        $this->zipcode = $zipcode;
        $this->floor_area = $floor_area;
        $this->floor = $floor;
        $this->number_of_floors = $number_of_floors;
        $this->schedule_work = $schedule_work;
        $this->owned_office = $owned_office;
        $this->monthly_rent_payment_date = $monthly_rent_payment_date;
    }

    /**
     * @param  FormRequest  $request
     *
     * @return OfficeDTO
     */
    public static function fromRequest(FormRequest $request): OfficeDTO
    {
        $request = $request->validated();

        return new self(
            $request['name'],
            $request['country_id'],
            $request['manager_id'] ?? null,
            $request['city_id'] ?? null,
            $request['company_id'],
            $request['address'] ?? null,
            $request['zipcode'] ?? null,
            $request['floor_area'] ?? null,
            $request['floor'] ?? null,
            $request['number_of_floors'] ?? null,
            $request['schedule_work'] ?? null,
            $request['owned_office'] ?? null,
            $request['monthly_rent_payment_date'] ?? null,
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
      return [
            'name' => $this->name,
            'country_id' => $this->country_id,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
            'address' => $this->address,
            'zipcode' => $this->zipcode,
            'floor_area' => $this->floor_area,
            'floor' => $this->floor,
            'number_of_floors' => $this->number_of_floors,
            'schedule_work' => $this->schedule_work,
            'owned_office' => $this->owned_office,
            'monthly_rent_payment_date' => $this->monthly_rent_payment_date,
        ];
    }

}