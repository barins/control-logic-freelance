<?php

use App\Modules\Core\Validation\Field;

/**
 * Rules for office.
 */

return [
    'office_type' => [
        'name' => ['string'],
        'country_id' => ['int'],
        'company_id' => ['string', 'uuid'],
        'manager_id' => ['string', 'uuid'],
        'city_id' => ['int'],
        'address' => ['string'],
        'floor_area' => ['int'],
        'floor' => ['int'],
        'number_of_floors' => ['int'],
        'owned_office' => ['bool'],
        'schedule_work' => ['array'],
        'monthly_rent_payment_date' => ['string'],
    ],

    'office_create' => mergeArray([
        'name' => ['string', 'required'],
        'country_id' => ['int', 'required', 'exists:countries,id'],
        'city_id' => ['int', 'required'],
        'manager_id' => ['uuid'],
        'company_id' => ['uuid', 'required'],
        'address' => ['string'],
        'zipcode' => ['string'],
        'floor_area' => ['numeric'],
        'floor' => ['int'],
        'number_of_floors' => ['int'],
        'owned_office' => ['bool'],
        'monthly_rent_payment_date' => ['string', "required_if:owned_office,==,true"],
    ], Field::get('schedule_work')),
];