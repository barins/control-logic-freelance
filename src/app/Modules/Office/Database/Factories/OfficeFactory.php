<?php

use Faker\Generator as Faker;
use App\Modules\Office\Model\Office;
use App\Modules\Company\Model\Company;

$factory->define(Office::class, static function(Faker $faker) {
    return [
        'name' => $faker->name,
        'country_id' => 235,
        'city_id' => 1,
        'company_id' => Company::inRandomOrder()->first(),
        'address' => $faker->address,
        'floor_area' => 1,
        'floor' => 1,
        'number_of_floors' => 1,
        'total_number_of_employees' => 0,
        'total_salary_costs' => 0,
        'schedule_work' => [
            'monday' =>
                [
                    'enabled' => true,
                    'from' => '09:00',
                    'to' => '19:00',
                ],
            'tuesday' =>
                [
                    'enabled' => true,
                    'from' => '09:00',
                    'to' => '19:00',
                ],
            'wednesday' =>
                [
                    'enabled' => true,
                    'from' => '09:00',
                    'to' => '19:00',
                ],
            'thursday' =>
                [
                    'enabled' => true,
                    'from' => '09:00',
                    'to' => '19:00',
                ],
            'friday' =>
                [
                    'enabled' => true,
                    'from' => '09:00',
                    'to' => '19:00',
                ],
            'saturday' =>
                [
                    'enabled' => false,
                    'from' => '09:00',
                    'to' => '19:00',
                ],
            'sunday' =>
                [
                    'enabled' => false,
                    'from' => '09:00',
                    'to' => '19:00',
                ],
        ],
        'owned_office' => false,
        'total_rental_costs' => 0,
        'total_rental_costs_currency' => 0,
        'total_household_costs' => 0,
        'total_communal_costs' => 0,
        'total_unexpected_costs' => 0,
        'monthly_rent_payment_date' => now(),
        'total_income' => 0,
        'total_number_of_projects' => 0,
    ];
});
