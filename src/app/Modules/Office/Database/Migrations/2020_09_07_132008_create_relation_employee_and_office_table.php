<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationEmployeeAndOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('offices', static function (Blueprint $table) {
            $table->uuid('manager_id')->unsigned()->index()->nullable();

            $table->foreign('manager_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('offices', static function (Blueprint $table) {
            $table->dropColumn('manager_id');
        });
    }
}
