<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('offices', static function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->bigInteger('country_id')->unsigned()->index();
            $table->bigInteger('city_id')->unsigned()->index();
            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->double('floor_area');
            $table->string('floor')->nullable();
            $table->string('number_of_floors')->nullable();
            $table->integer('total_number_of_employees')->default(0);
            $table->integer('total_salary_costs')->default(0);
            $table->json('schedule_work')->nullable();
            $table->boolean('owned_office')->nullable();
            $table->integer('total_rental_costs')->default(0);
            $table->string('total_rental_costs_currency')->default(0);
            $table->integer('total_household_costs')->default(0);
            $table->integer('total_communal_costs')->default(0);
            $table->integer('total_unexpected_costs')->default(0);
            $table->dateTime('monthly_rent_payment_date')->nullable();
            $table->integer('total_income')->default(0);
            $table->integer('total_number_of_projects')->default(0);
            $table->uuid('company_id');
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('offices');
    }
}
