<?php

namespace App\Modules\Office\ElasticIndexConfigurator;

use ScoutElastic\Migratable;
use ScoutElastic\IndexConfigurator;

class OfficeIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'control_logic_office';

    /**
     * @var array
     */
    protected $settings = [
        'mapping' => [
            'total_fields' => [
                'limit' => 5000
            ]
        ]
    ];
}