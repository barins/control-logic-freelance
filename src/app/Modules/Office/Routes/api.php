<?php

/**
 * Routes for REST-API of office
 */

use App\Modules\Office\Http\Controllers\OfficeController;
use App\Modules\Office\Http\Controllers\OfficeTableController;

Route::group(['middleware' => 'auth:master_user'], static function () {
    Route::get('{uuid}', [OfficeController::class, 'index'])->name('office.index');
    Route::post('create', [OfficeController::class, 'create'])->name('office.create');
    Route::put('{uuid}/update', [OfficeController::class, 'update'])->name('office.update');
    Route::delete('{uuid}', [OfficeController::class, 'delete'])->name('office.delete');

    Route::get('select/options', [OfficeController::class, 'getOptionsForSelect'])->name('office.select.option');

    /**
     * To build the main table for offices
     */
    Route::group(['prefix' => 'table'], static function () {
        Route::get('columns', [OfficeTableController::class, 'getColumns'])->name('offices.table.columns');
        Route::get('structure', [OfficeTableController::class, 'getTableStructure'])->name('offices.table.structure');
        Route::get('filter/select', [OfficeTableController::class, 'getOptionsForSelectFilter'])->name('offices.table.filter.select');
        Route::get('/', [OfficeTableController::class, 'getOffices'])->name('offices.table.get');
    });
});