<?php

namespace App\Modules\Office\Presenter;

use App\Modules\Office\Model\Office;
use Yaro\Presenter\AbstractPresenter;

class OfficePresenter extends AbstractPresenter
{
    /**
     * @var Office $model
     */
    protected $model;

    public $arrayable = [
        'id',
        'name',
        'country',
        'city',
        'address',
        'zipcode',
        'floor_area',
        'floor',
        'number_of_floors',
        'owned_office',
        'total_income',
        'schedule_work',
        'manager',
        'total_number_of_employees',
        'total_number_of_departments',
        'current_salary_costs',
        'current_monthly_rental_costs',
        'current_monthly_household_costs',
        'current_monthly_communal_costs',
        'current_monthly_unexpected_costs',
        'monthly_office_costs',
        'monthly_rent_payment_date',
        'current_monthly_income',
        'total_number_of_projects',
        'total_unexpected_costs',
        'total_communal_costs',
        'total_household_costs',
        'total_rental_costs',
        'total_salary_costs',
        'annual_salary_costs',
        'annual_rental_costs',
        'annual_household_costs',
        'annual_communal_costs',
        'annual_unexpected_costs',
        'annual_income',
        'annual_office_costs',
        'current_month_profit',
        'previous_month_profit',
        'previous_month_growth',
        'annual_profit',
        'previous_annual_profit',
        'currency',
        'created_at',
        'updated_at',
    ];

    public function getCountryPresent(): array
    {
        return $this->__get('country')->toArray();
    }

    public function getCityPresent(): array
    {
        return $this->__get('city')->toArray();
    }

    public function getManagerPresent(): ?array
    {
        return $this->getUnknownProperty('manager');
    }

    public function getTotalNumberOfDepartmentsPresent(): int
    {
        //Test
        return 0;
    }

    public function getCurrentSalaryCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getCurrentMonthlyRentalCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getCurrentMonthlyHouseholdCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getCurrentMonthlyCommunalCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getCurrentMonthlyUnexpectedCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getMonthlyOfficeCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getCurrentMonthlyIncomePresent(): int
    {
        //Test
        return 0;
    }

    public function getAnnualSalaryCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getAnnualRentalCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getAnnualHouseholdCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getAnnualCommunalCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getAnnualUnexpectedCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getAnnualIncomePresent(): int
    {
        //Test
        return 0;
    }

    public function getAnnualOfficeCostsPresent(): int
    {
        //Test
        return 0;
    }

    public function getCurrentMonthProfitPresent(): int
    {
        //Test
        return 0;
    }

    public function getPreviousMonthProfitPresent(): int
    {
        //Test
        return 0;
    }

    public function getPreviousMonthGrowthPresent(): int
    {
        //Test
        return 0;
    }

    public function getAnnualProfitPresent(): int
    {
        //Test
        return 0;
    }

    public function getPreviousAnnualProfitPresent(): int
    {
        //Test
        return 0;
    }

    public function getCurrencyPresent(): string
    {
        //Test
        return 'USD';
    }

    protected function getUnknownProperty(string $modelName): ?array
    {
        $model = $this->__get($modelName);

        if($model) {
            return $model->toArray();
        }

        return null;
    }
}