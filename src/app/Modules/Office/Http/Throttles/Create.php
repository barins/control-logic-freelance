<?php

namespace App\Modules\Office\Http\Throttles;

use App\Modules\Core\Throttles\Throttle;

class Create extends Throttle
{
    public string $key = 'name';
    public int $maxAttempts = 5;
    public int $decaySeconds = 60;
}