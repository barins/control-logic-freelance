<?php

namespace App\Modules\Office\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Modules\Core\Http\Controllers\Controller;
use App\Modules\Office\Services\Table\TableService;
use App\Modules\Company\Http\Requests\OwnerCompanyRequest;
use App\Modules\Office\Http\Requests\Table\GetOptionsForSelectFilterRequest;

/**
 * Class OfficeTableController
 *
 * @package App\Modules\Office\Http\Controllers
 */
class OfficeTableController extends Controller
{
    protected TableService $service;

    public function __construct()
    {
        $this->service = new TableService();
    }

    /**
     * Get offices.
     *
     * Including filters.
     *
     * @param OwnerCompanyRequest $request
     *
     * @return JsonResponse
     */
    public function getOffices(OwnerCompanyRequest $request): JsonResponse
    {
        $office = $this->service->getItems($request->get('company_id'));

        return $this->response('Offices', compact('office'));
    }

    /**
     * Get the structure of the table
     *
     * Including filters.
     *
     * @return JsonResponse
     */
    public function getTableStructure(): JsonResponse
    {
        $structure = $this->service->getTableStructure();

        return $this->response('Offices table structure', compact('structure'));
    }

    /**
     * Get all columns for building office list table.
     *
     * @return JsonResponse
     */
    public function getColumns(): JsonResponse
    {
        $columns = $this->service->getColumns();

        return $this->response('Columns for building office list table', compact('columns'));
    }

    /**
     * @param  GetOptionsForSelectFilterRequest  $request
     *
     * @return JsonResponse
     */
    public function getOptionsForSelectFilter(GetOptionsForSelectFilterRequest $request): JsonResponse
    {
        $options = $this->service->getOptionsForSelectFilter($request->get('filter_name'), $request->get('company_id'));

        return $this->response('Options for select filter', compact('options'));

    }
}
