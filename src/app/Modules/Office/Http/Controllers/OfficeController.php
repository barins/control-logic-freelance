<?php

namespace App\Modules\Office\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Modules\Office\DTO\OfficeDTO;
use App\Modules\Office\Services\CRUD\CRUDService;
use App\Modules\Core\Http\Controllers\Controller;
use App\Modules\Office\Services\Select\SelectService;
use App\Modules\Office\Http\Requests\GetOptionsForSelectRequest;
use App\Modules\Office\Http\Requests\CRUD\{
    UpdateOfficeRequest,
    DeleteOfficeRequest,
    CreateOfficeRequest,
};

/**
 * Class OfficeController
 *
 * @package App\Modules\Office\Http\Controllers
 */
class OfficeController extends Controller
{
    protected CRUDService $service;

    public function __construct()
    {
        $this->service = New CRUDService();
    }

    /**
     * Get office.
     *
     * @param  string  $id_office
     *
     * @return JsonResponse
     */
    public function index(string $id_office): JsonResponse
    {
        $data = $this->service->getOffice($id_office);

        if($data['success']) {
            return $this->response("Data of office id:$id_office", ['office' => $data['office']]);
        }

        return $this->response('Office not found', [], ['office' => "Office id:$id_office doesn't exist"], 400);
    }

    /**
     * Delete office.
     *
     * @param  DeleteOfficeRequest  $request
     *
     * @return JsonResponse
     */
    public function delete(DeleteOfficeRequest $request): JsonResponse
    {
        $uuid_office = $request->input('uuid');

        $success = $this->service->delete($uuid_office);

        if($success) {
            return $this->response("Office deleted");
        }

        return $this->response('Office not found', [], ['office' => "Office id:$uuid_office doesn't exist"], 400);
    }

    /**
     * @param  CreateOfficeRequest  $request
     *
     * Create a new office
     *
     * @return JsonResponse
     */
    public function create(CreateOfficeRequest $request): JsonResponse
    {
        $office = OfficeDTO::fromRequest($request);

        $office = $this->service->create($office);

        return $this->response('Office successfully added', compact('office'));
    }

    /**
     * @param  UpdateOfficeRequest  $request
     *
     * @return JsonResponse
     */
    public function update(UpdateOfficeRequest $request): JsonResponse
    {
        $office = OfficeDTO::fromRequest($request);

        $office = $this->service->update($request->input('uuid'), $office);

        return $this->response('Office successfully updated', compact('office'));
    }

    /**
     * Get all options for select of office.
     *
     * @param  GetOptionsForSelectRequest  $request
     *
     * @return JsonResponse
     */
    public function getOptionsForSelect(GetOptionsForSelectRequest $request): JsonResponse
    {
        $options = (New SelectService)->getOptions(
            $request->input('name_select'),
            $request->input('parameter', null),
        );

        return $this->response('Options for select: ' . $request->input('name_select'), compact('options'));
    }
}
