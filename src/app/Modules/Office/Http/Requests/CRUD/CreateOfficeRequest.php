<?php

namespace App\Modules\Office\Http\Requests\CRUD;

use App\Modules\Core\Rules\OwnerCity;
use App\Modules\Employee\Rules\OwnerEmployee;
use App\Modules\Office\Http\Throttles\Create;
use App\Modules\MasterUser\Rules\OwnerCompany;
use App\Modules\Core\Http\Requests\FormRequest;

/**
 * Class CreateOfficeRequest
 *
 * @method $this setOfficeCreateRules()
 *
 * @package App\Modules\Offect\Http\Requests\CRUD
 */
class CreateOfficeRequest extends FormRequest
{
    public Create $throttle;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->registerThrottle();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        $this->setOfficeCreateRules();

        return [
            'company_id' => [New OwnerCompany],
            'city_id' => [New OwnerCity],
            'manager_id' => [new OwnerEmployee($this->company_id)],
        ];
    }

    protected function registerThrottle(): void
    {
        $throttle = new Create($this);

        $throttle->check();

        $throttle->incrementAttempts();

        $this->throttle = $throttle;
    }
}
