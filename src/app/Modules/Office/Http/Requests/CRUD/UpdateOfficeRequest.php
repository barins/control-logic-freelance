<?php

namespace App\Modules\Office\Http\Requests\CRUD;

use App\Modules\Core\Rules\OwnerCity;
use App\Modules\Office\Rules\OwnerOffice;
use App\Modules\Employee\Rules\OwnerEmployee;
use App\Modules\Office\Http\Throttles\Update;
use App\Modules\MasterUser\Rules\OwnerCompany;
use App\Modules\Core\Http\Requests\FormRequest;

/**
 * Class UpdateOfficeRequest
 *
 * @method $this setOfficeCreateRules()
 *
 * @package App\Modules\Offect\Http\Requests\CRUD
 */
class UpdateOfficeRequest extends FormRequest
{
    public Update $throttle;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->merge(['uuid' => $this->segment(4)]);

        $this->registerThrottle();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        $this->setOfficeCreateRules();

        return [
            'uuid' => ['required', 'uuid', new OwnerOffice($this->company_id)],
            'company_id' => [New OwnerCompany],
            'city_id' => [New OwnerCity],
            'manager_id' => [new OwnerEmployee($this->company_id)],
        ];
    }

    protected function registerThrottle(): void
    {
        $throttle = new Update($this);

        $throttle->check();

        $throttle->incrementAttempts();

        $this->throttle = $throttle;
    }
}
