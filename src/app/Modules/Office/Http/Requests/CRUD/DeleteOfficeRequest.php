<?php

namespace App\Modules\Office\Http\Requests\CRUD;

use App\Modules\Office\Rules\OwnerOffice;
use App\Modules\MasterUser\Rules\OwnerCompany;
use App\Modules\Core\Http\Requests\FormRequest;

/**
 * Class CreateOfficeRequest
 *
 * @package App\Modules\Offect\Http\Requests\CRUD
 */
class DeleteOfficeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->merge(['uuid' => $this->segment(4)]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        return [
            'company_id' => ['required', 'uuid', New OwnerCompany],
            'uuid' => ['required', 'uuid', new OwnerOffice($this->company_id)],
        ];
    }
}
