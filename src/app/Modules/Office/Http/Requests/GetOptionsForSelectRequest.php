<?php

namespace App\Modules\Office\Http\Requests;

use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\MasterUser\Rules\OwnerCompany;
use Illuminate\Validation\Rule;

/**
 * Class GetOptionsForSelectRequest
 *
 * @property-read string name_select
 * @property-read string parameter
 * @property-read string company_id
 *
 * @package App\Modules\Office\Http\Requests
 */
class GetOptionsForSelectRequest extends FormRequest
{
    /**
     * Available selects for CRUD 'office'.
     */
    protected array $availableSelects = [
        'country',
        'city',
        'manager',
    ];

    protected array $rulesForParameterOfSelect = [
        'city' => ['int', 'required', 'exists:countries,id'],
        'manager' => ['uuid', 'required', 'exists:companies,id'],
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
       $rules =  [
           'name_select' => ['required', 'string', Rule::in($this->availableSelects)],
           'company_id' => ['uuid', New OwnerCompany],
       ];

       if(isset($this->rulesForParameterOfSelect[$this->name_select])) {
           $rules['parameter'] = $this->rulesForParameterOfSelect[$this->name_select];
       }

       return $rules;
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name_select.in' => [
                'description' => __('validation.in'),
                'available options' => $this->availableSelects,
            ],
        ];
    }
}
