<?php

namespace App\Modules\MasterUser\Presenter;

use Yaro\Presenter\AbstractPresenter;
use App\Modules\MasterUser\Model\MasterUser;

/**
 * Class CompanyPresenter
 *
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $confirmation_code
 * @property boolean $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class MasterUserPresenter extends AbstractPresenter
{
    /**
     * @var MasterUser $model
     */
    protected $model;

    protected $arrayable = [
        'id',
        'full_name',
        'first_name',
        'last_name',
        'companies',
        'email',
        'is_active',
    ];

    public function getFullNamePresent(): string
    {
        return "{$this->model->first_name} {$this->model->last_name}";
    }

    public function getCompaniesPresent(): array
    {
        return $this->model->companies->toArray();
    }
}