<?php

namespace App\Modules\MasterUser\Presenter;

use Yaro\Presenter\AbstractPresenter;
use App\Modules\MasterUser\Model\MasterUser;

/**
 * Class ResetPasswordRequestPresenter
 *
 * @property string code
 * @property MasterUser master_user
 *
 */
class ResetPasswordRequestPresenter extends AbstractPresenter
{
    protected $arrayable = [
        'email',
        'user_name',
        'confirmation_code',
    ];

    public function getEmailPresent(): string
    {
        return $this->model->master_user->email;
    }

    public function getUserNamePresent(): string
    {
        return $this->model->master_user->first_name;
    }

    public function getConfirmationCodePresent(): string
    {
        return $this->model->code;
    }
}