<?php

namespace App\Modules\MasterUser\Providers;

use App\Modules\MasterUser\Model\MasterUser;
use Caffeinated\Modules\Support\ServiceProvider;
use App\Modules\MasterUser\Observers\MasterUserObserver;
use Caffeinated\Modules\Exceptions\ModuleNotFoundException;
use App\Modules\MasterUser\Services\Workspace\Status\Repositories\TeacherWorkspace;
use App\Modules\MasterUser\Services\Workspace\Status\Repositories\TeacherWorkspaceRepository;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     *
     * @throws ModuleNotFoundException
     */
    public function boot(): void
    {
        $this->loadTranslationsFrom(module_path('master_user', 'Resources/Lang', 'app'), 'master_user');
        $this->loadViewsFrom(module_path('master_user', 'Resources/Views', 'app'), 'master_user');
        $this->loadMigrationsFrom(module_path('master_user', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('master_user', 'Config', 'app'));
            $this->mergeConfigFrom(module_path('master_user', 'Validation') . '/validation-rules.php', 'validation-rules');
        }
        $this->loadFactoriesFrom(module_path('master_user', 'Database/Factories', 'app'));

        $this->registerObservers();
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->register(RouteServiceProvider::class);

        $this->app->bind(TeacherWorkspace::class, TeacherWorkspaceRepository::class);
    }

    protected function registerObservers(): void
    {
        MasterUser::observe(MasterUserObserver::class);
    }
}
