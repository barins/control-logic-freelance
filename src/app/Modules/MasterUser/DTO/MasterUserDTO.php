<?php

namespace App\Modules\MasterUser\DTO;

/**
 * Class MasterUserDTO
 *
 * @package App\Modules\MasterUser\DTO
 */
class MasterUserDTO
{
    private ?int $id;
    private string $email;
    private string $password;
    private string $first_name;
    private string $last_name;
    private bool $is_active;
    private ?string $created_at;
    private ?string $updated_at;

    private function __construct(
        ?int $id,
        string $email,
        string $password,
        string $first_name,
        string $last_name,
        bool $is_active,
        ?string $created_at,
        ?string $updated_at
    )
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->is_active = $is_active;
        $this->created_at = $created_at;
        $this->updated_at = $updated_at;
    }

    /**
     * @param  array  $data
     *
     * @return MasterUserDTO
     */
    public static function fromArray(array $data): MasterUserDTO
    {
       return new self(
           $data['id'] ?? null,
           $data['email'],
           $data['password'],
           $data['first_name'],
           $data['last_name'],
           $data['is_active'] ?? false,
           $data['created_at'] ?? null,
           $data['updated_at'] ?? null
       );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [];

        if(! is_null($this->id)) {
            $data['id'] = $this->id;
        }

        if(! is_null($this->created_at)) {
            $data['created_at'] = $this->created_at;
        }

        if(! is_null($this->updated_at)) {
            $data['updated_at'] = $this->updated_at;
        }

        return array_merge($data, [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'password' => $this->password,
            'is_active' => $this->is_active,
        ]);
    }
}