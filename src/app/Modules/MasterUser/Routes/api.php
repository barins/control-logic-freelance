<?php

use App\Modules\Office\Http\Controllers\OfficeController;
use App\Modules\MasterUser\Http\Controllers\Workspace\BaseWorkspaceController;
use App\Modules\MasterUser\Http\Controllers\Subscription\PlanController;
use App\Modules\MasterUser\Http\Controllers\Subscription\SubscriptionController;
use App\Modules\MasterUser\Http\Controllers\AuthMasterUserController as AuthController;

/**
 * Routes for REST-API of master-user
 */

Route::group(['prefix' => 'auth'], static function () {
    Route::post('sign-up', [AuthController::class, 'register'])->name('master_user.sign-up');
    Route::post('activation', [AuthController::class, 'confirm'])->name('master_user.activation');
    Route::post('sign-in', [AuthController::class, 'login'])->name('master_user.sign-in');
    Route::post('logout', [AuthController::class, 'logout'])->name('master_user.logout');
    Route::post('refresh', [AuthController::class, 'refresh'])->name('master_user.refresh');
    Route::get('me', [AuthController::class, 'me'])->name('master_user.index');

    Route::post('password-reset-request', [AuthController::class, 'passwordResetRequest'])->name('master_user.password-reset-request');
    Route::post('password-reset', [AuthController::class, 'passwordReset'])->name('master_user.password-reset');
});

Route::group(['middleware' => 'auth:master_user'], static function () {
    Route::group(['prefix' => 'subscription'], static function () {
        Route::get('plans', [PlanController::class, 'get'])->name('subscription.plans.index');

        Route::post('pay', [SubscriptionController::class, 'paySubscription'])->name('subscription.pay');
    });

    Route::group(['prefix' => 'workspace-teacher'], static function () {
        Route::get('', [BaseWorkspaceController::class, 'setStatus'])->name('workspace-teacher.teacher.status');
        Route::post('initialization', [BaseWorkspaceController::class, 'initialization'])->name('workspace-teacher.teacher.initialization');
        Route::post('activate', [BaseWorkspaceController::class, 'activate'])->name('workspace-teacher.teacher.activate');
        Route::post('deactivate', [BaseWorkspaceController::class, 'deactivate'])->name('workspace-teacher.teacher.deactivate');
    });
});

