<?php

namespace App\Modules\MasterUser\Services\Auth;

use App\Modules\Company\DTO\CompanyDTO;
use Illuminate\Auth\AuthenticationException;
use App\Modules\MasterUser\DTO\MasterUserDTO;
use App\Modules\MasterUser\Services\Auth\Handlers\Get;
use App\Modules\MasterUser\Services\Auth\Handlers\Login;
use App\Modules\MasterUser\Services\Auth\Handlers\Logout;
use App\Modules\MasterUser\Services\Auth\Handlers\Confirm;
use App\Modules\MasterUser\Services\Auth\Handlers\Register;
use App\Modules\MasterUser\Services\Auth\Handlers\RefreshToken;
use App\Modules\MasterUser\Services\Auth\Handlers\ResetPassword;
use App\Modules\MasterUser\Services\Auth\Handlers\RequestResetPassword;

/**
 * Class AuthService
 *
 * @package App\Modules\MasterUser\Services\Auth
 */
final class AuthService
{
    /**
     * @param  MasterUserDTO  $master_user
     * @param  CompanyDTO     $company
     *
     * @return array
     */
    public function register(MasterUserDTO $master_user, CompanyDTO $company): array
    {
       return (New Register($master_user, $company))->handle();
    }

    /**
     * @param  string  $email
     * @param  string  $confirmation_code
     *
     * @return bool
     */
    public function confirm(string $email, string $confirmation_code): bool
    {
        return (New Confirm($email, $confirmation_code))->handle();
    }

    /**
     * @param  string  $email
     * @param  string|null  $password
     *
     * @return array
     */
    public function login(string $email, string $password = null): array
    {
        return (New Login($email, $password))->handle();
    }

    /**
     * @return string
     */
    public function refreshToken(): string
    {
        return (New RefreshToken())->handle();
    }

    /**
     * @param  string  $email
     *
     * @return bool|array
     */
    public function requestResetPassword(string $email)
    {
        return (new RequestResetPassword($email))->handle();
    }

    /**
     * @param  string  $email
     * @param  string  $code
     * @param  string  $new_password
     *
     * @return bool
     */
    public function resetPassword(string $email, string $code, string $new_password): bool
    {
        return (new ResetPassword($email, $code, $new_password))->handle();
    }

    /**
     * @return array
     *
     * @throws AuthenticationException
     */
    public function get(): array
    {
        return (New Get())->handle();
    }

    public function logout(): void
    {
        (New Logout())->handle();
    }
}