<?php

namespace App\Modules\MasterUser\Services\Auth\Handlers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;
use App\Modules\MasterUser\Model\ResetPasswordRequest;

final class RequestResetPassword implements Handler
{
    private string $email;

    /**
     * RequestResetPassword constructor.
     *
     * @param  string  $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function handle()
    {
        $master_user_id = MasterUser::where('email', $this->email)->value('id');

        if($master_user_id) {
            $code = $this->generateCode();

            ResetPasswordRequest::create([
                'user_id' => $master_user_id,
                'code' => $code,
                'email' => $this->email,
            ]);

            $this->recordSuccessfulExecution($master_user_id);

            if(config('app.development')) {
                return ['confirmation_code' => $code];
            }

            return true;
        }

        return false;
    }

    protected function generateCode(): string
    {
        return Str::random(8);
    }

    protected function recordSuccessfulExecution($master_user_id): void
    {
        Log::info('Password recovery request successfully sent', compact('master_user_id'));
    }
}