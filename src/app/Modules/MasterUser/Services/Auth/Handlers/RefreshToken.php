<?php

namespace App\Modules\MasterUser\Services\Auth\Handlers;

use Illuminate\Support\Facades\Auth;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

final class RefreshToken implements Handler
{
    /**
     * @return string
     */
    public function handle(): string
    {
       return Auth::guard(MasterUser::GUARD)->refresh();
    }
}