<?php

namespace App\Modules\MasterUser\Services\Auth\Handlers;

use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

/**
 * Class Confirm
 *
 * @package App\Modules\MasterUser\Services\Auth\Handlers
 */
final class Confirm implements Handler
{
    protected string $email;
    protected string $confirmation_code;

    /**
     * Confirm constructor.
     *
     * @param  string  $email
     * @param  string  $confirmation_code
     */
    public function __construct(string $email, string $confirmation_code)
    {
        $this->email = $email;
        $this->confirmation_code = $confirmation_code;
    }

    /**
     * @return bool
     */
    public function handle(): bool
    {
       $master_user = MasterUser::where('email', $this->email)
           ->where('confirmation_code', $this->confirmation_code)
           ->where('is_active', false)
           ->first();

       if($master_user) {
           $master_user->is_active = true;
           $master_user->save();

           return true;
       }

       return false;
    }
}