<?php

namespace App\Modules\MasterUser\Services\Auth\Handlers;

use Illuminate\Support\Facades\Log;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;
use App\Modules\MasterUser\Model\ResetPasswordRequest;

/**
 * Class ResetPassword
 *
 * @package App\Modules\MasterUser\Services\Auth\Handlers
 */
final class ResetPassword implements Handler
{
    private string $code;
    private string $email;
    private string $new_password;

    private ?MasterUser $master_user;
    private ?ResetPasswordRequest $resetPasswordRequest;

    /**
     * ResetPassword constructor.
     *
     * @param  string  $email
     * @param  string  $code
     * @param  string  $new_password
     */
    public function __construct(string $email, string $code, string $new_password)
    {
        $this->email = $email;
        $this->code = $code;
        $this->new_password = $new_password;
    }

    public function handle(): bool
    {
        if($this->checkExistsMasterUser() && $this->checkCode()) {
            $this->resetPasswordRequest->status = 1;
            $this->resetPasswordRequest->save();

            $this->master_user->password = bcrypt($this->new_password);
            $this->master_user->save();

            $this->recordSuccessfulExecution($this->master_user->id);

            return true;
        }

        return false;
    }

    protected function checkExistsMasterUser(): bool
    {
        $this->master_user = MasterUser::where('email', $this->email)->first();

        return (bool) $this->master_user;
    }

    protected function checkCode(): bool
    {
        $this->resetPasswordRequest = ResetPasswordRequest::where('user_id', $this->master_user->id)
            ->where('code', $this->code)
            ->where('status', false)
            ->first();

        return (bool) $this->resetPasswordRequest;
    }

    protected function recordSuccessfulExecution($master_user_id): void
    {
        Log::info('Password updated successfully', compact('master_user_id'));
    }
}