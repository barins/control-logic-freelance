<?php

namespace App\Modules\MasterUser\Services\Auth\Handlers;

use Illuminate\Support\Str;
use App\Modules\Core\Services\Handler;
use App\Modules\Company\DTO\CompanyDTO;
use App\Modules\MasterUser\Model\MasterUser;
use App\Modules\MasterUser\DTO\MasterUserDTO;
use App\Modules\Company\Services\Register\RegisterService;

/**
 * Class Register
 *
 * @package App\Modules\MasterUser\Services\Auth\Handlers
 */
final class Register implements Handler
{
    protected MasterUserDTO $master_user;
    protected CompanyDTO $company;

    protected MasterUser $user;

    protected array $data;

    /**
     * Register constructor.
     *
     * @param  MasterUserDTO  $master_user
     * @param  CompanyDTO  $company
     */
    public function __construct(MasterUserDTO $master_user, CompanyDTO $company)
    {
        $this->master_user = $master_user;
        $this->company = $company;
    }

    /**
     * @return array
     */
    public function handle(): ?array
    {
        $this->prepareData();

        $this->saveToDBUser();

        $this->registerFirstCompanyThisUser();

        $this->sendNotificationForConfirmation();

        if(config('app.development')) {
            return ['confirmation_code' => $this->data['confirmation_code']];
        }

        return [];
    }

    protected function prepareData(): void
    {
        $this->data = $this->master_user->toArray();
        $this->data['password'] = bcrypt($this->data['password']);
        $this->data['confirmation_code'] = Str::random(8);
    }

    protected function saveToDBUser(): void
    {
        $this->user = MasterUser::create($this->data);
    }

    protected function registerFirstCompanyThisUser(): void
    {
        $this->company->setMasterUserID($this->user->id);

        (New RegisterService())->firstRegister($this->company);
    }

    protected function sendNotificationForConfirmation(): void
    {
        $this->user->sendNotificationForConfirmationAccount($this->data);
    }
}