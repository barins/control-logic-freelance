<?php

namespace App\Modules\MasterUser\Services\Auth\Handlers;

use Illuminate\Support\Facades\Auth;
use App\Modules\Core\Services\Handler;
use Illuminate\Auth\AuthenticationException;
use App\Modules\MasterUser\Model\MasterUser;

final class Get implements Handler
{
    /**
     * @return array
     *
     * @throws AuthenticationException
     */
    public function handle(): array
    {
        $master_user = Auth::guard(MasterUser::GUARD)->user();

        if(! $master_user) {
            throw new AuthenticationException();
        }

        return $master_user->toArray();
    }
}