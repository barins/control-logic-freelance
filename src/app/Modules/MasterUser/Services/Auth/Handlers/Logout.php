<?php

namespace App\Modules\MasterUser\Services\Auth\Handlers;

use Illuminate\Support\Facades\Auth;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

final class Logout implements Handler
{
    /**
     * @return void
     */
    public function handle(): void
    {
        Auth::guard(MasterUser::GUARD)->logout();
    }
}