<?php

namespace App\Modules\MasterUser\Services\Auth\Handlers;

use Illuminate\Support\Facades\Auth;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

final class Login implements Handler
{
    protected string $email;
    protected ?string $password;

    protected string $jwt;

    /**
     * Login constructor.
     *
     * @param  string  $email
     * @param  string|null  $password
     */
    public function __construct(string $email, ?string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function handle(): array
    {
        if(is_null($this->password)) {
            $this->authenticateByEmail();
        } else {
            $this->authenticateByCredential();
        }

        $response = [
            'success' => $this->jwt !== '',
            'token' => $this->jwt,
        ];

        if($response['success']) {
            $response['master_user'] = MasterUser::where('email', $this->email)->first()->toArray();
        }

        return $response;
    }

    protected function authenticateByCredential(): void
    {
        $this->jwt = (string) Auth::guard(MasterUser::GUARD)->attempt(
            [
                'email' => $this->email,
                'password' => $this->password,
                'is_active' => 1,
            ]
        );
    }

    protected function authenticateByEmail(): void
    {
        $this->jwt = Auth::guard(MasterUser::GUARD)->login(MasterUser::where('email', $this->email)->first());
    }
}