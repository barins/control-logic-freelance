<?php

namespace App\Modules\MasterUser\Services\Subscription;

use App\Modules\MasterUser\Services\Subscription\Handlers\GetAllPlans;

/**
 * Class SubscriptionService
 *
 * @package App\Modules\MasterUser\Services\Subscriptio
 */
final class SubscriptionService
{
    /**
     * @return array
     */
    public function getAllPlans(): array
    {
        return (New GetAllPlans())->handle();
    }
}