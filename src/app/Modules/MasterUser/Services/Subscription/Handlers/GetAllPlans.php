<?php

namespace App\Modules\MasterUser\Services\Subscription\Handlers;

use Illuminate\Support\Facades\Cache;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\Plan;

final class GetAllPlans implements Handler
{
    /**
     * @return array
     */
    public function handle(): array
    {
        return Cache::remember('subscriptions', config('cache.default-time'), function () {
            return $this->getAllSubscriptionsInDB();
        });
    }

    protected function getAllSubscriptionsInDB(): array
    {
        return Plan::get()->groupBy('type')->toArray();
    }
}