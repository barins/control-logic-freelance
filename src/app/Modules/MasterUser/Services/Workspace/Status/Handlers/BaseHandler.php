<?php

namespace App\Modules\MasterUser\Services\Workspace\Status\Handlers;

use Illuminate\Support\Facades\Auth;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

abstract class BaseHandler implements Handler
{
    protected function getAuthMasterUserID(): int
    {
        return Auth::guard(MasterUser::GUARD)->id();
    }
}