<?php

namespace App\Modules\MasterUser\Services\Workspace\Status\Handlers;

use App\Modules\MasterUser\Services\Workspace\Status\Repositories\TeacherWorkspace;

final class Deactivate extends BaseHandler
{
    public TeacherWorkspace $teacherWorkspace;

    public function __construct(TeacherWorkspace $teacherWorkspace)
    {
        $this->teacherWorkspace = $teacherWorkspace;
    }

    public function handle(): bool
    {
       return $this->teacherWorkspace->changeActivation($this->getAuthMasterUserID(), false);
    }
}