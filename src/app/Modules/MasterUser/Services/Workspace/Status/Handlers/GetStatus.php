<?php

namespace App\Modules\MasterUser\Services\Workspace\Status\Handlers;

use App\Modules\MasterUser\Services\Workspace\Status\Repositories\TeacherWorkspace;

final class GetStatus extends BaseHandler
{
    public TeacherWorkspace $teacherWorkspace;

    public function __construct(TeacherWorkspace $teacherWorkspace)
    {
        $this->teacherWorkspace = $teacherWorkspace;
    }

    /**
     *
     * @return bool
     */
    public function handle(): bool
    {
        return $this->teacherWorkspace->getStatus($this->getAuthMasterUserID());
    }
}