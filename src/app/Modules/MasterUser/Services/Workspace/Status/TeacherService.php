<?php

namespace App\Modules\MasterUser\Services\Workspace\Status;

use App\Modules\MasterUser\Services\Workspace\Status\Handlers\Activate;
use App\Modules\MasterUser\Services\Workspace\Status\Handlers\GetStatus;
use App\Modules\MasterUser\Services\Workspace\Status\Handlers\Deactivate;
use App\Modules\MasterUser\Services\Workspace\Status\Handlers\Initialization;
use App\Modules\MasterUser\Services\Workspace\Status\Repositories\TeacherWorkspace;

/**
 * Class TeacherService
 *
 * @package App\Modules\MasterUser\Services\Workspace\Teacher
 */
final class TeacherService
{
    public TeacherWorkspace $teacherWorkspace;

    public function __construct(TeacherWorkspace $teacherWorkspace)
    {
        $this->teacherWorkspace = $teacherWorkspace;
    }

    public function initialization(): bool
    {
        return (New Initialization($this->teacherWorkspace))->handle();
    }

    public function getStatus(): bool
    {
        return (New GetStatus($this->teacherWorkspace))->handle();
    }

    public function activate(): bool
    {
       return (New Activate($this->teacherWorkspace))->handle();
    }

    public function deactivate(): bool
    {
       return  (New Deactivate($this->teacherWorkspace))->handle();
    }
}