<?php

namespace App\Modules\MasterUser\Services\Workspace\Status\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class TeacherWorkspaceRepository implements TeacherWorkspace
{
    protected Builder $builder;

    public function __construct()
    {
        $this->builder = DB::table('workspace-teachers');
    }

    public function create(int $master_user_id): bool
    {
        $exists = $this->builder->where('user_id', $master_user_id)->exists();

        if (! $exists) {
            $this->builder
                ->insert([
                    'user_id' => $master_user_id,
                    'activated' => true
                ]);

            return true;
        }

        return false;
    }

    public function getStatus(int $master_user_id): bool
    {
        return (bool) $this->builder
            ->where('user_id', $master_user_id)
            ->value('activated');
    }

    public function changeActivation(int $master_user_id, bool $status): bool
    {
        return (bool)
            $this->builder
                ->where('user_id', $master_user_id)
                ->update(['activated' => $status]);
    }
}