<?php

namespace App\Modules\MasterUser\Services\Workspace\Status\Repositories;

interface TeacherWorkspace {
    public function create(int $master_user_id): bool;

    public function getStatus(int $master_user_id): bool;

    public function changeActivation(int $master_user_id, bool $activated): bool;
}