<?php

namespace App\Modules\MasterUser\Http\Controllers\Subscription;

use Illuminate\Http\JsonResponse;
use App\Modules\Core\Http\Controllers\Controller;
use App\Modules\MasterUser\Services\Subscription\SubscriptionService;

/**
 * Class PlanController
 *
 * @package App\Modules\MasterUser\Http\Controllers
 */
class PlanController extends Controller
{
    protected SubscriptionService $service;

    public function __construct()
    {
        $this->service = New SubscriptionService();
    }

    /**
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        return $this->response('All plans', ['plans' => $this->service->getAllPlans()]);
    }
}
