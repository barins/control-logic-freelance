<?php

namespace App\Modules\MasterUser\Http\Controllers\Subscription;

use Illuminate\Http\JsonResponse;
use App\Modules\Core\Http\Controllers\Controller;
use App\Modules\MasterUser\Services\Subscription\SubscriptionService;
use App\Modules\MasterUser\Http\Requests\Subscription\PaySubscriptionRequest;

/**
 * Class SubscriptionController
 *
 * @package App\Modules\MasterUser\Http\Controllers
 */
class SubscriptionController extends Controller
{
    protected SubscriptionService $service;

    /**
     * SubscriptionController constructor.
     */
    public function __construct()
    {
        $this->service = New SubscriptionService();
    }

    /**
     * @param  PaySubscriptionRequest  $request
     *
     * TODO: Not done
     *
     * @return JsonResponse
     */
    public function paySubscription(PaySubscriptionRequest $request): JsonResponse
    {
       return $this->response('Request for pay subscription', $request->validated());
    }
}
