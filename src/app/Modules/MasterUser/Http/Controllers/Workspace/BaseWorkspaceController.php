<?php

namespace App\Modules\MasterUser\Http\Controllers\Workspace;

use Illuminate\Http\JsonResponse;
use App\Modules\Core\Http\Controllers\Controller;
use App\Modules\MasterUser\Services\Workspace\Status\TeacherService;

/**
 * Class BaseWorkspaceController
 *
 * @package App\Modules\MasterUser\Http\Controllers\Workspace\BaseWorkspaceController
 */
class BaseWorkspaceController extends Controller
{
    protected TeacherService $service;

    public function __construct(TeacherService $service)
    {
        $this->service = $service;
    }

    /**
     * @return JsonResponse
     */
    public function setStatus(): JsonResponse
    {
        $activated = $this->service->getStatus();

        return $this->response('Your status of workspace', ['workspace' => compact('activated')]);
    }

    /**
     * @return JsonResponse
     */
    public function initialization(): JsonResponse
    {
        $success = $this->service->initialization();

        if ($success) {
            $response = $this->response('Successful initialization');
        } else {
            $response = $this->response('Not initialized', [],
                ['initialization' => 'Initialization is already done'], 423);
        }

        return $response;
    }

    /**
     * @return JsonResponse
     */
    public function activate(): JsonResponse
    {
        $success = $this->service->activate();

        if($success) {
            $response = $this->response('You have successfully activated workspace teacher');
        } else {
            $response = $this->responseActionImpossible();
        }

        return $response;
    }

    /**
     * @return JsonResponse
     */
    public function deactivate(): JsonResponse
    {
        $success = $this->service->deactivate();

        if($success) {
            $response = $this->response('You deactivated workspace teacher');
        } else {
            $response = $this->responseActionImpossible();
        }

        return $response;
    }

    protected function responseActionImpossible(): JsonResponse
    {
        return $this->response('Action is impossible', [], [], 400);
    }
}
