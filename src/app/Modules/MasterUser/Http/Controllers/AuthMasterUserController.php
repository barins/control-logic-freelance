<?php

namespace App\Modules\MasterUser\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Modules\Company\DTO\CompanyDTO;
use Illuminate\Auth\AuthenticationException;
use App\Modules\MasterUser\Model\MasterUser;
use App\Modules\MasterUser\DTO\MasterUserDTO;
use App\Modules\Core\Http\Controllers\Controller;
use App\Modules\MasterUser\Services\Auth\AuthService;
use App\Modules\MasterUser\Http\Requests\Auth\{
    ConfirmationMasterUserRequest as ConfirmationRequest,
    RegisterMasterUserRequest as RegisterRequest,
    LoginMasterUserRequest as LoginRequest,
    PasswordResetSubmitRequest,
    PasswordResetRequest,
};

/**
 * Class AuthMasterUserController
 *
 * @package App\Modules\MasterUser\Http\Controllers
 */
class AuthMasterUserController extends Controller
{
    protected AuthService $service;

    /**
     * MasterUserController constructor.
     */
    public function __construct()
    {
        $this->service = New AuthService();

        $guard = MasterUser::GUARD;

        $this->middleware("auth:$guard", ['only' => ['me', 'logout', 'refresh']]);
    }

    /**
     * @param  RegisterRequest  $request
     *
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $bodyParam = $request->validated();

        $master_user = MasterUserDTO::fromArray($bodyParam);
        $company = CompanyDTO::fromArray($bodyParam);

        $result = $this->service->register($master_user, $company);

        $request->throttle->clearAttempts();

        return $this->response('Successful registration', $result);
    }

    /**
     * @param  ConfirmationRequest  $request
     *
     * @return JsonResponse
     */
    public function confirm(ConfirmationRequest $request): JsonResponse
    {
        $bodyParams = $request->validated();

        $success = $this->service->confirm($bodyParams['email'], $bodyParams['confirmation_code']);

        if($success) {
            $request->throttle->clearAttempts();

            $data = $this->service->login($bodyParams['email']);

            $response = $this->respondWithToken($data['token']);
            $response['master_user'] = $data['master_user'];

            return $this->response('Successfully confirmed', $response);
        }

        return $this->response('Not confirmed', [], ['request' => 'Wrong data'], 400);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  LoginRequest  $request
     *
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $credentials = $request->validated();

        $data = $this->service->login($credentials['email'], $credentials['password']);

        if ($data['success']) {
            $response = $this->respondWithToken($data['token']);
            $response['master_user'] = $data['master_user'];

            $request->throttle->clearAttempts();

            return $this->response('Successful authorization', $response);
        }

        return $this->response('Unsuccessful authorization', [], ['status' => 'Wrong credentials'], 401);
    }

    /**
     * @return JsonResponse
     *
     * Get the authenticated User.
     *
     * @throws AuthenticationException
     */
    public function me(): JsonResponse
    {
        return $this->response('My data', [
            'master_user' => $this->service->get()
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
       $this->service->logout();

        return $this->response('Successfully logged out');
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        return $this->response(
            'Access token updated successfully',
            $this->respondWithToken($this->service->refreshToken())
        );
    }

    /**
     * @param  PasswordResetSubmitRequest  $request
     *
     * @return JsonResponse
     */
    public function passwordResetRequest(PasswordResetSubmitRequest $request): JsonResponse
    {
       $email = $request->input('email');

       $success = $this->service->requestResetPassword($email);

       if($success) {
           $response = is_bool($success) ? [] : $success;

           return $this->response('Password recovery request successfully sent', $response);
       }

        return $this->response('Password not recovered', [], ['request' => 'Wrong data'], 401);
    }

    /**
     * @param  PasswordResetRequest  $request
     *
     * @return JsonResponse
     */
    public function passwordReset(PasswordResetRequest $request): JsonResponse
    {
       $bodyParam = $request->validated();

       $success = $this->service->resetPassword($bodyParam['email'], $bodyParam['code'], $bodyParam['password']);

       if($success) {
           return $this->response('Password updated successfully');
       }

        return $this->response('Password not updated', [], ['request' => 'Wrong data'], 401);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return array
     */
    protected function respondWithToken($token): array
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer'
        ];
    }
}
