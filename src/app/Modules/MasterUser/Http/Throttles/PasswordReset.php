<?php

namespace App\Modules\MasterUser\Http\Throttles;

use App\Modules\Core\Throttles\Throttle;

class PasswordReset extends Throttle
{
    public string $key = 'email';
    public int $maxAttempts = 5;
    public int $decaySeconds = 60;
}
