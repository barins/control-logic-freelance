<?php

namespace App\Modules\MasterUser\Http\Throttles;

use App\Modules\Core\Throttles\Throttle;

class Login extends Throttle
{
    public string $key = 'email';
    public int $maxAttempts = 10;
    public int $decaySeconds = 60;
}
