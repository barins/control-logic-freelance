<?php

namespace App\Modules\MasterUser\Http\Throttles;

use App\Modules\Core\Throttles\Throttle;

class Confirm extends Throttle
{
    public string $key = 'confirmation_code';
    public int $maxAttempts = 5;
    public int $decaySeconds = 60;
}
