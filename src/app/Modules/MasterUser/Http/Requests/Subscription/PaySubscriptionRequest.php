<?php

namespace App\Modules\MasterUser\Http\Requests\Subscription;

use App\Modules\Core\Http\Requests\FormRequest;

class PaySubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
       return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        // Not done.
        return [
            'package_identifier' => ['required'],
            'card_holder_name' => ['string', 'required'],
            'card_number' => ['required'],
            'cvv' => ['int', 'required'],
            'expiry_date' => ['string', 'required']
        ];
    }
}
