<?php

namespace App\Modules\MasterUser\Http\Requests\Auth;

use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\MasterUser\Http\Throttles\Login;

class LoginMasterUserRequest extends FormRequest
{
    public Login $throttle;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->registerThrottle();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        $this->setMasterUserLoginRules();

        return [];
    }

    protected function registerThrottle(): void
    {
        $throttle = new Login($this);

        $throttle->check();

        $throttle->incrementAttempts();

        $this->throttle = $throttle;
    }
}
