<?php

namespace App\Modules\MasterUser\Http\Requests\Auth;

use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\MasterUser\Http\Throttles\PasswordReset;

class PasswordResetRequest extends FormRequest
{
    public PasswordReset $throttle;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->registerThrottle();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        return [
            'password' => ['required', 'confirmed'],
            'email' => ['required', 'email'],
            'code' => ['required', 'string'],
        ];
    }

    protected function registerThrottle(): void
    {
        $throttle = new PasswordReset($this);

        $throttle->check();

        $throttle->incrementAttempts();

        $this->throttle = $throttle;
    }
}
