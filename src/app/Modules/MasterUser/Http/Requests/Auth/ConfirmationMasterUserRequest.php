<?php

namespace App\Modules\MasterUser\Http\Requests\Auth;

use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\MasterUser\Http\Throttles\Confirm;

class ConfirmationMasterUserRequest extends FormRequest
{
    public Confirm $throttle;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->registerThrottle();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        return [
            'email' => ['required', 'email'],
            'confirmation_code' => ['required'],
        ];
    }

    protected function registerThrottle(): void
    {
        $throttle = new Confirm($this);

        $throttle->check();

        $throttle->incrementAttempts();

        $this->throttle = $throttle;
    }
}
