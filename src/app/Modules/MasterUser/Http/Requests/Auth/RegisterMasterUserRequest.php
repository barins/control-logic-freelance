<?php

namespace App\Modules\MasterUser\Http\Requests\Auth;

use App\Modules\Core\Http\Requests\FormRequest;
use App\Modules\MasterUser\Http\Throttles\Register;

/**
 * Class RegisterMasterUserRequest
 *
 * @method setMasterUserTypeRules
 * @method setMasterUserLoginRules
 * @method setMasterUserRegisterRules
 *
 * @method setCompanyRegisterRules
 *
 * @package App\Modules\MasterUser\Http\Requests\Auth
 */
class RegisterMasterUserRequest extends FormRequest
{
    public Register $throttle;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->registerThrottle();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        $this->setMasterUserRegisterRules();
        $this->setCompanyRegisterRules();

        return [];
    }

    protected function registerThrottle(): void
    {
        $throttle = new Register($this);

        $throttle->check();

        $throttle->incrementAttempts();

        $this->throttle = $throttle;
    }
}
