<?php

namespace App\Modules\MasterUser\Rules;

use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;
use App\Modules\MasterUser\Model\MasterUser;
use App\Modules\Company\Model\MasterUserCompany;

class OwnerCompany implements Rule
{
    private string $owner_id;

    public function __construct()
    {
        $this->owner_id = Auth::guard(MasterUser::GUARD)->id();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        try {
            $result = MasterUserCompany::where('owner_id', $this->owner_id)
                ->where('company_id', $value)
                ->exists();
        } catch (\Exception $exception) {
            return false;
        }

        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.company.owner');
    }
}
