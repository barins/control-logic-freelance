<?php

namespace App\Modules\MasterUser\Observers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use App\Modules\MasterUser\Model\MasterUser;

class MasterUserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param MasterUser $user
     * @return void
     */
    public function created(MasterUser $user): void
    {
        static::log('Create user', $user->toArray());
    }

    /**
     * Handle the user "updated" event.
     *
     * @param MasterUser $user
     * @return void
     */
    public function updated(MasterUser $user): void
    {
        static::log('Update user', $user->toArray());
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param MasterUser $user
     * @return void
     */
    public function deleted(MasterUser $user): void
    {
        static::log('Delete user', $user->toArray());
    }

    /**
     * @param $title
     * @param $data
     */
    protected static function log($title, array $data): void
    {
        Log::info("Master user: $title", Arr::except($data, ['updated_at', 'created_at', 'password']));
    }
}
