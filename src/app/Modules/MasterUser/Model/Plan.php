<?php

namespace App\Modules\MasterUser\Model;

use App\Modules\Core\Model\BaseModel;

/**
 * Class Subscription
 *
 * @property string $base_title
 * @property string $type
 * @property string $identifier
 * @property string $description
 * @property string $price
 *
 * @package App\Modules\MasterUser\Model
 */
class Plan extends BaseModel
{
    public $table = 'subscription-plans';

    public static bool $usePresenter = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'base_title',
        'type',
        'identifier',
        'description',
        'price',
        'updated_at',
        'created_at',
    ];

    protected $hidden = [
        'type',
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'price' => 'integer',
    ];
}
