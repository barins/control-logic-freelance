<?php

namespace App\Modules\MasterUser\Model;

use Yaro\Presenter\PresenterTrait;
use Illuminate\Foundation\Auth\User;
use App\Modules\Core\Traits\UsesUuid;
use App\Modules\Company\Model\Company;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use App\Modules\MasterUser\Notifications\Confirm;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Modules\MasterUser\Presenter\MasterUserPresenter;

/**
 * Class MasterUser
 *
 * @property string $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $confirmation_code
 * @property boolean $is_active
 * @property Company $companies
 * @property string $created_at
 * @property string $updated_at
 *
 * @package App\Modules\MasterUser\Model
 */
class MasterUser extends User implements JWTSubject
{
    use UsesUuid;
    use Notifiable;
    use PresenterTrait;

    public const GUARD = 'master_user';

    protected string $presenter = MasterUserPresenter::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'confirmation_code',
        'is_active',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'confirmation_code',
        'laravel_through_key',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'is_active' => 'boolean',
    ];

    final public function toArray(bool $presenter = true): array
    {
        if($presenter) {
            $presenter = $this->getPresenterClass();
            if ($presenter) {
                $presenter = new $presenter($this);
                return $presenter->toArray();
            }
        }

        return parent::toArray();
    }

    /**
     * @param  array  $data
     */
    public function sendNotificationForConfirmationAccount(array $data): void
    {
        $this->notify(new Confirm($data));
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * Get companies for the master-user.
     */
    public function companies(): belongsToMany
    {
        return $this->belongsToMany(Company::class, 'company_master_user', 'owner_id', 'company_id');
    }
}
