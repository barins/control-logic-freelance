<?php

namespace App\Modules\MasterUser\Model;

use App\Modules\Core\Model\BaseModel;
use App\Modules\MasterUser\Notifications\Confirm;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Modules\MasterUser\Presenter\ResetPasswordRequestPresenter;

/**
 * Class ResetPasswordRequest
 *
 * @property integer user_id
 * @property string code
 * @property bool status
 * @property MasterUser master_user
 *
 * @package App\Modules\MasterUser\Model
 */
class ResetPasswordRequest extends BaseModel
{
    public $table = 'reset_password_request';

    public string $presenter = ResetPasswordRequestPresenter::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'code',
        'status',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'status' => 'bool',
    ];

    public static function boot(): void
    {
        static::created(static function (ResetPasswordRequest $model) {
            $model->sendNotificationForConfirmationPassword($model->toArray());
        });

        parent::boot();
    }

    /**
     * @param  array  $data
     */
    public function sendNotificationForConfirmationPassword(array $data): void
    {
        $this->notify(new Confirm($data));
    }

    /**
     * Get master user for the reset-password-request.
     */
    public function master_user(): hasOne
    {
        return $this->hasOne(MasterUser::class, 'id', 'user_id');
    }
}
