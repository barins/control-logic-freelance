<?php

/**
 * Rules for master-user.
 */
return [
    'master_user_type' => [
        'first_name' => ['string'],
        'last_name' => ['string'],
        'email' => ['email'],
    ],

    'master_user_login' => [
        'password' => ['required', 'min:6'],
        'email' => ['required', 'email'],
    ],

    'master_user_register' => [
        'first_name' => ['required', 'string'],
        'last_name' => ['required', 'string'],
        'email' => ['required', 'email', 'unique:master_users'],
        'password' => ['required', 'min:6'],
    ]
];