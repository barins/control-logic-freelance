<?php

namespace App\Modules\MasterUser\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use App\Modules\MasterUser\Mail\Confirm as ConfirmByMail;

class Confirm extends Notification
{
    use Queueable;

    public array $data;
    public string $recipient;

    /**
     * Create a new notification instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via(): array
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @return ConfirmByMail
     */
    public function toMail(): ConfirmByMail
    {
        $recipient = $this->data['email'];

        unset($this->data['email']);

        return (new ConfirmByMail($this->data))->to($recipient);
    }
}
