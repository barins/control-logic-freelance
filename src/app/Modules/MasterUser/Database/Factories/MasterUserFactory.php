<?php

use Faker\Generator as Faker;
use App\Modules\MasterUser\Model\MasterUser;

$factory->define(MasterUser::class, static function(Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt(random_int(1, 20)),
        'is_active' => 0,
    ];
});
