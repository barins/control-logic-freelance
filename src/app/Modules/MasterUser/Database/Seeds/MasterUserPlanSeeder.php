<?php

namespace App\Modules\MasterUser\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Modules\MasterUser\Model\Plan;

class MasterUserPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Plan::insert([
            [
                'base_title' => 'Basic',
                'identifier' => 'monthly_basic',
                'description' => 'This is basic subscription...',
                'price' => 25,
                'type' => 'monthly',
            ],
            [
                'base_title' => 'Standard',
                'identifier' => 'monthly_standard',
                'description' => 'This is standard subscription...',
                'price' => 50,
                'type' => 'monthly',
            ],
            [
                'base_title' => 'Pro',
                'identifier' => 'monthly_pro',
                'description' => 'This is pro subscription...',
                'price' => 80,
                'type' => 'monthly',
            ],
            [
                'base_title' => 'Basic',
                'identifier' => 'annual_basic',
                'description' => 'This is basic subscription...',
                'price' => 250,
                'type' => 'annual',
            ],
            [
                'base_title' => 'Standard',
                'identifier' => 'annual_standard',
                'description' => 'This is standard subscription...',
                'price' => 500,
                'type' => 'annual',
            ],
            [
                'base_title' => 'Pro',
                'identifier' => 'annual_pro',
                'description' => 'This is pro subscription...',
                'price' => 800,
                'type' => 'annual',
            ],
        ]);
    }
}