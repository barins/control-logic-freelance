<?php

namespace App\Modules\MasterUser\Database\Seeds;

use Illuminate\Database\Seeder;

class MasterUserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(MasterUserPlanSeeder::class);
    }
}