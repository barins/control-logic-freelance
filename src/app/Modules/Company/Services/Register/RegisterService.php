<?php

namespace App\Modules\Company\Services\Register;

use App\Modules\Company\DTO\CompanyDTO;
use App\Modules\Company\Services\Register\Handlers\FirstRegister;

/**
 * Class RegisterService
 *
 * @package App\Modules\Company\Services\Register
 */
class RegisterService
{
    /**
     * @param  CompanyDTO  $company
     */
    public function firstRegister(CompanyDTO $company): void
    {
        (new FirstRegister($company))->handle();
    }
}