<?php

namespace App\Modules\Company\Services\Register\Handlers;

use Faker\Factory;
use App\Modules\Core\Services\Handler;
use App\Modules\Company\Model\Company;
use App\Modules\Company\DTO\CompanyDTO;
use App\Modules\Company\Model\MasterUserCompany;

/**
 * Class FirstRegister
 *
 * @package App\Modules\Company\Services\Register\Handlers
 */
class FirstRegister implements Handler
{
    protected CompanyDTO $company;

    public function __construct(CompanyDTO $company)
    {
        $this->company = $company;
    }

    public function handle(): void
    {
        $this->saveToDBCompany();
    }

    protected function saveToDBCompany(): void
    {
        $company = $this->company->toArray();

        $company_id = Company::create($company)->id;

        MasterUserCompany::create([
            'owner_id' => $company['master_user_id'],
            'company_id' => $company_id,
        ]);
    }}