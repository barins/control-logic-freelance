<?php

namespace App\Modules\Company\Services\Industry\Handlers;

use Illuminate\Support\Facades\Cache;
use App\Modules\Core\Services\Handler;
use App\Modules\Company\Model\CompanyIndustry;

/**
 * Class Get
 *
 * @package App\Modules\Company\Services\Industry\Handlers
 */
class Get implements Handler
{
    public function handle(): array
    {
        return Cache::remember('industries', config('cache.default-time'), function () {
            return $this->getAllIndustriesInDB();
        });
    }

    protected function getAllIndustriesInDB(): array
    {
        return CompanyIndustry::all()->toArray();
    }
}