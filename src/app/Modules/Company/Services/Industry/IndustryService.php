<?php

namespace App\Modules\Company\Services\Industry;

use App\Modules\Company\Services\Industry\Handlers\Get;

/**
 * Class IndustryService
 *
 * @package App\Modules\Company\Services\Industry
 */
class IndustryService
{
    /**
     * @return array
     */
    public function get(): array
    {
        return (New Get())->handle();
    }
}