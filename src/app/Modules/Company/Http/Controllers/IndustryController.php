<?php

namespace App\Modules\Company\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Modules\Core\Http\Controllers\Controller;
use App\Modules\Company\Services\Industry\IndustryService;

/**
 * Class IndustryController
 *
 * @package App\Modules\Company\Http\Controllers
 */
class IndustryController extends Controller
{
    /**
     * Get all industries
     *
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        return $this->response('All industries', ['industries' => (New IndustryService)->get()]);
    }
}
