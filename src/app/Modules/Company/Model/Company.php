<?php

namespace App\Modules\Company\Model;

use App\Modules\Core\Model\Country;
use App\Modules\Core\Model\BaseModel;
use App\Modules\Core\Traits\UsesUuid;
use App\Modules\MasterUser\Model\MasterUser;
use Illuminate\Database\Eloquent\Relations\hasOne;
use App\Modules\Company\Presenter\CompanyPresenter;
use Illuminate\Database\Eloquent\Relations\hasOneThrough;

/**
 * Class Company
 *
 * @property string $id
 * @property string  $company_name
 * @property string  $company_domain
 * @property string  $database_name
 * @property integer $country_id
 * @property integer $industry_id
 * @property Country $country
 * @property CompanyIndustry $industry
 * @property string  $created_at
 * @property string  $updated_at
 *
 * @package App\Modules\Company\Model
 */
class Company extends BaseModel
{
    use UsesUuid;

    protected string $presenter = CompanyPresenter::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'company_domain',
        'country_id',
        'industry_id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'confirmation_code',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get master-user for the company.
     */
    public function masterUser(): hasOneThrough
    {
        return $this->hasOneThrough(MasterUser::class, MasterUserCompany::class, 'company_id', 'id');
    }

    /**
     * Get country for the company.
     */
    public function country(): hasOne
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    /**
     * Get industry for the company.
     */
    public function industry(): hasOne
    {
        return $this->hasOne(CompanyIndustry::class, 'id', 'industry_id');
    }
}
