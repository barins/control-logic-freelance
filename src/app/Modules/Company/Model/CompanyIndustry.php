<?php

namespace App\Modules\Company\Model;

use App\Modules\Core\Model\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Modules\Company\Presenter\CompanyIndustryPresenter;

/**
 * Class Company
 *
 * @property integer $id
 * @property string $name
 * @property Company $companies
 * @property string $slug
 *
 * @package App\Modules\Company\CompanyIndustry
 */
class CompanyIndustry extends BaseModel
{
    protected $table = 'company_industry';

    public string $presenter = CompanyIndustryPresenter::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get companies for the industry.
     */
    public function companies(): hasMany
    {
        return $this->hasMany(Company::class, 'company_id', 'id');
    }
}
