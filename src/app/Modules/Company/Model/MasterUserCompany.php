<?php

namespace App\Modules\Company\Model;

use App\Modules\Core\Model\BaseModel;

/**
 * Class Company
 *
 * @property integer $id
 * @property string $owner_id
 * @property string $company_id
 *
 * @package App\Modules\Company\UserCompany
 */
class MasterUserCompany extends BaseModel
{
    protected $table = 'company_master_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id',
        'company_id',
    ];

    public $timestamps = false;
}
