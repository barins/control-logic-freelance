<?php

namespace App\Modules\Company\DTO;

final class CompanyDTO
{
    private ?string $id;
    private string $company_name;
    private string $company_domain;
    private int $country_id;
    private int $industry_id;
    private ?string $database_name;
    private ?string $created_at;
    private ?string $updated_at;
    private ?string $master_user_id;

    private function __construct(
        ?string $id,
        string $company_name,
        string $company_domain,
        int $country_id,
        int $industry_id,
        ?string $database_name,
        ?string $created_at,
        ?string $updated_at,
        ?string $master_user_id = null
    )
    {
        $this->id = $id;
        $this->company_name = $company_name;
        $this->company_domain = $company_domain;
        $this->country_id = $country_id;
        $this->industry_id = $industry_id;
        $this->database_name = $database_name;
        $this->created_at = $created_at;
        $this->updated_at = $updated_at;
        $this->master_user_id = $master_user_id;
    }

    /**
     * @param  array  $data
     *
     * @return CompanyDTO
     */
    public static function fromArray(array $data): CompanyDTO
    {
        return new self(
            $data['id'] ?? null,
            $data['company_name'],
            $data['company_domain'],
            $data['country_id'],
            $data['category_id'],
            $data['database_name'] ?? null,
            $data['created_at'] ?? null,
            $data['updated_at'] ?? null
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return  [
            'company_name' => $this->company_name,
            'company_domain' => $this->company_domain,
            'country_id' => $this->country_id,
            'industry_id' => $this->industry_id,
        ];
    }

    public function setMasterUserID(string $master_user_id): void
    {
        $this->master_user_id = $master_user_id;
    }
}