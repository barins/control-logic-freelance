<?php

/**
 * Rules of company.
 */
return [
    'company_type' => [
        'company_name' => ['string'],
        'company_domain' => ['string'],
        'country_id' => ['integer'],
        'category_id' => ['integer'],
    ],

    'company_register' => [
        'company_name' => ['required', 'string', 'unique:companies'],
        'company_domain' => ['required', 'string', 'unique:companies'],
        'country_id' => ['required', 'integer', 'exists:countries,id'],
        'category_id' => ['required', 'integer', 'exists:company_industry,id'],
    ],
];