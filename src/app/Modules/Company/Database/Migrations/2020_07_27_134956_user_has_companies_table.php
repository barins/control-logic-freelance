<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserHasCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('user_has_companies', static function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->uuid('owner_id')->unsigned()->index();
            $table->uuid('company_id')->unsigned()->index();

            $table->foreign('owner_id')->references('id')->on('master_users')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('user_has_companies');
    }
}
