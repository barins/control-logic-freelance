<?php

use Faker\Generator as Faker;
use App\Modules\Company\Model\Company;

$factory->define(Company::class, static function(Faker $faker) {
    return [
        'company_name' => $faker->company,
        'company_domain' => $faker->url,
        'country_id' => 235,
        'industry_id' => 1
    ];
});
