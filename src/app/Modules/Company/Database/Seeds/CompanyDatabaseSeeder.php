<?php

namespace App\Modules\Company\Database\Seeds;

use Illuminate\Database\Seeder;

class CompanyDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(CompanyIndustrySeeder::class);
    }
}