<?php

namespace App\Modules\Company\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Modules\Company\Model\CompanyIndustry as Industry;

class CompanyIndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Industry::insert([
            [
                'name' => 'Manufacturing',
                'slug' => 'manufacturing',
            ],
            [
                'name' => 'Logistics',
                'slug' => 'logistics',
            ],
            [
                'name' => 'IT',
                'slug' => 'it',
            ],
            [
                'name' => 'Lawyers',
                'slug' => 'lawyers',
            ],
            [
                'name' => 'Sales',
                'slug' => 'sales',
            ],
        ]);
    }
}