<?php

namespace App\Modules\Company\Presenter;

use Yaro\Presenter\AbstractPresenter;

/**
 * Class CompanyPresenter
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 *
 * @package App\Modules\MasterUser\Presenter
 */
class CompanyIndustryPresenter extends AbstractPresenter
{
    protected $arrayable = [
        'id',
        'name',
        'slug',
    ];

}