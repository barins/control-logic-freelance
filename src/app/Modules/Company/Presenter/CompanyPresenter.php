<?php

namespace App\Modules\Company\Presenter;

use App\Modules\Company\Model\Company;
use Yaro\Presenter\AbstractPresenter;

/**
 * Class CompanyPresenter
 *
 * @property integer $id
 * @property string  $company_name
 * @property string  $company_domain
 * @property integer $country
 * @property integer $industry
 * @property string  $created_at
 * @property string  $updated_at
 *
 * @package App\Modules\MasterUser\Presenter
 */
class CompanyPresenter extends AbstractPresenter
{
    /**
     * @var Company $model
     */
    protected $model;

    protected $arrayable = [
        'id',
        'company_name',
        'company_domain',
        'country',
        'industry',
        'created_at',
        'updated_at',
    ];

    public function getCountryPresent(): array
    {
        return $this->model->country->toArray();
    }

    public function getIndustryPresent(): array
    {
        return $this->model->industry->toArray();
    }
}