<?php

namespace App\Modules\Core\DTO;

/**
 * Class BaseDTO
 *
 * To transfer structured data to the service
 *
 * Request -> Controller -> Service -> Handler
 */
abstract class BaseDTO
{
    protected static bool $showNullVars = true;

    public function toArray(): array
    {
        $vars = get_object_vars($this);

        if(! self::$showNullVars) {
            $vars = array_filter($vars);
        }

        return $vars;
    }

    public function disableDisplayNullVariables(): BaseDTO
    {
        self::$showNullVars = false;

        return $this;
    }
}