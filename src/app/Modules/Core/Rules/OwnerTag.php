<?php

namespace App\Modules\Core\Rules;

use App\Modules\Core\Model\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Rule;
use App\Modules\MasterUser\Model\MasterUser;

class OwnerTag implements Rule
{
    private string $owner_id;

    public function __construct()
    {
        $this->owner_id = Auth::guard(MasterUser::GUARD)->id();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  string|array  $tag_id
     * @return bool
     */
    public function passes($attribute, $tag_id): bool
    {
        try {
            $builder = Tag::where(function ($query) {
                $query->where('master_user_id', null);
                $query->orWhere('master_user_id', $this->owner_id);
            });

            if(is_array($tag_id)) {
                $builder = $builder->whereIn('id', $tag_id);

                $access = $builder->count() === count($tag_id);
            } else {
                $builder = $builder->where('id', $tag_id);

                $access = $builder->exists();
            }
        } catch (\Exception $exception) {
            $access = false;
        }

        return $access;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.owner');
    }
}
