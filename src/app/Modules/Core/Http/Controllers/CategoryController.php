<?php

namespace App\Modules\Core\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Modules\Core\Services\Category\CategoryService;
use App\Modules\Core\Http\Requests\Category\{
    AddCategoryRequest,
    DeleteCategoryRequest,
};

class CategoryController extends Controller
{
    protected CategoryService $service;

    public function __construct()
    {
        $this->service = New CategoryService();
    }

    /**
     * Get all categories;
     *
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        $categories = $this->service->get();

        return $this->response('All categories', compact('categories'));
    }

    /**
     * Add new category.
     *
     * @param  AddCategoryRequest  $request
     *
     * @return JsonResponse
     */
    public function add(AddCategoryRequest $request): JsonResponse
    {
        $this->service->add($request->input('value'));

        $categories = $this->service->get();

        return $this->response('Successful added new category', compact('categories'));
    }

    /**
     * Delete category.
     *
     * @param  DeleteCategoryRequest  $request
     *
     * @return JsonResponse
     */
    public function delete(DeleteCategoryRequest $request): JsonResponse
    {
        $this->service->delete($request->input('id'));

        return $this->response('Deleted category');
    }
}
