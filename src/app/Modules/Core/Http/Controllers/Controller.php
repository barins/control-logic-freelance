<?php

namespace App\Modules\Core\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param  string  $message
     * @param          $data
     * @param  array   $error
     * @param  int     $status
     *
     * @return JsonResponse
     */
    public function response(
        string $message = 'Success',
        array $data = [],
        array $error = [],
        int $status = 200
    ): JsonResponse
    {
        return response()->api($message, $data, $error, $status);
    }
}
