<?php

namespace App\Modules\Core\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Modules\Core\Services\Tag\TagService;
use App\Modules\Core\Http\Requests\Tag\{
    AddTagRequest,
    DeleteTagRequest,
};

class TagController extends Controller
{
    protected TagService $service;

    public function __construct()
    {
        $this->service = New TagService();
    }

    /**
     * Get all tags;
     *
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        $tags = $this->service->get();

        return $this->response('All tags', compact('tags'));
    }

    /**
     * Add new tag.
     *
     * @param  AddTagRequest  $request
     *
     * @return JsonResponse
     */
    public function add(AddTagRequest $request): JsonResponse
    {
        $this->service->add($request->input('value'));

        $tags = $this->service->get();

        return $this->response('Successful added new tag', compact('tags'));
    }

    /**
     * Delete tag.
     *
     * @param  DeleteTagRequest  $request
     *
     * @return JsonResponse
     */
    public function delete(DeleteTagRequest $request): JsonResponse
    {
        $this->service->delete($request->input('id'));

        return $this->response('Deleted tag');
    }
}
