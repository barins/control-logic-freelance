<?php

namespace App\Modules\Core\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Modules\Core\Services\Country\CountryService;

/**
 * Class CountryController
 *
 * @package App\Modules\MasterUser\Http\Controllers
 */
class CountryController extends Controller
{
    /**
     * Get all countries
     *
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        return $this->response('All countries', ['countries' => (New CountryService)->get()]);
    }
}
