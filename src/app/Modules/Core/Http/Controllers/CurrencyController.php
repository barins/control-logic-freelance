<?php

namespace App\Modules\Core\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Modules\Core\Services\Currency\CurrencyService;

/**
 * Class CurrencyController
 *
 * @package App\Modules\Core\Http\Controllers
 */
class CurrencyController extends Controller
{
    /**
     * Get all currencies
     *
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        $currencies = (New CurrencyService())->get();

        return $this->response('All currencies', compact('currencies'));
    }
}
