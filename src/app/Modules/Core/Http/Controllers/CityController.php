<?php

namespace App\Modules\Core\Http\Controllers;

use App\Modules\Core\Http\Requests\City\AddCityRequest;
use App\Modules\Core\Services\City\CityService;
use Illuminate\Http\JsonResponse;

/**
 * Class CityController
 *
 * @package App\Modules\Core\Http\Controllers
 */
class CityController extends Controller
{
    /**
     * Add city
     *
     * @param AddCityRequest $request
     *
     * @return JsonResponse
     */
    public function add(AddCityRequest $request): JsonResponse
    {
        $city = (New CityService())->add($request->get('city'), $request->get('country_id'));

        return $this->response('Success', compact('city'));
    }
}
