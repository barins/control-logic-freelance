<?php

namespace App\Modules\Core\Http\Requests\Category;

use App\Modules\Core\Http\Requests\FormRequest;

class DeleteCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->merge(['id' => $this->segment(5)]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        return [
            'id' => ['required', 'uuid', 'exists:categories,id'],
        ];
    }
}
