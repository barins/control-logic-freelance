<?php

namespace App\Modules\Core\Http\Requests\Category;

use App\Modules\Core\Http\Requests\FormRequest;

class AddCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        return [
            'value' => ['required', 'string'],
        ];
    }
}
