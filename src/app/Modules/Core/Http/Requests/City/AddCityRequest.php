<?php

namespace App\Modules\Core\Http\Requests\City;

use App\Modules\Core\Http\Requests\FormRequest;

class AddCityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function setRules(): array
    {
        return [
            'city' => ['required', 'string'],
            'country_id' => ['required', 'integer', 'exists:countries,id']
        ];
    }
}
