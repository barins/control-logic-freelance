<?php

namespace App\Modules\Core\Http\Requests;

use Illuminate\Support\Str;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest as LaravelFormRequest;

/**
 * Class FormRequest
 *
 * @property string $company_id
 *
 * @package App\Http\Requests
 */
abstract class FormRequest extends LaravelFormRequest
{
    public function __call($method, $parameters)
    {
        if(str_contains($method, 'set')) {
            $this->prepareDefaultRules($method);
        } else {
            return parent::__call($method, $parameters);
        }
    }

    public array $selectedRules = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize(): bool;

    /**
     * Set additional the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function setRules(): array;

    /**
     * Get all rules that apply to the request.
     *
     * @return array
     */
    final public function rules(): array
    {
        return mergeArray(
            $this->setRules(),
            $this->selectedRules,
        );
    }

    protected function prepareDefaultRules(string $method): void
    {
        $method = preg_split('/(?=[A-Z])/', $method, -1, PREG_SPLIT_NO_EMPTY);

        unset($method[array_key_first($method)], $method[array_key_last($method)]);

        $name_rule = Str::snake(implode($method));

        if(isset(config('validation-rules')[$name_rule])) {
            $this->selectedRules = array_merge($this->selectedRules, config('validation-rules')[$name_rule]);
        } else {
           throw new \RuntimeException("Category '{$name_rule}' of rules not found.");
        }
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator $validator
     * @return void
     *
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator): void
    {
        $errors = (new ValidationException($validator))->errors();

        $this->throwValidationException($errors);
    }

    /**
     * Custom validation exception
     *
     * @param array $errors
     *
     * @return void
     *
     * @throws HttpResponseException
     */
    public function throwValidationException(array $errors): void
    {
        throw new HttpResponseException(
             response()->api(
                'The given data was invalid.',
                [],
                $errors,
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            )
        );
    }
}
