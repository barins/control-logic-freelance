<?php

namespace App\Modules\Core\Model;

use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;

/**
 * Class Media
 *
 * @property string $id UUID
 *
 * @package App\Modules\Core\Model
 */
class Media extends BaseMedia
{
    public $incrementing = false;

    protected static function boot(): void
    {
        parent::boot();

        static::creating(static function (Media $model) {
            if (is_null($model->getOriginal('id'))) {
                $model->id = Str::uuid()->toString();
            }
        });
    }
}