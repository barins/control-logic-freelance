<?php

namespace App\Modules\Core\Model;

use App\Modules\Core\Traits\UsesUuid;

/**
 * Class Category
 *
 * @package App\Modules\Core\Model
 */
class Category extends BaseModel
{
    use UsesUuid;

    protected $table = 'categories';

    public static bool $usePresenter = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'master_user_id',
        'value',
    ];

    protected $hidden = [
        'master_user_id',
        'pivot',
        'created_at',
        'updated_at',
    ];
}
