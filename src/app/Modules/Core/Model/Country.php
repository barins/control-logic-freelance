<?php

namespace App\Modules\Core\Model;

use DateTime;
use App\Modules\Company\Model\Company;
use App\Modules\Core\Presenter\CountryPresenter;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Country
 *
 * @property int $id
 * @property string $name 'United States'
 * @property string $code 'US'
 * @property Company $companies
 * @property DateTime $created_at
 * @property DateTime $updated_at
 *
 * @package App\Modules\Core\Model
 */
class Country extends BaseModel
{
    protected string $presenter = CountryPresenter::class;

    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get companies for the country.
     */
    public function companies(): hasMany
    {
        return $this->hasMany(Company::class);
    }
}
