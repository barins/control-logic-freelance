<?php

namespace App\Modules\Core\Model;

use Spatie\MediaLibrary\HasMedia;
use Yaro\Presenter\PresenterTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Image\Exceptions\InvalidManipulation;
use App\Modules\Core\Presenter\PhotoMediaPresenter;

/**
 * Class BaseModel
 *
 * Base active record class
 *
 * @package App\Modules\Core\Model
 */
abstract class BaseModel extends Model implements HasMedia
{
    protected string $presenter;
    protected string $fullPresenter;

    public static bool $usePresenter = true;

    public string $mediaCollectionName;

    use InteractsWithMedia;
    use PresenterTrait;
    use Notifiable;

    /**
     * Attachments.
     *
     * @var array
     */
    public array $attachments = [

    ];

    /**
     * @throws InvalidManipulation
     */
    public function registerAllMediaConversions(): void
    {
        $this->addMediaConversion('short')
            ->width(180)
            ->height(180)
            ->format('png');

        $this->addMediaConversion('long')
            ->width(670)
            ->height(280)
            ->format('png');

        $this->addMediaConversion('medium')
            ->width(800)
            ->height(800)
            ->format('png');
    }

    public function preparePhotoMediaItem($media): array
    {
        return (new PhotoMediaPresenter($media))->toArray();
    }

    /**
     * Create a new instance of the given model.
     *
     * @param  array  $attributes
     * @param  bool  $exists
     * @return static
     */
    public function newInstance($attributes = [], $exists = false)
    {
       $model = parent::newInstance($attributes, $exists);

       if(isset($this->presenter)) {
           $model->presenter = $this->presenter;
       }

       return $model;
    }

    public static function disablePresenter(): self
    {
        self::$usePresenter = false;

        return (new static);
    }

    public function useFullPresenter(): BaseModel
    {
        $this->presenter = $this->fullPresenter;

        return $this;
    }

    final public function toArray(): array
    {
        if(static::$usePresenter) {
            $presenter = $this->getPresenterClass();
            if ($presenter) {
                $presenter = new $presenter($this);
                return $presenter->toArray();
            }
        }

        return array_merge($this->attributesToArray(), $this->relationsToArray());
    }
}