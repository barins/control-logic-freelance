<?php

namespace App\Modules\Core\Model;

use App\Modules\Core\Traits\UsesUuid;

/**
 * Class Tag
 *
 * @package App\Modules\Core\Model
 */
class Tag extends BaseModel
{
    use UsesUuid;

    protected $table = 'tags';

    public static bool $usePresenter = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'master_user_id',
        'value',
    ];

    protected $hidden = [
        'master_user_id',
        'pivot',
        'created_at',
        'updated_at',
    ];
}
