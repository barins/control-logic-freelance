<?php

namespace App\Modules\Core\Model;

use DateTime;
use App\Modules\MasterUser\Model\MasterUser;
use App\Modules\Core\Presenter\CityPresenter;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class City
 *
 * @property int $id
 * @property string $name 'Kiev'
 * @property Country $country
 * @property DateTime $created_at
 * @property DateTime $updated_at
 *
 * @package App\Modules\Core\City
 */
class City extends BaseModel
{
    protected string $presenter = CityPresenter::class;

    protected $table = 'cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'country_id',
        'master_user_id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get companies for the city.
     */
    public function country(): HasOne
    {
        return $this->HasOne(Country::class, 'id', 'country_id');
    }

    /**
     * Get master-user for the city.
     */
    public function master_user(): HasOne
    {
        return $this->HasOne(MasterUser::class, 'id', 'master_user_id');
    }
}
