<?php

namespace App\Modules\Core\Model;

use App\Modules\Core\Presenter\CurrencyPresenter;
use App\Modules\Core\Traits\UsesUuid;

/**
 * Class Currency
 *
 * @package App\Modules\Core\Model
 */
class Currency extends BaseModel
{
    use UsesUuid;

    protected $table = 'currencies';

    protected string $presenter = CurrencyPresenter::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'code',
        'symbol',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
