<?php

namespace App\Modules\Core\Database\Seeds;

use App\Modules\Core\Model\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $tags = [
            [
                'value' => 'Undefined',
            ],
            [
                'value' => 'Competitors',
            ],
            [
                'value' => 'Debtors',
            ],
            [
                'value' => 'Architecture',
            ],
            [
                'value' => 'Construction',
            ],
            [
                'value' => 'Real estate',
            ],
            [
                'value' => 'Consulting creative',
            ],
            [
                'value' => 'Education and non-profit activities',
            ],
            [
                'value' => 'Finance',
            ],
            [
                'value' => 'Insurance',
            ],
            [
                'value' => 'Legal services',
            ],
            [
                'value' => 'Production media',
            ],
            [
                'value' => 'Retail',
            ],
            [
                'value' => 'Export',
            ],
            [
                'value' => 'Local',
            ],
            [
                'value' => 'French',
            ],
            [
                'value' => 'German',
            ],
            [
                'value' => 'Italian',
            ],
            [
                'value' => 'Spanish',
            ],
            [
                'value' => 'Advertisement',
            ],
            [
                'value' => 'Cold call',
            ],
            [
                'value' => 'Facebook',
            ],
            [
                'value' => 'Google',
            ],
            [
                'value' => 'Promotion',
            ],
            [
                'value' => 'Directions',
            ],
            [
                'value' => 'Equipment',
            ],
            [
                'value' => 'Office tools',
            ],
            [
                'value' => 'Rent a room',
            ],
            [
                'value' => 'Logic',
            ],
        ];

        foreach ($tags as $tag) {
            Tag::create($tag);
        }
    }
}