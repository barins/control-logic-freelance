<?php

namespace App\Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Modules\Office\Model\Office;
use App\Modules\Company\Model\Company;
use App\Modules\MasterUser\Model\MasterUser;
use App\Modules\Company\Model\MasterUserCompany;

class FakeFirstLoadDataSeeder extends Seeder
{
    protected Office $office;
    protected Company $company;
    protected MasterUser $masterUser;

    protected string $passwordMasterUser;
    protected string $emailMasterUser;
    protected string $nameCompany;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $master_user = config('seed.default.master_user');
        $this->passwordMasterUser = $master_user['password'];
        $this->emailMasterUser = $master_user['email'];
        $this->nameCompany = $master_user['name_company'];

        if (MasterUser::where('email', $this->emailMasterUser)->exists()) {
            echo 'Fake master user already created!' . PHP_EOL;

            return;
        }

       $this->createMasterUser();

       $this->createOffice();
    }

    protected function createMasterUser(): void
    {
        $this->masterUser = factory(MasterUser::class)->create([
            'email' => $this->emailMasterUser,
            'password' => bcrypt($this->passwordMasterUser),
            'is_active' => 1,
            'confirmation_code' => '12345678',
        ]);

        $this->company = factory(Company::class)->create([
            'company_name' => $this->nameCompany
        ]);

        MasterUserCompany::create([
            'owner_id' => $this->masterUser->id,
            'company_id' =>  $this->company->id,
        ]);
    }

    protected function createOffice(): void
    {
        $this->office = factory(Office::class)->create([
            'company_id' => $this->company->id
        ]);
    }
}