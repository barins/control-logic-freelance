<?php

namespace App\Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Modules\Core\Model\City;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $cities = [
            [
                'name' => 'Kiev',
                'country_id' => 235,
            ],
            [
                'name' => 'Odessa',
                'country_id' => 235,
            ],
            [
                'name' => 'Poltava',
                'country_id' => 235,
            ]
        ];

        City::insert($cities);
    }
}