<?php

namespace App\Modules\Core\Database\Seeds;

use Illuminate\Database\Seeder;

class CoreDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(CurrencySeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(CitySeeder::class);
        $this->call(TagSeeder::class);
        $this->call(FakeFirstLoadDataSeeder::class);
    }
}