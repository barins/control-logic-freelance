<?php

namespace App\Modules\Core\Pagination;

use Illuminate\Pagination\LengthAwarePaginator as BaseLengthAwarePaginator;

class LengthAwarePaginator extends BaseLengthAwarePaginator
{
    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'current_page' => $this->currentPage(),
            'items' => $this->items->toArray(),
            'from_item' => $this->firstItem(),
            'to_item' => $this->lastItem(),
            'first_page_url' => $this->url(1),
            'last_page' => $this->lastPage(),
            'last_page_url' => $this->url($this->lastPage()),
            'next_page_url' => $this->nextPageUrl(),
            'path' => $this->path(),
            'per_page' => $this->perPage(),
            'prev_page_url' => $this->previousPageUrl(),
            'options' => $this->query,
            'total' => $this->total(),
        ];
    }
}
