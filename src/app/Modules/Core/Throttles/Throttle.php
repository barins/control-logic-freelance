<?php

namespace App\Modules\Core\Throttles;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Cache\RateLimiter;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class Throttle
 *
 * @package App\Modules\Core\Throttles
 */
abstract class Throttle
{
    public RateLimiter $limiter;
    public Request $request;
    public string $key;
    public int $maxAttempts = 5;
    public int $decaySeconds = 60;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->limiter = app(RateLimiter::class);
    }

    /**
     * Get the throttle key for the given request.
     *
     * @return string
     */
    protected function key(): string
    {
        return $this->key.'|'.Str::lower($this->request->{$this->key}).'|'.$this->request->ip();
    }

    /**
     * Determine if the user has too many failed attempts.
     *
     * @return bool
     */
    public function hasTooManyAttempts(): bool
    {
        return $this->limiter->tooManyAttempts($this->key(), $this->maxAttempts);
    }

    /**
     * Increment the attempts for the user.
     *
     * @return void
     */
    public function incrementAttempts(): void
    {
        $this->limiter->hit($this->key(), $this->decaySeconds);
    }

    /**
     * Redirect the user after determining they are locked out.
     *
     * @throws HttpResponseException
     */
    public function sendLockoutResponse(): HttpResponseException
    {
        $seconds = $this->limiter->availableIn($this->key());

        $response = response()->api(
            'Many attempts to complete the request.',
            [],
            [$this->key => [Lang::get('auth.throttle', ['seconds' => $seconds])]],
            Response::HTTP_TOO_MANY_REQUESTS
        );

        Log::info($response);

        throw new HttpResponseException($response);
    }

    /**
     * Clear the locks for the given user credentials.
     *
     * @return void
     */
    public function clearAttempts(): void
    {
        $this->limiter->clear($this->key());
    }

    /**
     * Fire an event when a lockout occurs.
     *
     * @return void
     */
    public function fireLockoutEvent(): void
    {
        event(new Lockout($this->request));
    }

    /**
     * Get the throttle key for the given request.
     *
     * @return void
     *
     * @throws ValidationException
     */
    public function check(): void
    {
        if ($this->hasTooManyAttempts()) {
            $this->sendLockoutResponse();
        }
    }
}
