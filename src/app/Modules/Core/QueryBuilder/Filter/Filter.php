<?php

namespace App\Modules\Core\QueryBuilder\Filter;

use Spatie\QueryBuilder\Filters\Filter as SpatieFilter;

abstract class Filter implements SpatieFilter
{
   abstract public function getTitle(): string;
}