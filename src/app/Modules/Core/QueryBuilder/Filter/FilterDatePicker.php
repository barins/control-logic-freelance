<?php

namespace App\Modules\Core\QueryBuilder\Filter;

use Illuminate\Database\Eloquent\Builder;

class FilterDatePicker extends Filter
{
    /**
     * @param  Builder  $query
     * @param  array   $value ['2020-08-20 10:00:00', '2022-08-12 10:00:00']
     * @param  string  $property
     */
    public function __invoke(Builder $query, $value, string $property): void
    {
        if (is_array($value)) {
            $query->whereBetween($property, [$value[0], $value[1]]);
        } else {
            $query->where($property, $value);
        }
    }

    public function getTitle(): string
    {
        return 'date_picker';
    }
}