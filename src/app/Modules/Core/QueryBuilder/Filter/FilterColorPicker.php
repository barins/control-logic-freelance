<?php

namespace App\Modules\Core\QueryBuilder\Filter;

use Illuminate\Database\Eloquent\Builder;

class FilterColorPicker extends Filter
{
    /**
     * @param  Builder $query
     * @param  string  $color
     * @param  string  $property
     */
    public function __invoke(Builder $query, $color, string $property): void
    {
        $query->where($property, '=', $color);
    }

    public function getTitle(): string
    {
        return 'color_picker';
    }
}