<?php

namespace App\Modules\Core\QueryBuilder\Filter;

use Illuminate\Database\Eloquent\Builder;

class FilterSelect extends Filter
{
    private string $relationName;
    private string $columnNameInRelationTable;

    public function __construct(
        string $relationName = null,
        string $columnNameInRelationTable = 'id'
    )
    {
        if($relationName) {
            $this->relationName = $relationName;
            $this->columnNameInRelationTable = $columnNameInRelationTable;
        }
    }

    /**
     * @param  Builder       $query
     * @param  string|array  $value
     * @param  string        $property
     */
    public function __invoke(Builder $query, $value, string $property): void
    {
        if(isset($this->relationName)) {
            $this->filterByRelation($query, $value);
        } else {
            $this->filterBase($query, $value, $property);
        }
    }

    /**
     * @param  Builder  $query
     * @param           $value
     * @param  string   $property
     */
    private function filterBase(Builder $query, $value, string $property): void
    {
        if(is_array($value)) {
            $query->whereIn($property, $value);
        } else {
            $query->where($property, '=', $value);
        }
    }

    /**
     * @param  Builder  $query
     * @param           $value
     */
    private function filterByRelation(Builder $query, $value): void
    {
        $query->whereHas($this->relationName, function ($query) use ($value) {
            if(is_array($value)) {
                $query->whereIn($this->columnNameInRelationTable, $value);
            } else {
                $query->where($this->columnNameInRelationTable, '=', $value);
            }
        });
    }

    public function getTitle(): string
    {
        return 'select';
    }
}