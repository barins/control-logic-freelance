<?php

namespace App\Modules\Core\QueryBuilder\Filter;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Exceptions\InvalidFilterValue;

class FilterExists extends Filter
{
    /**
     * @param  Builder  $query
     * @param  bool     $value
     * @param  string   $property
     * @throws InvalidFilterValue
     */
    public function __invoke(Builder $query, $value, string $property): void
    {
        if(! is_bool($value)) {
            throw InvalidFilterValue::make($value);
        }

        $operator = $value ? '!=' : '=';

        $query->where($property, $operator, null);
    }

    public function getTitle(): string
    {
        return 'exists';
    }
}