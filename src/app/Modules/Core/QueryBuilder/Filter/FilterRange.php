<?php

namespace App\Modules\Core\QueryBuilder\Filter;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Exceptions\InvalidFilterValue;

class FilterRange extends Filter
{
    /**
     * @param  Builder  $query
     * @param  array  $value
     * @param  string  $property
     * @throws InvalidFilterValue
     */
    public function __invoke(Builder $query, $value, string $property): void
    {
        if(! is_array($value)) {
            throw InvalidFilterValue::make($property);
        }

        $query->where($property, '>=', $value[0])
            ->where($property, '<=', $value[1]);
    }

    public function getTitle(): string
    {
        return 'range';
    }
}