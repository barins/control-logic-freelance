<?php

namespace App\Modules\Core\QueryBuilder\Filter;

use Illuminate\Database\Eloquent\Builder;

class FilterCheckBox extends Filter
{
    /**
     * @param  Builder  $query
     * @param  bool     $value
     * @param  string   $property
     */
    public function __invoke(Builder $query, $value, string $property): void
    {
        $query->where($property, '=', $value);
    }

    public function getTitle(): string
    {
        return 'check-box';
    }
}