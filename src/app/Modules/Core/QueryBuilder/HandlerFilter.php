<?php

namespace App\Modules\Core\QueryBuilder;

use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\Filters\Filter;

class HandlerFilter extends AllowedFilter
{
    public ?string $customIdentifier;

    public function getFilterClass(): Filter
    {
        return $this->filterClass;
    }

    /**
     * @return string|null
     */
    public function getCustomIdentifier(): ?string
    {
        return $this->customIdentifier;
    }

    /**
     * @param  string  $name
     * @param  Filter  $filterClass
     * @param  null    $internalName
     * @param  null    $customIdentifier
     *
     * @return static
     */
    public static function register(
        string $name,
        Filter $filterClass,
        $internalName = null,
        $customIdentifier = null
    ): self
    {
        $filter = self::custom($name, $filterClass, $internalName);

        $filter->customIdentifier = $customIdentifier;

        return $filter;
    }
}