<?php

namespace App\Modules\Core\Traits;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

trait UsesUuid
{
    protected static function bootUsesUuid(): void
    {
        static::creating(static function (Model $model) {
            if (! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }
}