<?php

namespace App\Modules\Core\Traits;

use App\Modules\Core\Model\BaseModel;
use ScoutElastic\Builders\FilterBuilder;
use ScoutElastic\Builders\SearchBuilder;
use ScoutElastic\Searchable as BaseSearchable;

trait Searchable
{
    use BaseSearchable;

    /**
     * Execute the search.
     *
     * @param  string  $query
     * @param  callable|null  $callback
     * @return FilterBuilder|SearchBuilder
     */
    public static function search($query, $callback = null)
    {
        $softDelete = static::usesSoftDelete() && config('scout.soft_delete', false);

        /** @var $model BaseModel */
        $model = new static;

        if(isset($model->fullPresenter)) {
            $model->presenter = $model->fullPresenter;
        }

        if ($query === '*') {
            return new FilterBuilder($model, $callback, $softDelete);
        }

        return new SearchBuilder($model, $query, $callback, $softDelete);
    }
}