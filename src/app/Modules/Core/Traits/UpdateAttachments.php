<?php

namespace App\Modules\Core\Traits;

use Illuminate\Support\Arr;
use App\Modules\Core\Model\Media;
use App\Modules\Core\Model\BaseModel;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;

trait UpdateAttachments
{
    private BaseModel $model;
    private array $attachment_update;

    private array $baseFields = [
        'code',
        'date',
    ];

    /**
     * @param  BaseModel  $model
     * @param  array      $attachments_update
     *
     * @return BaseModel
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function updateAttachments(
        BaseModel $model,
        array $attachments_update
    ): BaseModel {
        $this->model = $model;

        foreach ($attachments_update as $attachment_update) {
            if($this->model->{$attachment_update['type']} === null) {
                continue;
            }

            $this->attachment_update = $attachment_update;

            $this->updateFields();
            $this->updateFiles();
        }

        $this->model->save();

        return $this->model;
    }

    /**
     * Update simple fields in attachment.
     *
     * Example: update field 'code'.
     */
    private function updateFields(): void
    {
        $baseFields = Arr::except($this->attachment_update, ['type', 'file']);

        foreach ($baseFields as $attachmentName => $value) {
            $attachment = $this->model->{$this->attachment_update['type']};
            $attachment[$attachmentName] = $value;

            $this->model->{$this->attachment_update['type']} = $attachment;
        }
    }

    /**
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    private function updateFiles(): void
    {
        if(isset($this->attachment_update['file']['delete'])) {
            $this->deleteFiles();
        }

        if(isset($this->attachment_update['file']['add'])) {
            $this->addFiles();
        }
    }

    private function deleteFiles(): void
    {
        $attachment = $this->model->{$this->attachment_update['type']};

        foreach ($this->attachment_update['file']['delete'] as $id) {
            foreach ($attachment['file'] as $key => $file) {
                if($file['id'] === $id) {
                    Media::where('id', '=', $id)->first()->delete();

                    unset($attachment['file'][$key]);
                }
            }
        }

        $attachment['file'] = array_values($attachment['file']);

        $this->model->{$this->attachment_update['type']} = $attachment;
    }

    /**
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    private function addFiles(): void
    {
        $attachment = $this->model->{$this->attachment_update['type']};

        foreach ($this->attachment_update['file']['add'] as $file) {
            $attachment['file'][] = $this->model->preparePhotoMediaItem(
                $this->model->addMedia($file)->toMediaCollection($this->attachment_update['type'])
            );
        }

        $attachment['file'] = array_values($attachment['file']);

        $this->model->{$this->attachment_update['type']} = $attachment;
    }
}