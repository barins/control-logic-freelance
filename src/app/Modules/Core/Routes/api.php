<?php

use App\Modules\Core\Http\Controllers\TagController;
use App\Modules\Core\Http\Controllers\CityController;
use App\Modules\Core\Http\Controllers\CountryController;
use App\Modules\Core\Http\Controllers\CurrencyController;
use App\Modules\Core\Http\Controllers\CategoryController;
use App\Modules\Company\Http\Controllers\IndustryController;

/**
 * Routes of core
 */

Route::get('countries', [CountryController::class, 'get'])->name('countries.index');
Route::get('industries', [IndustryController::class, 'get'])->name('industries.index');
Route::get('currencies', [CurrencyController::class, 'get'])->name('currencies.index');

Route::group(['middleware' => 'auth:master_user'], static function () {
    Route::group(['prefix' => 'tags'], static function(){
        Route::get('/', [TagController::class, 'get'])->name('tags.index');
        Route::post('', [TagController::class, 'add'])->name('tags.add');
        Route::delete('{uuid}', [TagController::class, 'delete'])->name('tags.delete');
    });

    Route::group(['prefix' => 'categories'], static function(){
        Route::get('/', [CategoryController::class, 'get'])->name('categories.index');
        Route::post('', [CategoryController::class, 'add'])->name('categories.add');
        Route::delete('{uuid}', [CategoryController::class, 'delete'])->name('categories.delete');
    });

    Route::post('city', [CityController::class, 'add'])->name('city.add');
});
