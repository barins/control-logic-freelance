<?php

namespace App\Modules\Core\Services\BaseHandlers;

use Illuminate\Support\Facades\App;
use App\Modules\Core\Services\Handler;
use App\Modules\Core\Traits\UpdateAttachments;
use App\Modules\Core\Services\UploadAttachments\UploadAttachmentsInterface;

abstract class AttachmentHandler implements Handler
{
    use UpdateAttachments;

    protected UploadAttachmentsInterface $uploadAttachmentsService;

    public function __construct()
    {
        $this->uploadAttachmentsService = App::make(UploadAttachmentsInterface::class);
    }

}