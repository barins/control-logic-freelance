<?php

namespace App\Modules\Core\Services;

/**
 * Interface Handler
 *
 * @package App\Modules\Core\Services
 */
interface Handler
{
    public function handle();
}