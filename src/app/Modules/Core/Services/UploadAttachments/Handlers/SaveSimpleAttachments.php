<?php

namespace App\Modules\Core\Services\UploadAttachments\Handlers;

use App\Modules\Core\Model\BaseModel;
use App\Modules\Core\Services\Handler;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;

final class SaveSimpleAttachments extends BaseUploadAttachment implements Handler
{
    protected BaseModel $model;
    protected array $attachmentsNoCode;

    public function __construct(
        BaseModel $model,
        array $attachmentsNoCode
    )
    {
        $this->model = $model;
        $this->attachmentsNoCode = $attachmentsNoCode;
    }

    /**
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function handle(): BaseModel
    {
        foreach($this->attachmentsNoCode as $attachments) {
            $fieldInModel = $attachments['type'];

            $this->model->$fieldInModel = $this->model->preparePhotoMediaItem(
                $this->uploadAttachment($attachments['file'], $fieldInModel)
            );
        }

        return $this->model;
    }
}