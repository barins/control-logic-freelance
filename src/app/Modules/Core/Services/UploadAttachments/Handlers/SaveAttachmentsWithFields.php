<?php

namespace App\Modules\Core\Services\UploadAttachments\Handlers;

use Illuminate\Support\Arr;
use App\Modules\Core\Model\BaseModel;
use App\Modules\Core\Services\Handler;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;

final class SaveAttachmentsWithFields extends BaseUploadAttachment implements Handler
{
    protected BaseModel $model;
    protected array $attachmentsWithCode;

    public function __construct(
        BaseModel $model,
        array $attachmentsWithCode
    )
    {
        $this->model = $model;
        $this->attachmentsWithCode = $attachmentsWithCode;
    }

    /**
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function handle(): BaseModel
    {
        foreach($this->attachmentsWithCode as $attachments) {
            $fieldInModel = $attachments['type'];

            $uploadedAttachments = [];
            foreach($attachments['file'] as $file) {
                $uploadedAttachments[] = $this->model->preparePhotoMediaItem(
                    $this->uploadAttachment($file, $fieldInModel)
                );
            }

            $dataOfAttachments = Arr::except($attachments, ['type', 'file']);

            $this->model->$fieldInModel = mergeArray($dataOfAttachments, ['file' => $uploadedAttachments]);
        }

        return $this->model;
    }
}