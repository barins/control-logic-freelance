<?php

namespace App\Modules\Core\Services\UploadAttachments\Handlers;

use App\Modules\Core\Services\Handler;

final class PrepareAllAttachments implements Handler
{
    private array $allowedAttachments;
    private array $notPreparedAttachments;

    public function __construct(
        array $allowedAttachments,
        array $notPreparedAttachments
    )
    {
        $this->allowedAttachments = $allowedAttachments;
        $this->notPreparedAttachments = $notPreparedAttachments;
    }

    public function handle(): array
    {
        $attachments = [];
        $attachments['with_fields'] = [];
        $attachments['simple'] = [];

        foreach ($this->allowedAttachments as $allowed_attachment) {
            $notPreparedAttachment = $this->notPreparedAttachments[$allowed_attachment] ?? null;

            if (isset($notPreparedAttachment)) {
                if(is_array($notPreparedAttachment)) {
                    foreach ($notPreparedAttachment as $attachment) {
                        $attachment['type'] = $allowed_attachment;
                        $attachments['with_fields'][] = $attachment;
                    }
                    continue;
                }

                $attachments['simple'][] = [
                    'file' => $notPreparedAttachment,
                    'type' => $allowed_attachment,
                ];
            }
        }

        return $attachments;
    }
}