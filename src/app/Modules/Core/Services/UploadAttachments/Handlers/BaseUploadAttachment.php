<?php

namespace App\Modules\Core\Services\UploadAttachments\Handlers;

use Illuminate\Http\UploadedFile;
use App\Modules\Core\Model\BaseModel;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

abstract class BaseUploadAttachment
{
    protected BaseModel $model;

    /**
     * @param  UploadedFile  $attachment
     * @param  string  $collectionName
     *
     * @return Media
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    protected function uploadAttachment(UploadedFile $attachment, string $collectionName): Media
    {
        return $this->model->addMedia($attachment)->toMediaCollection($collectionName);
    }
}