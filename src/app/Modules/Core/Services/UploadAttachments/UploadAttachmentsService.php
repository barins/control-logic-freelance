<?php

namespace App\Modules\Core\Services\UploadAttachments;

use App\Modules\Core\Model\BaseModel;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use App\Modules\Core\Services\UploadAttachments\Handlers\{
    PrepareAllAttachments,
    SaveSimpleAttachments,
    SaveAttachmentsWithFields
};

final class UploadAttachmentsService implements UploadAttachmentsInterface
{
    private BaseModel $model;
    private array $preparedAttachments;

    /**
     * @inheritDoc
     */
    public function setModel(BaseModel $model): UploadAttachmentsService
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setAttachments(array $notPreparedAttachments): UploadAttachmentsService
    {
        $this->preparedAttachments = (New PrepareAllAttachments(
            $this->model->attachments,
            $notPreparedAttachments)
        )->handle();

        return $this;
    }

    /**
     * Upload in model.
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     *
     * @inheritDoc
     */
    public function upload(bool $inTransaction = false): BaseModel
    {
        $this->model = (New SaveSimpleAttachments($this->model, $this->preparedAttachments['simple']))->handle();
        $this->model = (New SaveAttachmentsWithFields($this->model, $this->preparedAttachments['with_fields']))->handle();

        $this->model->save();

        return $this->model;
    }
}