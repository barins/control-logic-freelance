<?php

namespace App\Modules\Core\Services\UploadAttachments;

use App\Modules\Core\Model\BaseModel;

/**
 * Interface UploadAttachmentsInterface
 *
 * @package App\Modules\Core\Services\UploadAttachments
 */
interface UploadAttachmentsInterface
{
    /**
     * Set the model for uploading attachments.
     *
     * @param  BaseModel  $model
     *
     * @return UploadAttachmentsInterface
     */
    public function setModel(BaseModel $model): UploadAttachmentsInterface;

    /**
     * Set available attachments.
     *
     * @param  array  $attachments
     *
     * @return UploadAttachmentsInterface
     */
    public function setAttachments(array $attachments): UploadAttachmentsInterface;

    /**
     * Upload attachments.
     *
     * @param  bool  $inTransaction
     *
     * @return mixed
     */
    public function upload(bool $inTransaction = false);
}