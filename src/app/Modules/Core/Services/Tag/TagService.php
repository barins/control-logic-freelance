<?php

namespace App\Modules\Core\Services\Tag;

use App\Modules\Core\Services\Tag\Handlers\{
    Add,
    Get,
    Delete
};

final class TagService
{
    public function get(): array
    {
        return (New Get())->handle();
    }

    public function add(string $tag_id): void
    {
        (New Add($tag_id))->handle();
    }

    public function delete(string $tag_id): void
    {
        (New Delete($tag_id))->handle();
    }
}