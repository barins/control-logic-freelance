<?php

namespace App\Modules\Core\Services\Tag\Handlers;

use App\Modules\Core\Model\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

/**
 * Class Get
 *
 * @package App\Modules\Core\Services\Tag\Handlers
 */
final class Get implements Handler
{
    protected ?string $master_user_id;

    public function __construct()
    {
        $this->master_user_id = Auth::guard(MasterUser::GUARD)->id();
    }

    public function handle(): array
    {
        return Cache::remember("tags.master_user.$this->master_user_id", config('cache.default-time'), function () {
            return $this->getAllTagsInDB();
        });
    }

    protected function getAllTagsInDB(): array
    {
        $builder = Tag::query();
        $builder = $builder->where('master_user_id', null);
        $builder = $builder->orWhere('master_user_id', $this->master_user_id);
        $builder = $builder->select(['value as name', 'id as value']);

        return $builder->get()->toArray();
    }
}