<?php

namespace App\Modules\Core\Services\Tag\Handlers;

use App\Modules\Core\Model\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

/**
 * @package App\Modules\Core\Services\Tag\Handlers
 */
final class Delete implements Handler
{
    protected string $tag_id;
    protected string $master_user_id;

    public function __construct(string $tag_id)
    {
        $this->tag_id = $tag_id;
        $this->master_user_id = Auth::guard(MasterUser::GUARD)->id();
    }

    public function handle(): void
    {
        $builder = Tag::query();
        $builder = $builder->where('id', $this->tag_id);
        $builder = $builder->Where('master_user_id', $this->master_user_id);
        $builder->delete();

        Cache::pull("tags.master_user.$this->master_user_id");
    }

}