<?php

namespace App\Modules\Core\Services\Tag\Handlers;

use App\Modules\Core\Model\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

/**
 * Class Add
 *
 * @package App\Modules\Core\Services\Tag\Handlers
 */
final class Add implements Handler
{
    protected string $tag;
    protected string $master_user_id;

    public function __construct(string $tag)
    {
        $this->tag = $tag;
        $this->master_user_id = Auth::guard(MasterUser::GUARD)->id();
    }

    public function handle(): void
    {
        Tag::create([
            'value' => $this->tag,
            'master_user_id' => $this->master_user_id,
        ]);

        Cache::pull("tags.master_user.$this->master_user_id");
    }

}