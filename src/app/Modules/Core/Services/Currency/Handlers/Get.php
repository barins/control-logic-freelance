<?php

namespace App\Modules\Core\Services\Currency\Handlers;

use App\Modules\Core\Model\Currency;
use App\Modules\Core\Services\Handler;

final class Get implements Handler
{
    public function handle(): array
    {
        return Currency::disablePresenter()->orderBy('name')->get()->toArray();
    }
}