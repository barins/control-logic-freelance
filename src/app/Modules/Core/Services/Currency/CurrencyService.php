<?php

namespace App\Modules\Core\Services\Currency;

use App\Modules\Core\Services\Currency\Handlers\Get;

final class CurrencyService
{
    public function get(): array
    {
        return (New Get())->handle();
    }
}