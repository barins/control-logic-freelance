<?php

namespace App\Modules\Core\Services\Category;

use App\Modules\Core\Services\Category\Handlers\{
    Add,
    Get,
    Delete
};

final class CategoryService
{
    public function get(): array
    {
        return (New Get())->handle();
    }

    public function add(string $tag_id): void
    {
        (New Add($tag_id))->handle();
    }

    public function delete(string $tag_id): void
    {
        (New Delete($tag_id))->handle();
    }
}