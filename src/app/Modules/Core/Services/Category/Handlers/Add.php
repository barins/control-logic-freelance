<?php

namespace App\Modules\Core\Services\Category\Handlers;

use App\Modules\Core\Model\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

/**
 * Class Add
 *
 * @package App\Modules\Core\Services\Category\Handlers
 */
final class Add implements Handler
{
    protected string $tag;
    protected string $master_user_id;

    public function __construct(string $tag)
    {
        $this->tag = $tag;
        $this->master_user_id = Auth::guard(MasterUser::GUARD)->id();
    }

    public function handle(): void
    {
        Category::create([
            'value' => $this->tag,
            'master_user_id' => $this->master_user_id,
        ]);

        Cache::pull("categories.master_user.$this->master_user_id");
    }

}