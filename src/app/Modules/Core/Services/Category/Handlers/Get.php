<?php

namespace App\Modules\Core\Services\Category\Handlers;

use App\Modules\Core\Model\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;

/**
 * Class Get
 *
 * @package App\Modules\Core\Services\Category\Handlers
 */
final class Get implements Handler
{
    protected ?string $master_user_id;

    public function __construct()
    {
        $this->master_user_id = Auth::guard(MasterUser::GUARD)->id();
    }

    public function handle(): array
    {
        return Cache::remember("categories.master_user.$this->master_user_id", config('cache.default-time'), function () {
            return $this->getAllCategoriesInDB();
        });
    }

    protected function getAllCategoriesInDB(): array
    {
        $builder = Category::query();
        $builder = $builder->where('master_user_id', null);
        $builder = $builder->orWhere('master_user_id', $this->master_user_id);
        $builder = $builder->select(['value as name', 'id as value']);

        return $builder->get()->toArray();
    }
}