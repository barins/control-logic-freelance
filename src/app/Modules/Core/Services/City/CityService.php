<?php

namespace App\Modules\Core\Services\City;

use App\Modules\Core\Services\City\Handlers\Add;

/**
 * Class CityService
 *
 * @package App\Modules\Core\Services\City
 */
class CityService
{
    public function add(string $city, string $country_id): array
    {
        return (New Add($city, $country_id))->handle();
    }
}