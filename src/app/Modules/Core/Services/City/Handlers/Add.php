<?php

namespace App\Modules\Core\Services\City\Handlers;

use App\Modules\Core\Model\City;
use App\Modules\Core\Services\Handler;
use App\Modules\MasterUser\Model\MasterUser;
use Illuminate\Support\Facades\Auth;

/**
 * Class Add
 *
 * @package App\Modules\Core\Services\City\Handlers
 */
class Add implements Handler
{
    private string $city;
    private string $country_id;

    public function __construct(string $city, string $country_id)
    {
        $this->city = $city;
        $this->country_id = $country_id;
    }

    public function handle(): array
    {
       $city = City::create([
           'name' => $this->city,
           'country_id' => $this->country_id,
           'master_user_id' => $this->getMasterUserID()
       ]);

       return $city->toArray();
    }

    private function getMasterUserID(): string
    {
        return Auth::guard(MasterUser::GUARD)->id();
    }
}