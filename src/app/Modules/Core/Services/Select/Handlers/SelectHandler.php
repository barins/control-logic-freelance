<?php

namespace App\Modules\Core\Services\Select\Handlers;

/**
 * Interface Handler
 *
 * @package App\Modules\Office\Services\Select\Handlers
 */
interface SelectHandler
{
    public function handle();
}