<?php

namespace App\Modules\Core\Services\Select;

use Illuminate\Support\Str;

abstract class SelectService
{
    protected string $moduleName;

    /**
     * @param  string  $nameOfSelect
     * @param  null|string|array  $param
     *
     * @return array
     */
    public function getOptions(
        string $nameOfSelect,
        $param = null
    ): array
    {
        $class_name = Str::studly($nameOfSelect);

        if(! file_exists($this->preparePath($class_name))) {
            throw new \RuntimeException("Select '{$class_name}' does not exist");
        }

        $select = "App\Modules\\$this->moduleName\Services\Select\Handlers\\{$class_name}";

        return (New $select($param))->handle();
    }

    protected function preparePath(string $class_name): string
    {
        $class_path = str_replace('Core', $this->moduleName, __DIR__);
        $class_path = "$class_path/Handlers/$class_name.php";
        $class_path = str_replace('/', DIRECTORY_SEPARATOR, $class_path);

        return $class_path;
    }
}