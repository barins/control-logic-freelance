<?php

namespace App\Modules\Core\Services\Country\Handlers;

use App\Modules\Core\Model\Country;
use Illuminate\Support\Facades\Cache;
use App\Modules\Core\Services\Handler;

/**
 * Class Get
 *
 * @package App\Modules\Core\Services\Country\Handlers
 */
class Get implements Handler
{
    public function handle(): array
    {
        return Cache::remember('countries', config('cache.default-time'), function () {
            return $this->getAllCountriesInDB();
        });
    }

    protected function getAllCountriesInDB(): array
    {
        return Country::all()->toArray();
    }
}