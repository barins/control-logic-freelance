<?php

namespace App\Modules\Core\Services\Country;

use App\Modules\Core\Services\Country\Handlers\Get;

/**
 * Class CountryService
 *
 * @package App\Modules\Core\Services\Country
 */
class CountryService
{
    public function get(): array
    {
        return (New Get())->handle();
    }
}