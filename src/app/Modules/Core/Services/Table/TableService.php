<?php

namespace App\Modules\Core\Services\Table;

/**
 * Interface TableService
 *
 * @package App\Modules\Core\Services\Table
 */
interface TableService
{
    /**
     * Get all entities given filters for client-side table building.
     *
     * @param string $company_id
     *
     * @return array
     */
    public function getItems(string $company_id): array;

    /**
     * Get the structure of the table.
     * Division by group.
     *
     * @return array
     */
    public function getTableStructure(): array;

    /**
     * Get all columns for building table.
     *
     * @return array
     */
    public function getColumns(): array;

    /**
     * @param  string  $filter_name
     * @param  string  $company_id
     *
     * @return array
     */
    public function getOptionsForSelectFilter(
        string $filter_name,
        string $company_id
    ): array;
}