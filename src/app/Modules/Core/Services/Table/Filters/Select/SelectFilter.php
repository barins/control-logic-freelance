<?php

namespace App\Modules\Core\Services\Table\Filters\Select;

interface SelectFilter {
    public function __construct(string $filter_name, string $company_id);

    /***
     * This function sends a prepared structure for building a client-side select filter.
     *
     * Example:
     *  select:
     *   option:
     *    {name} => {value}
     *
     * @return array
     */
    public function getPrepareData(): array;
}