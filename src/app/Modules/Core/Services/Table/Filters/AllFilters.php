<?php

namespace App\Modules\Core\Services\Table\Filters;

use App\Modules\Core\QueryBuilder\Filter\ {
    FilterColorPicker,
    FilterDatePicker,
    FilterCheckBox,
    FilterExists,
    FilterSelect,
    FilterRange,
};

abstract class AllFilters
{
   abstract public function get(): array;

   public function exists(): FilterExists
   {
       return New FilterExists();
   }

   public function checkBox(): FilterCheckBox
   {
       return New FilterCheckBox();
   }

   public function select(
       string $relationName = null,
       string $columnNameInRelationTable = 'id'
   ): FilterSelect
   {
       return New FilterSelect($relationName, $columnNameInRelationTable);
   }

   public function range(): FilterRange
   {
       return New FilterRange();
   }

   public function datePicker(): FilterDatePicker
   {
       return New FilterDatePicker();
   }

   public function colorPicker(): FilterColorPicker
   {
       return New FilterColorPicker();
   }
}