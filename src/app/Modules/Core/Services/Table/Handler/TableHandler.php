<?php

namespace App\Modules\Core\Services\Table\Handler;

use App\Modules\Core\Services\Handler;
use App\Modules\Core\QueryBuilder\HandlerFilter;
use App\Modules\Core\Services\Table\Filters\AllFilters;

abstract class TableHandler implements Handler
{
    public array $filters;

    public function __construct(AllFilters $filter)
    {
        $this->filters = $filter->get();
    }

    protected function prepareFiltersForClient(): array
    {
        $filters = [];

        foreach ($this->filters as $filter) {
            if($filter instanceof HandlerFilter) {
                $new_filter['type'] =  $filter->getFilterClass()->getTitle();
                $new_filter['identifier'] = $filter->getCustomIdentifier() ?? $filter->getInternalName();
                $new_filter['title'] = $filter->getName();

                $filters[] = $new_filter;
            }
        }

        return $filters;
    }
}