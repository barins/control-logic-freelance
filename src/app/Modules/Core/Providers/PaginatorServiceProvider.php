<?php

namespace App\Modules\Core\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Laravel\Scout\Builder as ScoutBuilder;
use App\Modules\Core\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Pagination\LengthAwarePaginator as BaseLengthAwarePaginator;

use function Symfony\Component\String\s;

class PaginatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * TODO: NOT DONE
     * @return void
     */
    public function boot(): void
    {
        EloquentBuilder::macro(config('api-paginate.method_name'), function (int $maxResults = null, int $defaultSize = null) {
            $maxResults = $maxResults ?? config('api-paginate.max_results');
            $defaultSize = $defaultSize ?? config('api-paginate.default_size');
            $numberParameter = config('api-paginate.number_parameter');
            $sizeParameter = config('api-paginate.size_parameter');
            $paginationParameter = config('api-paginate.pagination_parameter');

            $size = (int) request()->input($paginationParameter.'.'.$sizeParameter, $defaultSize);

            $size = $size > $maxResults ? $maxResults : $size;

            $paginator = $this
                ->paginate($size, ['*'], $paginationParameter.'.'.$numberParameter)
                ->setPageName($paginationParameter.'['.$numberParameter.']')
                ->appends(Arr::except(request()->input(), $paginationParameter.'.'.$numberParameter));

            if (! is_null(config('api-paginate.base_url'))) {
                $paginator->setPath(config('api-paginate.base_url'));
            }

            return $paginator;
        });


        ScoutBuilder::macro(config('api-paginate.method_name'), function (int $maxResults = null, int $defaultSize = null) {
            $maxResults = $maxResults ?? config('api-paginate.max_results');
            $defaultSize = $defaultSize ?? config('api-paginate.default_size');
            $numberParameter = config('api-paginate.number_parameter');
            $sizeParameter = config('api-paginate.size_parameter');
            $paginationParameter = config('api-paginate.pagination_parameter');

            $size = (int) request()->input($paginationParameter.'.'.$sizeParameter, $defaultSize);
            $number = (int) request()->input($paginationParameter.'.'.$numberParameter);

            $paginator = $this
                ->paginate($size, ['*'], $number)
                ->setPageName($paginationParameter.'['.$numberParameter.']')
                ->appends(Arr::except(request()->input(), $paginationParameter.'.'.$numberParameter));

            if (! is_null(config('api-paginate.base_url'))) {
                $paginator->setPath(config('api-paginate.base_url'));
            }

            return $paginator;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(BaseLengthAwarePaginator::class, LengthAwarePaginator::class);
    }
}
