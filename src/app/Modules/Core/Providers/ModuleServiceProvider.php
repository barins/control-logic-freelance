<?php

namespace App\Modules\Core\Providers;

use Caffeinated\Modules\Support\ServiceProvider;
use App\Modules\Core\Services\UploadAttachments\{
    UploadAttachmentsInterface,
    UploadAttachmentsService,
};

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadTranslationsFrom(module_path('core', 'Resources/Lang', 'app'), 'core');
        $this->loadViewsFrom(module_path('core', 'Resources/Views', 'app'), 'core');
        $this->loadMigrationsFrom(module_path('core', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('core', 'Config', 'app'));
            $this->mergeConfigFrom(module_path('core', 'Validation') . "/validation-rules.php", 'validation-rules');
        }
        $this->loadFactoriesFrom(module_path('core', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(BuilderServiceProvider::class);
        $this->app->register(PaginatorServiceProvider::class);

        $this->app->bind(UploadAttachmentsInterface::class, UploadAttachmentsService::class);
    }
}
