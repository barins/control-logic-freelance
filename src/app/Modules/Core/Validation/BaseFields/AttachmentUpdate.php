<?php

namespace App\Modules\Core\Validation\BaseFields;

use Illuminate\Validation\Rule;

final class AttachmentUpdate extends BaseField
{
    public function getRules(): array
    {
        return [
            'attachments_update' => ['array'],

            'attachments_update.*.type' => ['string', 'required', Rule::in($this->property)],

            'attachments_update.*.file.delete' => ['array'],
            'attachments_update.*.file.delete.*' => ['uuid'],

            'attachments_update.*.file.add' => ['array'],
            'attachments_update.*.file.add.*' => ['file', 'mimes:jpeg,png,jpg,gif,svg,doc,docx,xlsx,pdf,jpg,png,numbers,pages', 'max:10000'],

            'attachments_update.*.code' => ['string'],
            'attachments_update.*.date' => ['string', 'date_format:Y-m-d H:s'],
       ];
    }
}