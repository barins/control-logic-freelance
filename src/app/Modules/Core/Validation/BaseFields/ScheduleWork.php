<?php

namespace App\Modules\Core\Validation\BaseFields;

final class ScheduleWork extends BaseField
{
    public function getRules(): array
    {
        return [
            $this->property => ["array"],

            //Monday
            "{$this->property}.monday" => ["array", "required_with:{$this->property}"],
            "{$this->property}.monday.from" => ["date_format:H:i", "required_if:{$this->property}.monday.enabled,==,true"],
            "{$this->property}.monday.to" => ["date_format:H:i", "required_if:{$this->property}.monday.enabled,==,true"],
            "{$this->property}.monday.enabled" => ["bool", "required_with:{$this->property}"],

            //Tuesday
            "{$this->property}.tuesday" => ["array", "required_with:{$this->property}"],
            "{$this->property}.tuesday.from" => ["date_format:H:i", "required_if:{$this->property}.tuesday.enabled,==,true"],
            "{$this->property}.tuesday.to" => ["date_format:H:i", "required_if:{$this->property}.tuesday.enabled,==,true"],
            "{$this->property}.tuesday.enabled" => ["bool", "required_with:{$this->property}"],

            //Wednesday
            "{$this->property}.wednesday" => ["array", "required_with:{$this->property}"],
            "{$this->property}.wednesday.from" => ["date_format:H:i", "required_if:{$this->property}.wednesday.enabled,==,true"],
            "{$this->property}.wednesday.to" => ["date_format:H:i", "required_if:{$this->property}.wednesday.enabled,==,true"],
            "{$this->property}.wednesday.enabled" => ["bool", "required_with:{$this->property}"],

            //Thursday
            "{$this->property}.thursday" => ["array", "required_with:{$this->property}"],
            "{$this->property}.thursday.from" => ["date_format:H:i", "required_if:{$this->property}.thursday.enabled,==,true"],
            "{$this->property}.thursday.to" => ["date_format:H:i", "required_if:{$this->property}.thursday.enabled,==,true"],
            "{$this->property}.thursday.enabled" => ["bool", "required_with:{$this->property}"],

            //Friday
            "{$this->property}.friday" => ["array", "required_with:{$this->property}"],
            "{$this->property}.friday.from" => ["date_format:H:i", "required_if:{$this->property}.friday.enabled,==,true"],
            "{$this->property}.friday.to" => ["date_format:H:i", "required_if:{$this->property}.friday.enabled,==,true"],
            "{$this->property}.friday.enabled" => ["bool", "required_with:{$this->property}"],

            //Saturday
            "{$this->property}.saturday" => ["array", "required_with:{$this->property}"],
            "{$this->property}.saturday.from" => ["date_format:H:i", "required_if:{$this->property}.saturday.enabled,==,true"],
            "{$this->property}.saturday.to" => ["date_format:H:i", "required_if:{$this->property}.saturday.enabled,==,true"],
            "{$this->property}.saturday.enabled" => ["bool", "required_with:{$this->property}"],

            //Sunday
            "{$this->property}.sunday" => ["array", "required_with:{$this->property}"],
            "{$this->property}.sunday.from" => ["date_format:H:i", "required_if:{$this->property}.sunday.enabled,==,true"],
            "{$this->property}.sunday.to" => ["date_format:H:i", "required_if:{$this->property}.sunday.enabled,==,true"],
            "{$this->property}.sunday.enabled" => ["bool", "required_with:{$this->property}"],
       ];
    }
}