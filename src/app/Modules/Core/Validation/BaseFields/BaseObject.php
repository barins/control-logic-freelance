<?php

namespace App\Modules\Core\Validation\BaseFields;

use Illuminate\Validation\Rule;

final class BaseObject extends BaseField
{
    /**
     * @return array
     *
     * Example:
     * $this->property = [
     *    [
     *       'name' => 'ExampleName',
     *       'value => 'ExampleValue',
     *    ],
     *    ...
     * ]
     */
    public function getRules(): array
    {
        return [
            $this->property => ['array'],
            $this->property.'.*.name' => ['string', 'required'],
            $this->property.'.*.value' => ['string', 'required'],
       ];
    }
}