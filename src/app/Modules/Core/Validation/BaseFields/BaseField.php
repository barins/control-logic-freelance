<?php

namespace App\Modules\Core\Validation\BaseFields;

abstract class BaseField
{
    /**
     * @var string|array $property
     */
    protected $property;

    public function __construct($property)
    {
        $this->property = $property;
    }

    abstract public function getRules(): array;
}