<?php

namespace App\Modules\Core\Validation\BaseFields;

final class AttachmentCreate extends BaseField
{
    public function getRules(): array
    {
        return [
            $this->property => ["array"],

            "$this->property.*.file" => ['array', "required_with:$this->property.*.code"],
            "$this->property.*.file.*" => ['file', 'mimes:jpeg,png,jpg,gif,svg,doc,docx,xlsx,pdf,jpg,png,numbers,pages', 'max:10000'],

            "$this->property.*.code" => ['string', "required_with:$this->property.*.file"],
            "$this->property.*.date" => ['string', 'date_format:Y-m-d H:s'],
       ];
    }
}