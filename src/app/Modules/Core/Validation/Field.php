<?php

namespace App\Modules\Core\Validation;

use RuntimeException;
use Illuminate\Support\Str;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class Field
 *
 * Used for validation @var Validator.
 *
 * Indicates the type of the field and if it is an array the structure.
 *
 * @package App\Modules\Core\Validation\Fields
 */
final class Field
{
    /**
     * @param  string  $fieldName
     * @param  string|array  $property
     *
     * Specify the field to check.
     *
     * @return array
     */
    public static function get(string $fieldName, $property = null): array
    {
        $class_name = Str::studly($fieldName);

        if(! file_exists(self::preparePath($class_name))) {
            throw new RuntimeException("Field '{$class_name}' does not exist");
        }

        $class = "\App\Modules\Core\Validation\BaseFields\\{$class_name}";

        $fieldName = $property ?? $fieldName;

        return (New $class($fieldName))->getRules();
    }

    protected static function preparePath(string $class_name): string
    {
        $class_path = __DIR__ . "/BaseFields/$class_name.php";
        $class_path = str_replace('/', DIRECTORY_SEPARATOR, $class_path);

        return $class_path;
    }
}