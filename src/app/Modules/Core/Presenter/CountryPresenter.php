<?php

namespace App\Modules\Core\Presenter;

use Yaro\Presenter\AbstractPresenter;

/**
 * Class CompanyPresenter
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class CountryPresenter extends AbstractPresenter
{
    protected $arrayable = [
        'id',
        'name',
        'code',
    ];
}