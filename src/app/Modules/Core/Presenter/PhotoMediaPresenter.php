<?php

namespace App\Modules\Core\Presenter;

use Yaro\Presenter\AbstractPresenter;

class PhotoMediaPresenter extends AbstractPresenter
{
    protected $arrayable = [
        'id',
        'short_url',
        'long_url',
        'medium_url',
        'original_url'
    ];

    public function getShortUrlPresent()
    {
        return $this->model->getFullUrl('short');
    }

    public function getLongUrlPresent()
    {
        return $this->model->getFullUrl('long');
    }

    public function getMediumUrlPresent()
    {
        return $this->model->getFullUrl('medium');
    }

    public function getOriginalUrlPresent()
    {
        return $this->model->getFullUrl();
    }
}