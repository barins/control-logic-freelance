<?php

namespace App\Modules\Core\Presenter;

use App\Modules\Core\Model\City;
use Yaro\Presenter\AbstractPresenter;

/**
 * Class CompanyPresenter
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class CityPresenter extends AbstractPresenter
{
    /**
     * @var $model City $model
     */
    protected $model;

    protected $arrayable = [
        'id',
        'country',
        'name',
    ];

    public function getCountryPresent(): array
    {
        return $this->model->country->toArray();
    }
}