<?php

namespace App\Modules\Core\Presenter;

use App\Modules\Core\Model\Media;
use Yaro\Presenter\AbstractPresenter;

class BaseMediaPresenter extends AbstractPresenter
{
    /**
     * @var $model Media
     */
    protected $model;

    protected $arrayable = [
        'id',
        'original_url',
        'size',
        'mime',
    ];

    public function getOriginalUrlPresent()
    {
        return $this->model->getFullUrl();
    }

    public function getSizePresent()
    {
        return $this->model->size;
    }

    public function getMimePresent()
    {
        return $this->model->mime_type;
    }
}