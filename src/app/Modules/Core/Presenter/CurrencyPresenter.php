<?php

namespace App\Modules\Core\Presenter;

use Yaro\Presenter\AbstractPresenter;

/**
 * Class CurrencyPresenter
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string symbol
 */
class CurrencyPresenter extends AbstractPresenter
{
    protected $arrayable = [
        'name',
        'code',
        'symbol',
    ];
}