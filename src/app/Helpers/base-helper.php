<?php

use Illuminate\Support\Arr;

if (! function_exists('mergeArray')) {
    /**
     * Merges the configs together and takes multi-dimensional arrays into account.
     *
     * @param  array  $original
     * @param  array  $merging
     *
     * @return array
     */
    function mergeArray(array $original, array $merging): array
    {
        $array = array_merge($original, $merging);

        foreach ($original as $key => $value) {
            if (! is_array($value)) {
                continue;
            }

            if (! Arr::exists($merging, $key)) {
                continue;
            }

            if (is_numeric($key)) {
                continue;
            }

            $array[$key] = mergeArray($value, $merging[$key]);
        }

        return $array;
    }
}