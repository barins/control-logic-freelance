<?php

/**
 * Configuring settings for communicating with REST-API
 */

return [
    'version' => [
        'current' => '1.0',
        'all' => ['1.0'],
    ]
];