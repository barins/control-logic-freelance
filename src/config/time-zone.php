<?php

/**
 * Available Timezones
 */
return [
    [
        'name' => 'GMT Greenwich Mean Time',
        'value' => 'GMT Greenwich Mean Time'
    ],
    [
        'name' => 'GMT Universal Coordinated Time',
        'value' => 'GMT Universal Coordinated Time'
    ],
    [
        'name' => 'GMT+1:00 European Central Time',
        'value' => 'GMT+1:00 European Central Time'
    ],
    [
        'name' => 'GMT+2:00 Eastern European Time',
        'value' => 'GMT+2:00 Eastern European Time'
    ],
    [
        'name' => 'GMT+2:00 (Arabic) Egypt Standard Time',
        'value' => 'GMT+2:00 (Arabic) Egypt Standard Time'
    ],
    [
        'name' => 'GMT+3:00 Eastern African Time',
        'value' => 'GMT+3:00 Eastern African Time'
    ],
    [
        'name' => 'GMT+3:30 Middle East Time',
        'value' => 'GMT+3:30 Middle East Time'
    ],
    [
        'name' => 'GMT+4:00 Near East Time',
        'value' => 'GMT+4:00 Near East Time'
    ],
    [
        'name' => 'GMT+5:00 Pakistan Lahore Time',
        'value' => 'GMT+5:00 Pakistan Lahore Time'
    ],
    [
        'name' => 'GMT+5:30 India Standard Time',
        'value' => 'GMT+5:30 India Standard Time'
    ],
    [
        'name' => 'GMT+6:00 Bangladesh Standard Time',
        'value' => 'GMT+6:00 Bangladesh Standard Time'
    ],
    [
        'name' => 'GMT+7:00 Vietnam Standard Time',
        'value' => 'GMT+7:00 Vietnam Standard Time'
    ],
    [
        'name' => 'GMT+8:00 China Taiwan Time',
        'value' => 'GMT+8:00 China Taiwan Time'
    ],
    [
        'name' => 'GMT+9:00 Japan Standard Time',
        'value' => 'GMT+9:00 Japan Standard Time'
    ],
    [
        'name' => 'GMT+9:30 Australia Central Time',
        'value' => 'GMT+9:30 Australia Central Time'
    ],
    [
        'name' => 'GMT+10:00 Australia Eastern Time',
        'value' => 'GMT+10:00 Australia Eastern Time'
    ],
    [
        'name' => 'GMT+11:00 Solomon Standard Time',
        'value' => 'GMT+11:00 Solomon Standard Time'
    ],
    [
        'name' => 'GMT+12:00 New Zealand Standard Time',
        'value' => 'GMT+12:00 New Zealand Standard Time'
    ],
    [
        'name' => 'GMT-11:00 Midway Islands Time',
        'value' => 'GMT-11:00 Midway Islands Time'
    ],
    [
        'name' => 'GMT-10:00 Hawaii Standard Time',
        'value' => 'GMT-10:00 Hawaii Standard Time'
    ],
    [
        'name' => 'GMT-9:00 Alaska Standard Time',
        'value' => 'GMT-9:00 Alaska Standard Time'
    ],
    [
        'name' => 'GMT-8:00 Pacific Standard Time',
        'value' => 'GMT-8:00 Pacific Standard Time'
    ],
    [
        'name' => 'GMT-7:00 Phoenix Standard Time',
        'value' => 'GMT-7:00 Phoenix Standard Time'
    ],
    [
        'name' => 'GMT-7:00 Mountain Standard Time',
        'value' => 'GMT-7:00 Mountain Standard Time'
    ],
    [
        'name' => 'GMT-6:00 Central Standard Time',
        'value' => 'GMT-6:00 Central Standard Time'
    ],
    [
        'name' => 'GMT-5:00 Eastern Standard Time',
        'value' => 'GMT-5:00 Eastern Standard Time'
    ],
    [
        'name' => 'GMT-5:00 Indiana Eastern Standard Time',
        'value' => 'GMT-5:00 Indiana Eastern Standard Time'
    ],
    [
        'name' => 'GMT-4:00 Puerto Rico and US Virgin Islands Time',
        'value' => 'GMT-4:00 Puerto Rico and US Virgin Islands Time'
    ],
    [
        'name' => 'GMT-3:30 Canada Newfoundland Time',
        'value' => 'GMT-3:30 Canada Newfoundland Time'
    ],
    [
        'name' => 'GMT-3:00 Brazil Eastern Time',
        'value' => 'GMT-3:00 Brazil Eastern Time'
    ],
    [
        'name' => 'GMT-3:00 Argentina Standard Time',
        'value' => 'GMT-3:00 Argentina Standard Time'
    ],
    [
        'name' => 'GMT-1:00 Central African Time',
        'value' => 'GMT-1:00 Central African Time'
    ],
];