<?php

/**
 * Configuration for setting up the seeds launch
 */
return [
    'default' => [
        'master_user' => [
            'email' => env('MASTER_USER_DEFAULT_EMAIL', 'test@test.com'),
            'password' => env('MASTER_USER_DEFAULT_PASSWORD', '123password123'),
            'name_company' => env('MASTER_USER_DEFAULT_COMPANY_NAME', 'CompanyForTest')
        ],
    ]
];
