<?php

namespace Tests\Feature\App\Modules\MasterUser\Http\Controllers\Workspace;

use Tests\Feature\App\Modules\MasterUser\BaseCase;

final class TeacherControllerTest extends BaseCase
{
   public function testWorkspaceTeacher(): void
   {
       $response = $this->post(route('workspace-teacher.teacher.initialization'));

       self::assertEquals('Unauthenticated.', $this->getMessageFromHttpRequest($response));

       $this->baseLogin();

       $this->post(route('workspace-teacher.teacher.initialization'), $this->getAuthHeaders())->assertSuccessful();

       $this->post(route('workspace-teacher.teacher.deactivate'), $this->getAuthHeaders())->assertSuccessful();

       $this->post(route('workspace-teacher.teacher.activate'), $this->getAuthHeaders())->assertSuccessful();

       $response = $this->get(route('workspace-teacher.teacher.status'), $this->getAuthHeaders());

       $response = $this->getBodyFromHttpRequest($response);

       self::assertTrue($response['workspace-teacher']['activated']);
   }
}