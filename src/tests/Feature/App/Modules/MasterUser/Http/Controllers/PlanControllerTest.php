<?php

namespace Tests\Feature\App\Modules\MasterUser\Http\Controllers;

use Tests\Feature\App\Modules\MasterUser\BaseCase;

final class PlanControllerTest extends BaseCase
{
   public function testGetAllSubscriptions(): void
   {
       $this->baseLogin();

       $response = $this->get(route('subscription.plans.index'));

       $response->assertSuccessful();

       $response = $this->getBodyFromHttpRequest($response);

       self::assertEquals('monthly_standard', $response['plans']['monthly'][1]['identifier']);
   }
}