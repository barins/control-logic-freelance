<?php

namespace Tests\Feature\App\Modules\MasterUser\Http\Controllers;

use Illuminate\Testing\TestResponse;
use Tests\Feature\App\Modules\MasterUser\BaseCase;

final class AuthControllerTest extends BaseCase
{
    protected string $confirmation_code;

    public function testRegister(): void
    {
        $this->baseRegister()->assertSuccessful();
    }

    public function testActivation(): void
    {
        $this->baseActivation()->assertSuccessful();
    }

    public function testLogin(): void
    {
        $this->baseLogin()->assertSuccessful();
    }

    public function testPasswordResetRequest(): void
    {
        $this->basePasswordResetRequest()->assertSuccessful();
    }

    public function testPasswordReset(): void
    {
        $this->basePasswordResetRequest();

        $requestBody = [
            'code' => $this->confirmation_code,
            'email' => $this->master_user['email'],
            'password' => $this->master_user['password'],
            'password_confirmation' => $this->master_user['password'],
        ];

        $this->post(route('master_user.password-reset'), $requestBody)->assertSuccessful();
    }

    public function testGetAuthorizedMasterUser(): void
    {
        $response = $this->get(route('master_user.index'));

        self::assertEquals('Unauthenticated.', $this->getMessageFromHttpRequest($response));

        $this->baseLogin();

        $this->get(route('master_user.index'), $this->getAuthHeaders())->assertSuccessful();
    }

    protected function basePasswordResetRequest(): TestResponse
    {
        $this->baseLogin();

        $requestBody = [
            'email' => $this->master_user['email'],
        ];

        $response = $this->post(route('master_user.password-reset-request'), $requestBody);

        $this->confirmation_code = $this->getBodyFromHttpRequest($response)['confirmation_code'];

        return $response;
    }
}