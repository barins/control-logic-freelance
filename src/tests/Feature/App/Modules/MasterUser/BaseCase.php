<?php

namespace Tests\Feature\App\Modules\MasterUser;

use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use App\Modules\MasterUser\Model\MasterUser;

abstract class BaseCase extends TestCase
{
    protected array $master_user;

    protected function baseRegister(): TestResponse
    {
        $this->master_user = factory(MasterUser::class)->make()->toArray(false);
        $this->master_user['password'] = Str::random('8');

        return $this->post(route('master_user.sign-up'), $this->master_user);
    }

    protected function baseActivation(): TestResponse
    {
        $requestBody = [
            'confirmation_code' => $this->getBodyFromHttpRequest($this->baseRegister())['confirmation_code'],
            'email' => $this->master_user['email'],
        ];

        return $this->post(route('master_user.activation'), $requestBody);
    }

    protected function baseLogin(): TestResponse
    {
        $this->baseActivation();

        $requestBody = [
            'email' => $this->master_user['email'],
            'password' => $this->master_user['password'],
        ];

        $response = $this->post(route('master_user.sign-in'), $requestBody);

        $this->jwt = $this->getBodyFromHttpRequest($response)['access_token'];

        return $this->post(route('master_user.sign-in'), $requestBody);
    }

    protected function getAuthMasterUser(): array
    {
        return Auth::guard(MasterUser::GUARD)->user()->toArray(true);
    }
}
