<?php

namespace Tests\Feature\App\Modules\Office\Http\Controllers;

use Tests\Feature\App\Modules\Office\BaseCase;

final class OfficeControllerTest extends BaseCase
{
    public function testActionOffice(): void
    {
       $this->baseLogin();

       $response = $this->post(
           route('office.create'),
           $this->getBodyForRequestOffice(),
           $this->getAuthHeaders()
       );

       $office_id = $this->getBodyFromHttpRequest($response)['office']['id'];

       $response->assertSuccessful();

        $this->put(
            route('office.update', $office_id),
            $this->getBodyForRequestOffice(),
            $this->getAuthHeaders()
        )->assertSuccessful();
    }

   protected function getBodyForRequestOffice(): array
   {
       return [
           'name' => $this->faker->name,
           'country_id' => 235,
           'city_id' => 1,
           'address' => $this->faker->streetAddress,
           'floor_area' => 1,
           'floor' => 1,
           'company_id' => $this->getAuthMasterUser()['companies'][0]['id'],
           'number_of_floors' => 1,
           'owned_office' => 1,
           'working_hours' => config('working_hours.default'),
           'monthly_rent_payment_date' => '2024-08-12T08:06:53.000000Z'
       ];
   }
}