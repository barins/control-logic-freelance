<?php

namespace Tests\Feature\App\Modules\Core\Http\Controllers;

use Tests\Feature\App\Modules\MasterUser\BaseCase;

final class CountryControllerTest extends BaseCase
{
    public function testGetAllCountries(): void
    {
        $response = $this->get(route('countries.index'));

        $response->assertSuccessful();

        $response = $this->getBodyFromHttpRequest($response);

       self::assertEquals('Andorra', $response['countries'][5]['name']);
    }
}