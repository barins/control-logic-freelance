<?php

namespace Tests\Feature\App\Modules\Company\Http\Controllers;

use Tests\Feature\App\Modules\Company\BaseCase;

final class IndustryControllerTest extends BaseCase
{
    public function testGetAllIndustries(): void
    {
        $response = $this->get(route('industries.index'));

        $response->assertSuccessful();

        $response = $this->getBodyFromHttpRequest($response);

        self::assertEquals('IT', $response['industries'][2]['name']);
    }
}