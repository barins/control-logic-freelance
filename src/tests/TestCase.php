<?php

namespace Tests;

use Illuminate\Testing\TestResponse;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFaker;


    protected string $jwt;

    /**
     * @param  TestResponse  $response
     *
     * @return array
     */
    protected function getBodyFromHttpRequest(TestResponse $response): array
    {
        return json_decode($response->getContent(), true, 512, 0)['data'];
    }

    /**
     * @param  TestResponse  $response
     *
     * @return string
     */
    protected function getMessageFromHttpRequest(TestResponse $response): string
    {
        return json_decode($response->getContent(), true, 512, 0)['message'];
    }

    protected function getAuthHeaders(): array
    {
        return [
            'Authorization' => "Bearer {$this->jwt}",
            'accept' => 'application/json',
        ];
    }

    public function get($uri, array $headers = []): TestResponse
    {
        $headers['accept'] = 'application/json';

        return parent::get($uri, $headers);
    }

    public function post($uri, array $data = [], array $headers = []): TestResponse
    {
        $headers['accept'] = 'application/json';

        return parent::post($uri, $data, $headers);
    }
}
