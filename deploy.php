<?php

namespace Deployer;
require 'recipe/laravel.php';
require 'deployer/recipe/telegram.php';

define('STAGE_DEVELOPMENT', 'development');

// Configuration
set('ssh_type', 'native');
set('repository', 'git@gitlab.com:xxxxx/xxxxxx.git');
set('keep_releases', 3);
set('shared_dirs', [
    'src/storage',
    'src/public/media'
]);
set('writable_dirs', [
    'src/storage',
    'src/vendor',
]);
set('composer_options', 'install --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader --no-suggest');

// Gitlab CI env.
// https://docs.gitlab.com/ee/ci/variables/#doc-nav
set('gitlab_user_name', getenv('GITLAB_USER_NAME')); // The real name of the user who started the job
set('ci_pipeline_url', getenv('CI_PIPELINE_URL'));

// Servers
host(STAGE_DEVELOPMENT)->stage(STAGE_DEVELOPMENT)
    ->hostname('111.122.87.10') // test
    ->user('root')
    ->set('deploy_path', '/var/www/control-logic-back-end')
    ->set('branch', 'dev');

/**
 * Create cache folder in bootstrap directory
 */
task('deploy:cache_folder', function() {
    run('mkdir {{release_path}}/src/bootstrap/cache');
})->desc('Manually create cache folder');

/**
 * Run migrations
 */
task('migration', function() {
    run('php {{release_path}}/src/artisan migrate --force');
})->desc('Artisan migrations');

/**
 * Run migrations from scratch
 */
task('artisan:migration:fresh', function() {
    run('php {{release_path}}/src/artisan migrate:fresh');
})->desc('Artisan migrations from scratch');

/**
 * Run seed(from modules) in BD
 */
task('artisan:module:seed', function() {
    run('php {{release_path}}/src/artisan module:seed');
})->desc('Artisan module:seed');

/**
 * Run load fake data in BD
 */
task('artisan:seed-fake', function() {
    run('php {{release_path}}/src/artisan db:seed --class="\App\Modules\Core\Database\Seeds\FakeFirstLoadDataSeeder"
');
})->desc('Artisan module:seed:fake');

/**
 * Upload .env.production file as .env
 */
task('environment', function () {
    run(parse('cp {{release_path}}/src/.env.{{target}} {{release_path}}/src/.env'));
})->desc('Environment setup');

/**
 * Drops an Elasticsearch index of office.
 */
task('elastic:drop-index-office', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:drop-index "\App\Modules\Office\ElasticIndexConfigurator\OfficeIndexConfigurator"'));
})->desc('Execute elastic:drop-index-office');

/**
 * Create Elasticsearch index of office.
 */
task('elastic:create-index-office', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:create-index "\App\Modules\Office\ElasticIndexConfigurator\OfficeIndexConfigurator"'));
})->desc('Execute elastic:create-index-office');

/**
 * Drops an Elasticsearch index of department.
 */
task('elastic:drop-index-department', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:drop-index "\App\Modules\Department\ElasticIndexConfigurator\DepartmentIndexConfigurator"'));
})->desc('Execute elastic:drop-index-department');

/**
 * Create Elasticsearch index of department.
 */
task('elastic:create-index-department', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:create-index "\App\Modules\Department\ElasticIndexConfigurator\DepartmentIndexConfigurator"'));
})->desc('Execute elastic:create-index-department');


/**
 * Drops an Elasticsearch index of employee.
 */
task('elastic:drop-index-employee', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:drop-index "\App\Modules\Employee\ElasticIndexConfigurator\EmployeeIndexConfigurator"'));
})->desc('Execute elastic:drop-index-department');

/**
 * Create Elasticsearch index of employee.
 */
task('elastic:create-index-employee', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:create-index "\App\Modules\Employee\ElasticIndexConfigurator\EmployeeIndexConfigurator"'));
})->desc('Execute elastic:create-index-department');

/**
 * Drops an Elasticsearch index of contact.
 */
task('elastic:drop-index-contact', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:drop-index "\App\Modules\Contact\ElasticIndexConfigurator\ContactIndexConfigurator"'));
})->desc('Execute elastic:drop-index-department');

/**
 * Create Elasticsearch index of contact.
 */
task('elastic:create-index-contact', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:create-index "\App\Modules\Contact\ElasticIndexConfigurator\ContactIndexConfigurator"'));
})->desc('Execute elastic:create-index-department');

/**
 * Drops an Elasticsearch index of project.
 */
task('elastic:drop-index-project', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:drop-index "\App\Modules\Project\ElasticIndexConfigurator\ProjectIndexConfigurator"'));
})->desc('Execute elastic:drop-index-project');

/**
 * Create Elasticsearch index of project.
 */
task('elastic:create-index-project', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:create-index "\App\Modules\Project\ElasticIndexConfigurator\ProjectIndexConfigurator"'));
})->desc('Execute elastic:create-index-project');

/**
 * Drops an Elasticsearch index of cost.
 */
task('elastic:drop-index-cost', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:drop-index "\App\Modules\Cost\ElasticIndexConfigurator\CostIndexConfigurator"'));
})->desc('Execute elastic:drop-index-cost');

/**
 * Create Elasticsearch index of cost.
 */
task('elastic:create-index-cost', function () {
    run(parse('{{bin/php}} {{deploy_path}}/current/src/artisan elastic:create-index "App\Modules\Cost\ElasticIndexConfigurator\CostIndexConfigurator"'));
})->desc('Execute elastic:create-index-cost');

/**
 * Clear cache
 */
task('artisan:cache:clear', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/src/artisan cache:clear');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan cache:clear');

task('artisan:storage:link', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/src/artisan storage:link');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan storage:link');

/**
 * Clear compiled views
 */
task('artisan:view:clear', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/src/artisan view:clear');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan view:clear');

/**
 * Recreate route cache file
 */
task('artisan:route:cache', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/src/artisan route:cache');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan route:cache');

/**
 * Recreate config cache
 */
task('artisan:config:cache', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/src/artisan config:cache');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan config:clear');

/**
 * Module optimize
 */
task('artisan:module:optimize', function () {
    $output = run('{{bin/php}} {{deploy_path}}/current/src/artisan module:optimize');
    writeln('<info>' . $output . '</info>');
})->desc('Execute artisan module:optimize');

/**
 * Fix permissions
 */
task('permissions:reset', function () {
    run('cd {{deploy_path}}');
    run('sudo chown -R www-data:www-data {{deploy_path}}');
    run('sudo find . -type f -exec chmod 644 {} \;');
    run('sudo find . -type d -exec chmod 755 {} \;');

    run('cd {{release_path}}');
    run('sudo chmod 775 {{release_path}}/src/vendor');
})->desc('Fix permissions')->onStage(STAGE_DEVELOPMENT);

/**
 * Create storage folders not in version control
 */
task('deploy:storage_folder', function() {
    run('mkdir -p {{release_path}}/src/storage/app/public');
    run('mkdir -p {{release_path}}/src/storage/framework/cache');
    run('mkdir -p {{release_path}}/src/storage/framework/session');
    run('mkdir -p {{release_path}}/src/storage/framework/views');
    run('mkdir -p {{release_path}}/src/storage/logs');
})->desc('Manually create cache folder');


task('deploy:vendors', function() {
    if (!commandExist('unzip')) {
        writeln('<comment>To speed up composer installation setup "unzip" command with PHP zip extension https://goo.gl/sxzFcD</comment>');
    }
    run('cd {{release_path}}/src && {{bin/composer}} {{composer_options}}');
});

task('supervisor:reload', function () {
    run("cd {{release_path}} && supervisorctl reload");
})->desc('Reload supervisor');


task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:vendors',
    'deploy:writable',
    'deploy:shared',
    'deploy:symlink',
    'environment',
//    'elastic:drop-index-office',
//    'elastic:create-index-office',
//    'elastic:drop-index-department',
//    'elastic:create-index-department',
//    'elastic:drop-index-employee',
//    'elastic:create-index-employee',
//    'elastic:drop-index-contact',
//    'elastic:create-index-contact',
//    'elastic:drop-index-project',
//    'elastic:create-index-project',
//    'elastic:drop-index-cost',
//    'elastic:create-index-cost',
    'migration',
//    'artisan:migration:fresh',
//    'artisan:module:seed',
//    'artisan:seed-fake',
    'artisan:storage:link',
    'artisan:module:optimize',
    'artisan:cache:clear',
    'artisan:view:clear',
    'artisan:config:cache',
    'cleanup',
    'permissions:reset',
])->desc('Deploy the app');


after('deploy', 'success');
